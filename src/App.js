/*
 * Copyright  2018 Linkel Technology Co., Ltd, Beijing
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BA SIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React, { Component } from "react";
import { Admin, Resource } from "react-admin/lib";

import KeypairIcon from "@material-ui/icons/VpnKey";
import AccountIcon from "@material-ui/icons/Face";
import TransIcon from "@material-ui/icons/Receipt";
import BlockIcon from "@material-ui/icons/ViewColumn";
import FileIcon from "@material-ui/icons/CloudQueue";
import { includes } from "lodash"; 
import { 
    KeypairList, KeypairShow, KeypairEdit, KeypairCreate,
} from "./components/keypair";
import { 
    AccountList, AccountShow, AccountEdit, AccountCreate, 
} from "./components/account";
import { TransList, TransShow, TransCreate } from "./components/transaction";
import { BlockList, BlockShow } from "./components/block";
import { FileList, FileCreate, FileShow } from "./components/file";
import { AccessAuthCreate, AccessAuthEdit } from "./components/file/FileACLAlter";

import Dashboard from "./components/dashboard/Dashboard";

// import authProvider from "./authProvider";
import Messages from "./i18n/cn";

// import  dataProvider from './dataprovider/data-provider'
import buildGraphQLProvider from "./adaptator";
import indexDataProvider from "./dataprovider/ra-data-indexdb";
import fileStorageServerRestProvider from "./dataprovider/ra-data-fileStorageServer-rest";
import addUploadCapabilities from "./dataprovider/addUploadFeature";
import createRealtimeSaga from "./createRealtimeSaga";
import settings from "./settings";

const messages = {
    cn: Messages,
    en: Messages,
};
const i18nProvider = locale => messages[locale];

class App extends Component {
    constructor() {
        super();
        this.state = { dataProvider: null };
    }

    componentDidMount() {
        buildGraphQLProvider({
            clientOptions: { uri: settings.Prisma.endpoint },
        }).then((dataProvider) => {
            const upDataProvider = addUploadCapabilities(dataProvider);
            const realTimeSaga = createRealtimeSaga(upDataProvider);
            return this.setState({
                customSagas: realTimeSaga,
                dataProvider: (type, resource, params) => {
                    if (resource === "keypairs" || resource === "accounts") { return addUploadCapabilities(indexDataProvider)(type, resource, params); }
                    if (includes(["files", "proofs", "accessAuths"], resource)) { return fileStorageServerRestProvider(type, resource, params); }
                    return upDataProvider(type, resource, params);
                },
            });
        });
    }

    render() {
        const { dataProvider, customSagas, title } = this.state;

        if (!dataProvider) {
            return <div id="data-provider-loading">Loading</div>;
        }

        return (
            <Admin dataProvider={dataProvider}
                title = {title}
                // authProvider={authProvider}
                customSagas={[customSagas]}
                locale="cn" 
                i18nProvider={i18nProvider} 
                dashboard={Dashboard} >
                <Resource name="accounts" list={AccountList} show={AccountShow} edit={AccountEdit} create={AccountCreate} icon={AccountIcon} />
                <Resource name="keypairs" list={KeypairList} show={KeypairShow} edit={KeypairEdit} create={KeypairCreate} icon={KeypairIcon} />
                <Resource name="Block" list={BlockList} show={BlockShow} icon={BlockIcon} />
                <Resource name="Transaction" list={TransList} show={TransShow} create={TransCreate} icon={TransIcon} />
                <Resource name="files" list={FileList} show={FileShow} create={FileCreate} icon={FileIcon} />
                <Resource name="proofs" />
                <Resource name="accessAuths" edit={AccessAuthEdit} create={AccessAuthCreate} />
            </Admin>
        );
    }
}
export default App;
