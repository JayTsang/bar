import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { GraphQLClient } from "graphql-request";
import {
    SaveButton,
    Toolbar,
} from "react-admin";
import { Link } from "react-router-dom";
import { Button, Icon, LinearProgress } from "@material-ui/core";
import settings from "../../settings";


const graphqlClient = new GraphQLClient(`${settings.Prisma.endpoint}`);

const styles = () => ({
    button: {
        minHeight: 36,
        height: 36,
        marginLeft: 30,
    },
    buttonIcon: {
        marginRight: "8px",
    },
});

class AccountEditToolbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: null,
        };
    }

    componentDidMount() {
        const { creditCode } = this.props.record;
        graphqlClient.request(`
            {
                account(where: {
                   creditCode: "${creditCode}"
               }){
                   id
               }
            }
        `).then((res) => {
            if (res.account) this.setState({ status: true });
            else this.setState({ status: false });
            return 0;
        });
    }

    render() {
        const { name, creditCode, mobile } = this.props.record;
        const { status } = this.state;
        const { classes } = this.props;
        return (
            <Toolbar {...this.props} >
                <SaveButton redirect="edit" />
                {
                    status === null
                        ? <LinearProgress />
                        : (
                            status
                                ? null
                                : <Button
                                    className={classes.button}
                                    id="registerAccount"
                                    variant="contained"
                                    color="primary"
                                    component={Link}
                                    to={
                                        {
                                            pathname: "/Transaction/create",
                                            state: {
                                                chaincodeName: "ContractCert",
                                                chaincodeVersion: 1,
                                                chaincodeFunction: "SignUpSigner",
                                                chaincodeArgs: [`{"name":"${name}", "creditCode":"${creditCode}", "mobile":"${mobile}", "certNames":[] }`],
                                            },
                                        }
                                    }
                                >
                                    <Icon className={classes.buttonIcon}>person_add</Icon>
                                    注册账号
                                </Button>
                        )
                }
                {
                    status === null
                        ? <LinearProgress />
                        : (status
                            ? <Button
                                className={classes.button}
                                id="updateAccount"
                                variant="contained"
                                color="primary"
                                component={Link}
                                to={{
                                    pathname: "/Transaction/create",
                                    state: {
                                        chaincodeName: "ContractCert",
                                        chaincodeVersion: 1,
                                        chaincodeFunction: "UpdateSigner",
                                        chaincodeArgs: [`{"name":"${name}", "creditCode":"${creditCode}", "mobile":"${mobile}", "certNames":[] }`],
                                    },
                                }}
                            >
                                <Icon className={classes.buttonIcon}>person</Icon>
                                更新账号信息
                            </Button>
                            : null
                        )
                }
            </Toolbar>
        );
    }
}

export default withStyles(styles)(AccountEditToolbar);
