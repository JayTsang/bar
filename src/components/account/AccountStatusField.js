import React, { Component } from "react";
import { GraphQLClient } from "graphql-request";
import { LinearProgress, Chip } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { green, grey } from "@material-ui/core/colors";
import settings from "../../settings";

const graphqlClient = new GraphQLClient(`${settings.Prisma.endpoint}`);

const styles = () => ({
    chipGood: {
        backgroundColor: green[500],
        color: grey[50],
    },
});

class AccountStatusField extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: null,
        };
    }

    componentDidMount() {
        const { creditCode } = this.props.record;
        graphqlClient.request(`
            {
               account(where: {
                   creditCode: "${creditCode}"
               }){
                   id
               }
            }
        `).then((res) => {
            if (res.account) this.setState({ status: true });
            else this.setState({ status: false });
            return 0;
        });
    }

    render() {
        const { classes } = this.props;
        if (this.state.status === null) {
            return <LinearProgress />;
        } if (this.state.status) {
            return <Chip
                label={<span id="status">已注册到区块链</span>}
                className={classes.chipGood}
            />;
        } 
        return <Chip label={<span id="status">未注册到区块链</span>} />;
    }
}

const AccountStatusFieldWithStyles = withStyles(styles)(AccountStatusField);

AccountStatusFieldWithStyles.defaultProps = {
    addLabel: true,
};

export default AccountStatusFieldWithStyles;
