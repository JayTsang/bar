/*
 * Copyright  2018 Linkel Technology Co., Ltd, Beijing
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BA SIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from "react";
import {
    FormTab, TabbedForm,
    ReferenceManyField, Pagination, ChipField,
    Filter, DateField, DateInput, Responsive, SimpleList, List, 
    Edit, Create, Datagrid, TextField, EditButton, ShowButton,
    DisabledInput, SimpleForm, TextInput,
    Show, TabbedShowLayout, Tab,
} from "react-admin/lib";
import AccountStatusField from "./AccountStatusField";
import AccountEditToolbar from "./AccountEditToolbar";

const AccountFilter = props => (
    <Filter {...props}>
        <TextInput label="pos.search" source="q" alwaysOn />
        <DateInput source="created" />
    </Filter>
);

export const AccountList = props => (
    <List {...props}
        filters={<AccountFilter />}
        bulkActions={false}
        sort={{ field: "id", order: "DESC" }}>
        <Responsive
            small={
                <SimpleList
                    primaryText={record => record.creditCode}
                    secondaryText={record => record.name}
                    tertiaryText={record => new Date(record.createdAt).toLocaleDateString()}
                    linkType="show"
                />
            }
            medium={
                <Datagrid>
                    <TextField source="id" />
                    <TextField source="name" />
                    <TextField source="creditCode" />
                    <AccountStatusField source="status"/>
                    <ShowButton />
                    <EditButton />
                </Datagrid>
            }
        />
    </List>
);

const AccountTitle = ({ record }) => <span> {record ? `账号#${record.name}-${record.creditCode}` : ""}</span>;

export const AccountShow = props => (
    <Show title={<AccountTitle />} {...props}>
        <TabbedShowLayout>
            <Tab label="resources.accounts.tabs.tab1">
                <TextField source="id" />
                <TextField source="creditCode" id="creditCode" />
                <TextField source="name" id="name"/>
                <TextField source="mobile" id="mobile"/>
                <AccountStatusField source="status"/>
            </Tab>
            <Tab label="resources.accounts.tabs.tab2">
                <ReferenceManyField 
                    addLabel={false} 
                    source="id" 
                    reference="keypairs" 
                    target="ownerID"
                    pagination={<Pagination />}
                >
                    <Datagrid>
                        <TextField source="id" />
                        <TextField source="cert.sn" />
                        <TextField source="name"/>
                        <DateField source="createdAt" showTime />
                        <ShowButton />
                    </Datagrid>
                </ReferenceManyField>
            </Tab>
            <Tab label="resources.accounts.tabs.tab3">
                <ReferenceManyField 
                    addLabel={false} 
                    source="creditCode" 
                    reference="Transaction" 
                    target="signature.certID.creditCode"
                    perPage={10}
                    pagination={<Pagination />}
                >
                    <Datagrid>
                        <TextField source="txid" />
                        <ChipField source="type" />
                        <TextField source="signature.certID.certName" sortable={false}/>
                        <DateField source="timestamp" showTime/>
                        <ShowButton />
                    </Datagrid>
                </ReferenceManyField>
            </Tab>
        </TabbedShowLayout>
    </Show>
);

export const AccountEdit = props => (
    <Edit title={<AccountTitle />} {...props}>
        <TabbedForm toolbar={<AccountEditToolbar />} redirect="show" >
            <FormTab label="resources.accounts.tabs.tab1">
                <DisabledInput source="id" />
                <DisabledInput source="creditCode" />
                <DisabledInput source="name" />
                <TextInput source="mobile" />
            </FormTab>
            <FormTab label="resources.accounts.tabs.tab2">
                <ReferenceManyField 
                    addLabel={false} 
                    source="id" 
                    reference="keypairs" 
                    target="ownerID"
                    pagination={<Pagination />}
                >
                    <Datagrid>
                        <TextField source="id" />
                        <TextField source="cert.sn" />
                        <TextField source="name"/>
                        <DateField source="createdAt" showTime />
                        <EditButton />
                    </Datagrid>
                </ReferenceManyField>       
            </FormTab>
        </TabbedForm>
    </Edit>
);

export const AccountCreate = props => (
    <Create {...props}>
        <SimpleForm redirect="list">
            <TextInput source="creditCode" />
            <TextInput source="name" />
            <TextInput source="mobile" />
        </SimpleForm>
    </Create>
);
