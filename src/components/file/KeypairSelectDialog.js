import React, { Component } from "react";
import {
    GET_LIST,
} from "react-admin";

import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    Button,
    Typography,
    TextField as MUITextField,
    MenuItem,
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import indexDataProvider from '../../dataprovider/ra-data-indexdb';

const styles = theme => ({
    dialogTextField: {
        minWidth: 150,
        width: "100%",
        marginTop: 15,
    },
    dialogContentText: {
        color: "red",
        marginBottom: 15,
    },
});

class KeypairSelectDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            accounts: null,
            accountCreditCodeSelected: "",
            keypairs: null,
            keypairSelected: "",
            password: null,
        };
    }

    componentDidMount() {
        indexDataProvider(GET_LIST, "accounts", {
            pagination: { page: 1, perPage: 100000 },
            sort: { field: "id", order: "DESC" },
            filter: {},
        }).then((res) => {
            this._asyncRequestAccounts = null;
            this.setState({ accounts: res.data });
        });
    }

    handleAccountChange = (e) => {
        if (e.target.value === this.state.accountCreditCodeSelected) return;
        this.setState({
            accountCreditCodeSelected: e.target.value,
            keypairSelected: "",
        });
        indexDataProvider(GET_LIST, "keypairs", {
            pagination: { page: 1, perPage: 100000 },
            sort: { field: "id", order: "DESC" },
            filter: { ownerCreditCode: e.target.value },
        }).then((res) => {
            this.setState({ keypairs: res.data });
        });
    };

    handleKeypairChange = (e) => {
        const keypair = e.target.value;
        const handleKeypairChangeCustom = this.props.handleKeypairChange;
        if(handleKeypairChangeCustom) handleKeypairChangeCustom(keypair);
        this.setState({ keypairSelected: keypair });
    }

    handlePasswordChange = (e) => {
        this.setState({ password: e.target.value });
    }

    handleOk = () => {
        const { keypairSelected, password } = this.state;
        this.props.handleOk(keypairSelected, password);
    }

    handleCancel = () => {
        this.props.handleCancel();
    }

    render() {
        const {
            accounts,
            accountCreditCodeSelected,
            keypairs,
            keypairSelected,
            password,
        } = this.state;
        const { 
            classes,
            open,
            title,
            textContent,
            showPasswordInut,
        } = this.props;
        return (
            <Dialog
                disableBackdropClick
                disableEscapeKeyDown
                maxWidth="xs"
                open={open}
            >
                <DialogTitle id="keypair-select-dialog-title">{title}</DialogTitle>
                <DialogContent>
                    <Typography className={classes.dialogContentText}>{textContent}</Typography>
                    {
                        <MUITextField
                            id="keypair-select-input-account"
                            label="账户"
                            select
                            className={classes.dialogTextField}
                            onChange={this.handleAccountChange}
                            value={accountCreditCodeSelected}
                        >
                            {
                                accounts
                                    ? accounts.map(account => (
                                        <MenuItem
                                            key={account.creditCode}
                                            value={account.creditCode}
                                        >
                                            {account.name}
                                        </MenuItem>
                                    ))
                                    : <MenuItem key="none1" value=""></MenuItem>
                            }
                        </MUITextField>
                    }
                    {
                        <MUITextField
                            id="keypair-select-input-keypair"
                            label="密钥对"
                            select
                            className={classes.dialogTextField}
                            onChange={this.handleKeypairChange}
                            value={keypairSelected}
                        >
                            {
                                keypairs
                                    ? keypairs.map(keypair => (
                                        <MenuItem
                                            key={keypair.id}
                                            value={keypair}
                                        >
                                            {keypair.name}
                                        </MenuItem>
                                    ))
                                    : <MenuItem key="none2" value=""></MenuItem>
                            }
                        </MUITextField>
                    }
                    {
                        showPasswordInut
                        ? <MUITextField 
                            id="keypair-select-input-password"
                            label="私钥密码"
                            type="password"
                            className={classes.dialogTextField}
                            onChange={this.handlePasswordChange}
                            value={password}
                        />
                        : null
                    }
                </DialogContent>
                <DialogActions>
                    <Button
                        color="primary"
                        variant="contained"
                        onClick={this.handleCancel}
                        id="cancel"
                    >
                        取消
                    </Button>
                    <Button
                        color="primary"
                        variant="contained"
                        disabled={keypairSelected === ""}
                        onClick={this.handleOk}
                        id="ok"
                    >
                        确认
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default withStyles(styles)(KeypairSelectDialog);