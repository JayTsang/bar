import React from "react";
import {
    SimpleForm,
    DisabledInput,
    TextInput,
    NumberInput,
    SelectArrayInput,
    SelectInput,
    ReferenceInput,
    required,
    FormDataConsumer,
} from "react-admin";
import { isEmpty, cloneDeep } from "lodash";
import DateFnsUtils from "material-ui-pickers/utils/date-fns-utils";
import zhLocale from "date-fns/locale/zh-CN";
import { DateTimeInput } from "react-admin-date-inputs";

const fileACLActions = [
    { id: "download", name: "下载" },
    { id: "accessAuthRead", name: "访问权限读" },
    { id: "accessAuthWrite", name: "访问权限写" },
];

const [
    RESTRICTIONTYPE_UNRESTRICTED,
    RESTRICTIONTYPE_TIMERANGE,
    RESTRICTIONTYPE_COUNT,
] = [
    "UNRESTRICTED",
    "TIME_RANGE",
    "COUNT",
];

export const FileACLForm = (props) => {
    const newProps = cloneDeep(props);
    newProps.redirect = props.fileid
        ? `/files/${props.fileid}/show/2`
        : `/files/${props.record.file.fileID}/show/2`;
    return (
        <SimpleForm {...newProps}>
            <DisabledInput
                source="file.fileID"
                defaultValue={props.fileid}
                validate={required()}
            />
            {
                isEmpty(props.record)
                    ? <TextInput source="userID" validate={required()} />
                    : <DisabledInput source="userID" />
            }
            <SelectArrayInput
                source="authority"
                choices={fileACLActions}
                validate={required()}
            />
            <SelectInput
                source="restrictionType"
                choices={[
                    { id: RESTRICTIONTYPE_UNRESTRICTED, name: "无限制" },
                    { id: RESTRICTIONTYPE_TIMERANGE, name: "时间段" },
                    { id: RESTRICTIONTYPE_COUNT, name: "次数" },
                ]}
                validate={required()}
            />
            <FormDataConsumer>
                {
                    ({ formData, ...rest }) => {
                        const { restrictionType } = formData;
                        if (restrictionType === RESTRICTIONTYPE_TIMERANGE) { 
                            return (
                                <div style={{ display: "inline-block" }}>
                                    <DateTimeInput
                                        label="访问开始时间"
                                        source="restrictionContent.from"
                                        options={{ format: "YYYY/MM/dd-HH:mm:SS", showTodayButton: true }}
                                        providerOptions={{ utils: DateFnsUtils, locale: zhLocale }}
                                        validate={required()}
                                        {...rest}
                                    />
                                    <DateTimeInput
                                        label="访问截止时间"
                                        source="restrictionContent.to"
                                        options={{ format: "YYYY/MM/dd-HH:mm:SS", showTodayButton: true }}
                                        providerOptions={{ utils: DateFnsUtils, locale: zhLocale }}
                                        validate={required()}
                                        {...rest}
                                    />
                                </div>
                            );
                        }
                        if (restrictionType === RESTRICTIONTYPE_COUNT) { 
                            return <NumberInput 
                                label="访问次数" 
                                source="restrictionContent.initialCount" 
                                {...rest} 
                            />;
                        }
                        return null;
                    }
                }
            </FormDataConsumer>
            <SelectArrayInput
                source="postActStoreProof"
                choices={fileACLActions}
                validate={required()}
            />
            <ReferenceInput
                label="账号"
                source="requesterID"
                reference="accounts"
                validate={required()}
            >
                <SelectInput optionText="name" optionValue="creditCode" />
            </ReferenceInput>
            <FormDataConsumer>
                {
                    ({ formData, ...rest }) => (
                        <ReferenceInput label="密钥对" source="keypairID"
                            reference="keypairs"
                            filter={{ ownerCreditCode: formData.requesterID ? formData.requesterID : "none" }}
                            validate={required()}
                            {...rest}>
                            <SelectInput optionText="name" optionValue="id" {...rest} />
                        </ReferenceInput>
                    )
                }
            </FormDataConsumer>
            <TextInput label="密钥密码" source="keypairPwd" defaultValue="" type="password" />
        </SimpleForm>
    );
};
