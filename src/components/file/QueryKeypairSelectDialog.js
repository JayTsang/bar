import React from "react";
import {
    GET_ONE,
    CREATE,
    UPDATE,
    showNotification,
} from "react-admin";
import indexDataProvider from '../../dataprovider/ra-data-indexdb';
import { Transaction, RestAPI } from "rclink";
import settings from "../../settings";
import { connect } from 'react-redux';
import KeypairSelectDialog from "./KeypairSelectDialog";

const defaultQueryKeypairResource = "defaultQueryKeypair";
const defaultQueryKeypairID = 1;

const QueryKeypairSelectDialog = (props) => {
    const handleKeypairChange = (keypair) => {
        keypair.id = defaultQueryKeypairID; // 保证indexedDB中defaultQueryKeypair只有一条记录
    };

    // 客户端直接调用RepChain中的文件操作存证合约的密钥对功能限定方法, 将其只用于检索
    const restrictKeypairOnlyForQuery = (keypair) => {
        const chaincodeName = "FileOperationsProofStorage";
        const chaincodeVersion = 1;
        const chaincodeFunction = "restrictKeypairOnlyForQuery";
        const argObj = {
            accountCreditCode: keypair.ownerCreditCode,
            certName: keypair.name,
            isRestricted: true,
        };
        const chaincodeFunctionArgs = [JSON.stringify(argObj)];
        const txArgs = {
            type: "CHAINCODE_INVOKE",
            chaincodeName,
            chaincodeVersion,
            chaincodeInvokeParams: {
                chaincodeFunction,
                chaincodeFunctionArgs,
            },
        };
        const tx = new Transaction(txArgs);
        const txSignedBuffer = tx.sign({
            prvKey: keypair.kp.prvKeyPEM,
            pubKey: keypair.kp.pubKeyPEM,
            alg: "ecdsa-with-SHA1",
            creditCode: keypair.ownerCreditCode,
            certName: keypair.name,
        });
        const rest = new RestAPI(settings.RepChain.default.url_api);
        return rest.sendTransaction(txSignedBuffer);
    };

    const handleOk = (keypair) => {
        restrictKeypairOnlyForQuery(keypair).then((r) => {
            if (r.err) {
                const { showNotification } = props;
                showNotification(r.err, "warning");
                return;
            }
            indexDataProvider(GET_ONE, defaultQueryKeypairResource, { id: defaultQueryKeypairID })
                .then((res) => {
                    if (res.data) {
                        indexDataProvider(UPDATE, defaultQueryKeypairResource, {
                            id: defaultQueryKeypairID,
                            data: keypair,
                        }).then((res) => {
                            if (res.data) {
                                props.handleQueryKeypairSelected();
                            }
                        });
                    } else {
                        indexDataProvider(CREATE, defaultQueryKeypairResource, {
                            data: keypair,
                        }).then((res) => {
                            if (res.data) {
                                props.handleQueryKeypairSelected();
                            }
                        });
                    }
                })
        });
    }

    const handleCancel = () => {
        props.handleQueryKeypairCanceled();
    }

    return (
        <KeypairSelectDialog 
            open={props.open}
            title="选择默认检索密钥"
            textContent="该默认检索密钥将只能被用于文件相关信息的检索，请勿使用已加密的密钥"
            handleKeypairChange={handleKeypairChange}
            handleOk={handleOk}
            handleCancel={handleCancel}
        />
    );
}

export default connect(undefined, {
    showNotification,
})(QueryKeypairSelectDialog);