/*
 * Copyright 2019 Linkel Technology Co., Ltd, Beijing
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BA SIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React, { Component } from 'react';
import { Link } from "react-router-dom";
import {
    Chip,
    List, ListItem, ListItemAvatar, ListItemText, Divider,
    Table, TableHead, TableRow, TableCell, TableBody,
    Typography,
} from "@material-ui/core";
import { Done, Loop, ErrorOutline } from "@material-ui/icons";
import { withStyles } from '@material-ui/core/styles';
import { red, green, orange } from '@material-ui/core/colors';
import { Avatar } from "@material-ui/core";
import {
    Dialog, DialogTitle, DialogContent,
} from "@material-ui/core";
import PropTypes from 'prop-types';
import { get, isEmpty, isObject } from "lodash";

export const [
    FILE_OP_TYPE_U,
    FILE_OP_TYPE_D,
    FILE_OP_TYPE_A,
] = [
        "UPLOAD",
        "DOWNLOAD",
        "AUTH_UPDATE",
    ];
export const fileOpType = {
    [FILE_OP_TYPE_U]: {
        label: "文件上传存储",
        avatarChar: "U",
    },
    [FILE_OP_TYPE_D]: {
        label: "文件下载",
        avatarChar: "D",
    },
    [FILE_OP_TYPE_A]: {
        label: "文件访问权限变更",
        avatarChar: "A",
    },
};

export const [
    PROOF_STATUS_CONFIRMED,
    PROOF_STATUS_WAITING,
    PROOF_STATUS_FAILED
] = ["CONFIRMED", "WAITING", "FAILED"];
export const proofStatus = {
    [PROOF_STATUS_CONFIRMED]: "已上链确认",
    [PROOF_STATUS_WAITING]: "上链等待中",
    [PROOF_STATUS_FAILED]: "存证失败",
};

export const FileOperationTypeField = ({ source, record = {} }) => {
    return (
        <Chip
            label={fileOpType[record[source]].label}
            avatar={<Avatar>{fileOpType[record[source]].avatarChar}</Avatar>}
        />
    );
};

export const ProofLongTextField = withStyles({
    longText: {
        wordBreak: "break-all",
    }
})(({ classes, source, record = {} }) => {
    if (source === "txid" && record.status === PROOF_STATUS_CONFIRMED) {
        const url = `/Transaction/${record.transaction.id}/show`
        return (
            <Link className={classes.longText} to={url} id="confirmedTx">{get(record, source)}</Link>
        );
    }
    return <span className={classes.longText}>{get(record, source)}</span>
});

export class ProofStatusField extends Component {
    constructor(props) {
        super(props);
        this.state = { open: false };
    }

    handleClick = () => {
        this.setState({ open: true });
    }
    handleClose = () => {
        this.setState({ open: false });
    }

    render() {
        const { record, source } = this.props;
        const status = proofStatus[record[source]];
        let backgroundColor;
        let StatusIcon;
        const chipIconStyles = { color: "white" };
        switch (record[source]) {
            case PROOF_STATUS_CONFIRMED:
                backgroundColor = green[500];
                StatusIcon = <Done style={chipIconStyles} />;
                break;
            case PROOF_STATUS_WAITING:
                backgroundColor = orange[600];
                StatusIcon = <Loop style={chipIconStyles} />;
                break;
            case PROOF_STATUS_FAILED:
                backgroundColor = red[800];
                StatusIcon = <ErrorOutline style={chipIconStyles} />;
                break;
            default:
                break;
        }

        const StatusChip = withStyles({
            root: {
                backgroundColor,
            },
            label: {
                color: "white"
            }
        })(Chip)

        const LongListItemText = withStyles({
            root: {
                wordBreak: "break-all",
            }
        })(ListItemText);

        return (
            <div>
                <StatusChip
                    label={status}
                    deleteIcon={StatusIcon}
                    onDelete={this.handleClick}
                    onClick={this.handleClick}
                    id="proofStatus"
                />
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                >
                    <DialogTitle>文件操作存证结果</DialogTitle>
                    <DialogContent>
                        <List>
                            <Divider />
                            <ListItem divider>
                                <ListItemText primary={`存证状态：${status}`} />
                                <ListItemAvatar>
                                    <Avatar style={{ backgroundColor }}>
                                        {StatusIcon}
                                    </Avatar>
                                </ListItemAvatar>
                            </ListItem>
                            <ListItem divider>
                                <ListItemText primary={`操作类型：${fileOpType[record.operationType].label}`} id="opType" />
                            </ListItem>
                            <ListItem divider>
                                <Typography variant="subheading">操作参数：</Typography>
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Key</TableCell>
                                            <TableCell>Value</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {
                                            !isEmpty(record.operationParams) ?
                                                Object.keys(record.operationParams).map(key => (
                                                    <TableRow>
                                                        <TableCell>{key}</TableCell>
                                                        <TableCell>{
                                                            isObject(record.operationParams[key]) ?
                                                                JSON.stringify(record.operationParams[key]) :
                                                                record.operationParams[key]
                                                        }
                                                        </TableCell>
                                                    </TableRow>
                                                )) :
                                                <TableRow>
                                                    <TableCell>无</TableCell>
                                                    <TableCell>无</TableCell>
                                                </TableRow>
                                        }
                                    </TableBody>
                                </Table>
                            </ListItem>
                            <ListItem divider>
                                <LongListItemText primary={`操作发起者：${record.operatorID}`} />
                            </ListItem>
                            <ListItem divider>
                                <LongListItemText primary={`文件资源标识：`} />
                                <ProofLongTextField source="file.fileID" record={record} />
                            </ListItem>
                            <ListItem divider>
                                <LongListItemText primary={`存证交易哈希：`} />
                                <ProofLongTextField source="txid" record={record} />
                            </ListItem>
                            <ListItem divider>
                                <LongListItemText primary={`存证交易发起时间：${record.txTimestamp}`} />
                            </ListItem>
                            {
                                record.status === PROOF_STATUS_CONFIRMED && (
                                    <div>
                                        <ListItem divider>
                                            <LongListItemText primary={`确认区块哈希：${record.blockHash}`} />
                                        </ListItem>
                                        <ListItem divider>
                                            <LongListItemText primary={`确认区块出块时间：${record.blockTimestamp}`} />
                                        </ListItem>
                                    </div>
                                )
                            }
                        </List>
                    </DialogContent>
                </Dialog>
            </div>
        );
    }
};
ProofStatusField.propTypes = {
    record: PropTypes.object,
    source: PropTypes.string.isRequired,
}
