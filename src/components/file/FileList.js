/* eslint-disable no-nested-ternary */
/* eslint-disable import/no-extraneous-dependencies */
import React, { Component, useState } from "react";
import {
    ShowButton,
    DateField,
    Filter,
    Responsive,
    SimpleList,
    List,
    Datagrid,
    TextField,
    TextInput,
    NumberInput,
    SelectInput,
    GET_ONE,
    CardActions, 
    CreateButton, 
    ExportButton, 
    RefreshButton,
} from "react-admin";

import DateFnsUtils from "material-ui-pickers/utils/date-fns-utils";
import zhLocale from "date-fns/locale/zh-CN";
import { DateTimeInput } from "react-admin-date-inputs";
import Button from "@material-ui/core/Button";
import QueryKeypairSelectDialog from "./QueryKeypairSelectDialog";
import indexDataProvider from "../../dataprovider/ra-data-indexdb";

const FileFilter = props => (
    <Filter {...props}>
        <TextInput label="文件名" source="name" alwaysOn />
        <TextInput label="文件哈希" source="hash" />
        <NumberInput label="文件大小(下限)" source="sizeMin" />
        <NumberInput label="文件大小(上限)" source="sizeMax" />
        <SelectInput label="文件共享状态" source="shareStatus" choices={[
            { id: "OWNED", name: "自己拥有的" },
            { id: "OWNED_AND_SHARED_TO_OTHERS", name: "分享出去的" },
            { id: "OWNED_AND_NOT_SHARED_TO_OTHERS", name: "未分享出去的" },
            { id: "SHARED_FROM_OTHERS", name: "别人分享的" },
            { id: "ALL", name: "全部" },
        ]} alwaysOn />
        <DateTimeInput
            label="上传时间(下限)"
            source="uploadTimeFrom"
            options={{ format: "YYYY/MM/dd-HH:mm:SS", showTodayButton: true }}
            providerOptions={{ utils: DateFnsUtils, locale: zhLocale }}
        />
        <DateTimeInput
            label="上传时间(上限)"
            source="uploadTimeTo"
            options={{ format: "YYYY/MM/dd-HH:mm:SS", showTodayButton: true }}
            providerOptions={{ utils: DateFnsUtils, locale: zhLocale }}
        />
    </Filter>
);

const PostActions = ({
    basePath,
    currentSort,
    displayedFilters,
    exporter,
    filters,
    filterValues,
    resource,
    showFilter,
    total,
}) => {
    const [open, setOpen] = useState(false);

    const handleOnClick = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <QueryKeypairSelectDialog
                handleQueryKeypairCanceled={handleClose}
                handleQueryKeypairSelected={handleClose}
                open={open}
            />
            <CardActions>
                <Button color="primary" onClick={handleOnClick}>更改检索密钥</Button>
                {
                    filters && React.cloneElement(filters, {
                        resource,
                        showFilter,
                        displayedFilters,
                        filterValues,
                        context: "button",
                    })
                }
                <CreateButton basePath={basePath} />
                <ExportButton
                    disabled={total === 0}
                    resource={resource}
                    sort={currentSort}
                    filter={filterValues}
                    exporter={exporter}
                />
                <RefreshButton />
            </CardActions>
        </div>
    );
};


const defaultQueryKeypairResource = "defaultQueryKeypair";
const defaultQueryKeypairID = 1;

class FileList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hasDefaultQueryKeypair: null,
        };
    }

    componentDidMount() {
        indexDataProvider(GET_ONE, defaultQueryKeypairResource, {
            id: defaultQueryKeypairID,
        }).then((res) => {
            this._asyncRequestDefaultQueryKeypair = null;
            if (res.data) {
                this.setState({ hasDefaultQueryKeypair: true });
            } else {
                this.setState({ hasDefaultQueryKeypair: false });
            }
            return 0;
        });
    }

    handleQueryKeypairSelected() {
        this.setState({ hasDefaultQueryKeypair: true });
    }

    handleQueryKeypairCanceled() {
        this.props.history.goBack();
    }

    render() {
        const { props } = this;
        const {
            hasDefaultQueryKeypair,
        } = this.state;
        return (
            hasDefaultQueryKeypair === null
                ? null
                : (
                    !hasDefaultQueryKeypair
                        ? <QueryKeypairSelectDialog
                            handleQueryKeypairSelected={this.handleQueryKeypairSelected.bind(this)}
                            handleQueryKeypairCanceled={this.handleQueryKeypairCanceled.bind(this)}
                            open={true}
                        />
                        : <List
                            {...props}
                            filters={<FileFilter />}
                            bulkActions={false}
                            sort={{ field: "id", order: "DESC" }}
                            actions={<PostActions />}
                        >
                            <Responsive
                                small={
                                    <SimpleList
                                        primaryText={record => record.id}
                                        secondaryText={record => record.name}
                                    />
                                }
                                medium={
                                    <Datagrid>
                                        <TextField source="name" />
                                        <TextField source="size" />
                                        <DateField source="uploadTime" showTime />
                                        <ShowButton />
                                    </Datagrid>
                                }
                            />
                        </List>
                )
        );
    }
}

export default FileList;
