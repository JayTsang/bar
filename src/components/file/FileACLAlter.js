import React from "react";
import {
    Create,
    Edit,
} from "react-admin";
import { FileACLForm } from "./FileACLForm";

export const AccessAuthCreate = (props) => (
  <Create {...props}>
    <FileACLForm fileid={props.location.state ? props.location.state.fileID : null}/> 
  </Create>
);

export const AccessAuthEdit = (props) => (
  <Edit {...props}>
    <FileACLForm /> 
  </Edit>
);