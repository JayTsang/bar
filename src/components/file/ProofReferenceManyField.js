/* eslint-disable import/no-extraneous-dependencies */
import React, { Component } from "react";
import {
    TextField as MUITextField,
    MenuItem,
    Grid,
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import {
    ReferenceManyField,
    Datagrid,
    Pagination,
    TextField,
    GET_ONE,
} from "react-admin";
import { cloneDeep } from "lodash";
import { Crypto } from "rclink";
import {
    FileOperationTypeField,
    ProofStatusField,
    fileOpType,
    FILE_OP_TYPE_U,
    FILE_OP_TYPE_D,
    FILE_OP_TYPE_A,
    proofStatus,
    PROOF_STATUS_CONFIRMED,
    PROOF_STATUS_WAITING,
    PROOF_STATUS_FAILED,
} from "./ProofFields";
import settings from "../../settings";
import indexDataProvider from "../../dataprovider/ra-data-indexdb";
import getOriginDataToBeSigned from "../../dataprovider/originDataTBS";

const styles = () => ({
    textField: {
        minWidth: 150,
        marginBottom: 10,
        marginTop: 15,
    },
    longText: {
        wordBreak: "break-all",
    },
});

const getFilter = (filter) => {
    const newFilter = cloneDeep(filter);
    if (newFilter.operationType === "ALL") {
        newFilter.operationType = "";
    }
    if (newFilter.status === "ALL") {
        newFilter.status = "";
    }
    return newFilter;
};

class ProofReferenceManyField extends Component {
    constructor(props) {
        super(props);
        this.state = {
            operationType: "ALL",
            status: "ALL",
        };
    }

    async componentDidMount() {
        const defaultQueryKeypairResource = "defaultQueryKeypair";
        const defaultQueryKeypairID = 1;
        const defaultQueryKeypair = await indexDataProvider(GET_ONE, defaultQueryKeypairResource, {
            id: defaultQueryKeypairID,
        }).then(res => res.data);
        const sigAlg = "sha256";
        const certName = defaultQueryKeypair.name;
        const requesterID = defaultQueryKeypair.ownerCreditCode;
        const data = getOriginDataToBeSigned({
            certName,
            requesterID,
            sigAlg,
        });
        const signature = Crypto.Sign({
            prvKey: defaultQueryKeypair.kp.prvKeyPEM,
            data,
            alg: sigAlg,
        }).toString("hex");
        let wsUrl = `${settings.Server.wsBaseUrl}/proofs`;
        wsUrl = `${wsUrl}?sigAlg=${sigAlg}&signature=${signature}`;
        wsUrl = `${wsUrl}&requesterID=${requesterID}&certName=${certName}`;
        // eslint-disable-next-line no-undef
        const wsClient = new WebSocket(wsUrl, "echo-protocol");
        this.setState({ wsClient });
        wsClient.onerror = ((ev) => {
            console.error("websocket error: ", ev);
        });
        wsClient.onmessage = ((ev) => {
            const { refreshSwitch } = this.state;
            if (ev.data) this.setState({ refreshSwitch: !refreshSwitch });
        });
    }

    componentWillUnmount() {
        const { wsClient } = this.state;
        wsClient.close();
    }

    render() {
        const { classes } = this.props;
        const { refreshSwitch } = this.state;
        return (
            <div>
                <Grid container justify="flex-start" spacing={16}>
                    <Grid key="grid-item-1" item>
                        <MUITextField
                            id="filter-text-input-operatorID"
                            label="操作发起者信用代码"
                            // InputLabelProps={{ shrink: true }}
                            onChange={(e) => { this.setState({ operatorID: e.target.value }); }}
                            className={classes.textField}
                        />
                    </Grid>
                    <Grid key="grid-item-2" item>
                        <MUITextField
                            id="filter-text-input-operationType"
                            label="操作类型"
                            InputLabelProps={{ shrink: !!this.state.operationType }}
                            select
                            onChange={(e) => { this.setState({ operationType: e.target.value }); }}
                            value={this.state.operationType}
                            className={classes.textField}
                        >
                            <MenuItem key="ALL" value="ALL">全部</MenuItem>
                            <MenuItem key={FILE_OP_TYPE_U} value={FILE_OP_TYPE_U}>
                                {fileOpType[FILE_OP_TYPE_U].label}
                            </MenuItem>
                            <MenuItem key={FILE_OP_TYPE_D} value={FILE_OP_TYPE_D}>
                                {fileOpType[FILE_OP_TYPE_D].label}
                            </MenuItem>
                            <MenuItem key={FILE_OP_TYPE_A} value={FILE_OP_TYPE_A}>
                                {fileOpType[FILE_OP_TYPE_A].label}
                            </MenuItem>
                        </MUITextField>
                    </Grid>
                    <Grid key="grid-item-3" item>
                        <MUITextField
                            id="filter-text-input-status"
                            label="操作存证状态"
                            InputLabelProps={{ shrink: !!this.state.status }}
                            select
                            onChange={(e) => { this.setState({ status: e.target.value }); }}
                            value={this.state.status}
                            className={classes.textField}
                        >
                            <MenuItem key="ALL" value="ALL">全部</MenuItem>
                            <MenuItem key={PROOF_STATUS_CONFIRMED} value={PROOF_STATUS_CONFIRMED}>
                                {proofStatus[PROOF_STATUS_CONFIRMED]}
                            </MenuItem>
                            <MenuItem key={PROOF_STATUS_WAITING} value={PROOF_STATUS_WAITING}>
                                {proofStatus[PROOF_STATUS_WAITING]}
                            </MenuItem>
                            <MenuItem key={PROOF_STATUS_FAILED} value={PROOF_STATUS_FAILED}>
                                {proofStatus[PROOF_STATUS_FAILED]}
                            </MenuItem>
                        </MUITextField>
                    </Grid>
                </Grid>
                <div hidden>{refreshSwitch}</div>
                <ReferenceManyField
                    reference="proofs"
                    target="fileID"
                    addLabel={false}
                    sort={{ field: "operationType", order: "DESC" }}
                    perPage={10}
                    pagination={<Pagination />}
                    filter={getFilter(this.state)}
                    {...this.props}
                >
                    <Datagrid>
                        <FileOperationTypeField source="operationType" />
                        <TextField className={classes.longText} source="operatorID" />
                        <ProofStatusField source="status" />
                    </Datagrid>
                </ReferenceManyField>
            </div >
        );
    }
}

export default withStyles(styles)(ProofReferenceManyField);
