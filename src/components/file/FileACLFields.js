/*
 * Copyright 2019 Linkel Technology Co., Ltd, Beijing
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BA SIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import {
    Chip,
    Table, TableHead, TableRow, TableCell, TableBody,
    Grid,
    Card, CardContent,
    Typography,
} from "@material-ui/core";
import { withStyles } from '@material-ui/core/styles';
import { green, blue } from '@material-ui/core/colors';
import { Avatar } from "@material-ui/core";
import { mapKeys, isEmpty } from "lodash";

const styles = {
    actionGrid: {
        marginTop: 5,
        marginBottom: 5,
    },
};

export const FileAccessActionField = withStyles(styles)(({ classes, source, record = {} }) => {
    const actionLabels = [];
    const actionAvatarChars = [];
    if (record[source].includes("download")) {
        actionLabels.push("下载");
        actionAvatarChars.push("D");
    }
    // if(record.authority & 0b0100) authoritys.push("删除");
    if (record[source].includes("accessAuthRead")) {
        actionLabels.push("访问权限读");
        actionAvatarChars.push("AR");
    }
    if (record[source].includes("accessAuthWrite")) {
        actionLabels.push("访问权限写");
        actionAvatarChars.push("AW");
    }
    return (
        <Grid className={classes.actionGrid} container justify="flex-start" spacing={8}>
            {
                actionLabels.map((element, index) => (
                    <Grid key={`grid-item-${index}`} item>
                        <Chip
                            label={element}
                            avatar={<Avatar>{actionAvatarChars[index]}</Avatar>}
                        />
                    </Grid>
                ))
            }
        </Grid>
    );
});

export const FileAccessRestrictionTypeField = ({ record = {} }) => {
    const restrictionTypes = {
        UNRESTRICTED: {
            label: "无限制",
            color: green[500],
        },
        TIME_RANGE: {
            label: "时间段",
            color: blue[500],
        },
        COUNT: {
            label: "次数",
            color: blue[500],
        },
    };

    const RestrictionTypeChip = withStyles({
        root: {
            backgroundColor: restrictionTypes[record.restrictionType].color,
        },
        label: {
            color: "white"
        }
    })(Chip);

    return <RestrictionTypeChip label={restrictionTypes[record.restrictionType].label} />
};

export const FileAccessRestrictionContentPanel = ({ record = {} }) => {
    // record.restrictionContent = { from: new Date().toISOString(), to: new Date().toISOString() }
    const restrictionContent = mapKeys(record.restrictionContent, (v, k) => {
        switch (k) {
            case "from":
                return "起始时间";
            case "to":
                return "终止时间";
            case "initialCount":
                return "初始次数";
            case "remainCount":
                return "剩余次数";
            default:
                return "其他";
        }
    });
    return (
        <Card>
            <CardContent>
                <Typography variant="body1" color="default" gutterBottom >
                    访问限定条件内容
                </Typography>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>限定条件</TableCell>
                            <TableCell>限定值</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            !isEmpty(restrictionContent) ?
                                Object.keys(restrictionContent).map(key => (
                                    <TableRow>
                                        <TableCell>{key}</TableCell>
                                        <TableCell>{restrictionContent[key]}</TableCell>
                                    </TableRow>
                                )) :
                                <TableRow>
                                    <TableCell>无</TableCell>
                                    <TableCell>无</TableCell>
                                </TableRow>
                        }
                    </TableBody>
                </Table>
            </CardContent>
        </Card>
    );
};