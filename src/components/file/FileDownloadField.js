import React, { useState } from "react";
import { Button, withStyles } from "@material-ui/core";
import fileDownload from "js-file-download";
import { connect } from "react-redux";
import { FileDownload as FileDownloadIcon } from "@material-ui/icons";
import {
    showNotification,
} from "react-admin";
import KeypairSelectDialog from "./KeypairSelectDialog";
import fileStorageServerRestProvider from "../../dataprovider/ra-data-fileStorageServer-rest";

const FileDownloadField = ({ classes, record = {}, showNotification }) => {
    const [open, setOpen] = useState(false);
    const handleOk = (keypair, password) => {
        fileStorageServerRestProvider("DOWNLOAD", "files", { id: record.fileID, keypair, password })
            .then((res) => {
                setOpen(false);
                fileDownload(res.data, record.name);
            }).catch(err => showNotification(err.message, "warning"));
    };
    const handleCancel = () => {
        setOpen(false);
    };

    return (
        <div>
            <Button
                variant="contained"
                color="primary"
                className={classes.downloadButton}
                onClick={() => setOpen(true)}
                id="download"
            >
                <FileDownloadIcon />
                下载文件
            </Button>
            <KeypairSelectDialog
                open={open}
                title="选择签名密钥"
                textContent="该签名密钥将被用于对文件下载请求信息的签名，请勿使用检索密钥"
                handleOk={handleOk}
                handleCancel={handleCancel}
                showPasswordInut={true}
            />
        </div>
    );
};

export default connect(undefined, {
    showNotification,
})(withStyles({
    downloadButton: {
        marginTop: 16,
        marginBottom: 8,
    },
})(FileDownloadField));
