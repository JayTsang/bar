/*
 * Copyright  2018 Linkel Technology Co., Ltd, Beijing
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BA SIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from "react";
import {
    Show,
    Create,
    TabbedShowLayout,
    Tab,
    FileInput,
    FileField,
    TextField,
    SimpleForm,
    TextInput,
    SelectInput,
    Toolbar,
    SaveButton,
    ReferenceInput,
    FormDataConsumer,
    required,
} from "react-admin/lib";
import { FileUpload } from "@material-ui/icons";
import ProofReferenceManyField from "./ProofReferenceManyField";
import FileACLReferenceManyField from "./FileACLReferenceManyField";
import FileDownloadField from "./FileDownloadField";

export { default as FileList } from "./FileList";

const FileTitle = ({ record }) => <span> {record ? `"${record.fileID}"` : ""}</span>;

const FileCreateToolbar = props => (
    <Toolbar {...props}>
        <SaveButton
            label="上传"
            icon={<FileUpload />}
            redirect="list"
        />
    </Toolbar>
);

// TODO: 去除账号选择UI，账号由选择默认检索密钥时确定
export const FileCreate = props => (
    <Create {...props}>
        <SimpleForm toolbar={<FileCreateToolbar />}>
            <FileInput multiple source="filesToUpload" validate={required()}>
                <FileField source="src" title="title" />
            </FileInput>
            <ReferenceInput label="账号" source="requesterID"
                reference="accounts" validate={required()} >
                <SelectInput optionText="name" optionValue="creditCode" />
            </ReferenceInput>
            <FormDataConsumer>
                {
                    ({ formData, ...rest }) => (
                        <ReferenceInput label="密钥对" source="keypairID"
                            reference="keypairs"
                            filter={{ ownerCreditCode: formData.requesterID ? formData.requesterID : "none" }}
                            validate={required()}
                            {...rest}>
                            <SelectInput optionText="name" optionValue="id" {...rest} />
                        </ReferenceInput>
                    )
                }
            </FormDataConsumer>
            <TextInput label="密钥密码" source="keypairPwd" defaultValue="" type="password" />
        </SimpleForm>
    </Create>
);
// TODO: 使用统一的Dialog让所有文件操作时进行密钥选择, 
// 可参考react-admin api doc: Altering the Form Values before Submitting
// (https://marmelab.com/react-admin/Actions.html#altering-the-form-values-before-submitting)
export const FileShow = props => (
    <Show title={<FileTitle />} {...props}>
        <TabbedShowLayout>
            <Tab label="resources.files.tabs.tab1">
                <TextField source="fileID" />
                <TextField source="hash" id="hash"/>
                <TextField source="hashAlg" id="hashAlg"/>
                <TextField source="name" id="name"/>
                <TextField source="size" />
                <TextField source="uploadTime" />
                <TextField source="storageTime" />
                <FileDownloadField />
            </Tab>
            <Tab label="resources.files.tabs.tab2" id="showProofs">
                <ProofReferenceManyField />
            </Tab>
            <Tab label="resources.files.tabs.tab3" id="showACLs">
                <FileACLReferenceManyField id="showACLs"/>
            </Tab>
        </TabbedShowLayout>
    </Show>
);
