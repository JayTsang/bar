import React, { Component } from "react";
import {
    TextField as MUITextField,
    MenuItem,
    Grid,
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import {
    ReferenceManyField,
    Datagrid,
    Pagination,
    TextField,
    EditButton,
    CreateButton,
} from "react-admin";
import { cloneDeep } from "lodash";
import {
    FileAccessActionField,
    FileAccessRestrictionTypeField,
    FileAccessRestrictionContentPanel,
} from "./FileACLFields";

const styles = () => ({
    barField: {
        minWidth: 150,
    },
    bar: {
        marginBottom: 10,
        marginTop: 15,
    },
    longText: {
        wordBreak: "break-all",
    },
});

const getFilter = (filter) => {
    const newFilter = cloneDeep(filter);
    if (newFilter.accessAuthority.length === 0) {
        delete newFilter.accessAuthority;
    } else {
        newFilter.accessAuthority = newFilter.accessAuthority.reduce(
            // eslint-disable-next-line no-bitwise
            (acc, cur) => acc | cur,
        );
    }
    if (newFilter.accessRestrictionType === "ALL") {
        delete newFilter.accessRestrictionType;
    }
    return newFilter;
};

class FileACLReferenceManyField extends Component {
    constructor(props) {
        super(props);
        this.state = {
            accessAuthority: [],
        };
    }

    render() {
        const { classes } = this.props;
        const { accessAuthority = 0, accessRestrictionType = "ALL" } = this.state;
        return (
            <div>
                <Grid container className={classes.bar}>
                    <Grid item xs={6}>
                        <Grid container justify="flex-start" spacing={16}>
                            <Grid key="grid-item-1" item>
                                <MUITextField
                                    id="filter-text-input-objectUserID"
                                    label="目标用户信用代码"
                                    onChange={(e) => { 
                                        this.setState({ objectUserID: e.target.value }); 
                                    }}
                                    className={classes.barField}
                                />
                            </Grid>
                            <Grid key="grid-item-2" item>
                                <MUITextField
                                    id="filter-text-input-accessAuthority"
                                    label="访问权限"
                                    InputLabelProps={{ shrink: accessAuthority.length > 0 }}
                                    select
                                    SelectProps={{ multiple: true }}
                                    onChange={(e) => {
                                        this.setState({ accessAuthority: e.target.value });
                                    }}
                                    value={accessAuthority}
                                    className={classes.barField}
                                >
                                    <MenuItem key="download" value={0b1000}>下载</MenuItem>
                                    <MenuItem key="accessAuthRead" value={0b0010}>访问权限读</MenuItem>
                                    <MenuItem key="accessAuthWrite" value={0b0001}>访问权限写</MenuItem>
                                </MUITextField>
                            </Grid>
                            <Grid key="grid-item-3" item>
                                <MUITextField
                                    id="filter-text-input-accessRestrictionType"
                                    label="访问限定条件类型"
                                    InputLabelProps={{ shrink: !!accessRestrictionType }}
                                    select
                                    onChange={(e) => { 
                                        this.setState({ accessRestrictionType: e.target.value }); 
                                    }}
                                    value={accessRestrictionType}
                                    className={classes.barField}
                                >
                                    <MenuItem key="ALL" value="ALL">
                                        全部
                                    </MenuItem>
                                    <MenuItem key="UNRESTRICTED" value="UNRESTRICTED">
                                        无限制
                                    </MenuItem>
                                    <MenuItem key="TIME_RANGE" value="TIME_RANGE">
                                        时间段
                                    </MenuItem>
                                    <MenuItem key="COUNT" value="COUNT">
                                        次数
                                    </MenuItem>
                                </MUITextField>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={6} >
                        <Grid container justify="flex-end" spacing={16}>
                            <Grid key="grid-item-4" item>
                                <CreateButton
                                    to={{
                                        pathname: "/accessAuths/create",
                                        state: {
                                            fileID: this.props.record.id,
                                        },
                                    }}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <ReferenceManyField
                    reference="accessAuths"
                    target="fileID"
                    addLabel={false}
                    sort={{ field: "authority", order: "DESC" }}
                    perPage={10}
                    pagination={<Pagination />}
                    filter={getFilter(this.state)}
                    {...this.props}
                >
                    <Datagrid expand={<FileAccessRestrictionContentPanel />}>
                        <TextField className={classes.longText} source="userID" />
                        <FileAccessActionField source="authority" />
                        <FileAccessRestrictionTypeField source="restrictionType" />
                        <FileAccessActionField source="postActStoreProof" />
                        <EditButton />
                    </Datagrid>
                </ReferenceManyField>
            </div>
        );
    }
}

export default withStyles(styles)(FileACLReferenceManyField);
