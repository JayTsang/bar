/* eslint-disable no-nested-ternary */
import React, { Component } from "react";
import { GraphQLClient } from "graphql-request";
import {
    LinearProgress,
    Divider,
    Chip,
} from "@material-ui/core";
import PropType from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { green, grey } from "@material-ui/core/colors";
import settings from "../../settings";

const graphqlClient = new GraphQLClient(`${settings.Prisma.endpoint}`);
const DELIMITER = "-COMBDELI-";

const styles = () => ({
    divider: {
        height: 1,
        marginTop: 5,
        marginBottom: 5,
        backgroundColor: "rgba(0,0,0,0.5)",
    },
    chipGood: {
        backgroundColor: green[500],
        color: grey[50],
    },
});

class CertificateStatusField extends Component {
    constructor(props) {
        super(props);
        this.state = {
            registrationStatus: null,
            enabledStatus: null,
            restrictStatus: null,
        };
    }

    componentDidMount() {
        const { ownerCreditCode, name } = this.props.record;
        graphqlClient.request(`
            {
               cert(where: {
                   creditCodeCombineName: "${ownerCreditCode}${DELIMITER}${name}"
               }){
                   id
                   status
                   isRestricted
               }
            }
        `).then((res) => {
            if (res.cert) {
                this.setState({ registrationStatus: true });
                this.setState({ enabledStatus: !!(res.cert.status) });
                this.setState({ restrictStatus: !!(res.cert.isRestricted) });
            } else {
                this.setState({ registrationStatus: false });
                this.setState({ enabledStatus: false });
            }
            return 0;
        });
    }

    render() {
        const { classes } = this.props;
        return (
            <div>
                {
                    this.state.registrationStatus === null
                        ? <LinearProgress />
                        : (
                            this.state.registrationStatus
                                ? <Chip
                                    label={<span id="registerStatus">已注册到区块链</span>}
                                    className={classes.chipGood}
                                />
                                : <Chip
                                    label={<span id="registerStatus">未注册到区块链</span>}
                                />
                        )
                }
                {
                    this.state.registrationStatus
                        ? <div>
                            <Divider variant="fullWidth" className={classes.divider} />
                            {
                                this.state.enabledStatus === null
                                    ? <LinearProgress />
                                    : (
                                        this.state.enabledStatus
                                            ? <Chip
                                                label={<span id="enabledStatus">已启用</span>}
                                                className={classes.chipGood}
                                            />
                                            : <Chip label={<span id="enabledStatus">已禁用</span>} />
                                    )
                            }
                        </div>
                        : null
                }
                {
                    this.state.registrationStatus
                        ? <div>
                            <Divider variant="fullWidth" className={classes.divider} />
                            {
                                this.state.restrictStatus === null
                                    ? <LinearProgress />
                                    : (
                                        this.state.restrictStatus
                                            ? <Chip
                                                label={<span id="restrictStatus">只用于文件信息检索</span>}

                                            />
                                            : <Chip
                                                label={<span id="restrictStatus">可用于所有文件操作</span>}
                                                className={classes.chipGood}
                                            />
                                    )
                            }
                        </div>
                        : null
                }
            </div >
        );
    }
}

const CertificateStatusFieldWithStyles = withStyles(styles)(CertificateStatusField);

CertificateStatusFieldWithStyles.defaultProps = {
    addLabel: true,
};

CertificateStatusFieldWithStyles.propTypes = {
    record: PropType.object.isRequired,
};

export default CertificateStatusFieldWithStyles;
