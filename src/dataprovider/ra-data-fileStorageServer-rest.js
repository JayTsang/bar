import {
    GET_LIST,
    GET_ONE,
    GET_MANY,
    GET_MANY_REFERENCE,
    CREATE,
    UPDATE,
    UPDATE_MANY,
    DELETE,
    DELETE_MANY,
} from "react-admin";
import { stringify } from "query-string";
import { cloneDeep } from 'lodash';
import { GetHashVal, Sign, ImportKey, GetKeyPEM } from 'rclink/lib/crypto';
import getOriginDataToBeSigned from "./originDataTBS";
import indexDataProvider from "./ra-data-indexdb";

const log = (type, resource, params) => {
    console.log(`fileStorageServer-rest request:\n type: ${type}\n resource: ${resource}\n params: ${JSON.stringify(params)}`);
};

const signAlg = "sha256";
const getDefaultQueryKeypair = () => indexDataProvider(GET_ONE, "defaultQueryKeypair", { id: 1 }).then((res) => {
        if (!res.data) return null;
        const { ownerCreditCode, name } = res.data;
        return {
            requesterID: ownerCreditCode,
            certName: name,
            prvKey: res.data.kp.prvKeyPEM,
        };
    });

const [
    RESOURCE_FILES,
    RESOURCE_PROOFS,
    RESOURCE_ACCESSAUTHS,
] = ["files", "proofs", "accessAuths"];

const resCollectionDataName = {
    [RESOURCE_FILES]: "fileList",
    [RESOURCE_PROOFS]: "proofs",
    [RESOURCE_ACCESSAUTHS]: "fileACLs",
};

const resSingleDataName = {
    [RESOURCE_FILES]: "file",
    [RESOURCE_PROOFS]: "proof",
    [RESOURCE_ACCESSAUTHS]: "fileACL",
};

const resDataIdField = {
    [RESOURCE_FILES]: "fileID",
    [RESOURCE_PROOFS]: "id",
    [RESOURCE_ACCESSAUTHS]: "fileIDCombineUserID",
};

const [
    FILEACL_ACTION_DOWNLOAD_N,
    FILEACL_ACTION_ACCESSAUTH_READ_N,
    FILEACL_ACTION_ACCESSAUTH_WRITE_N,
] = [
    0b1000, 0b0010, 0b0001,
];
const [
    FILEACL_ACTION_DOWNLOAD_S,
    FILEACL_ACTION_ACCESSAUTH_READ_S,
    FILEACL_ACTION_ACCESSAUTH_WRITE_S,
] = [
    "download", "accessAuthRead", "accessAuthWrite",
];

const converterFileACLAction = (fileACL) => {
    const { authority, postActStoreProof } = fileACL;
    fileACL.authority = [];
    fileACL.postActStoreProof = [];
    (authority & FILEACL_ACTION_DOWNLOAD_N)
        && fileACL.authority.push(FILEACL_ACTION_DOWNLOAD_S);
    (postActStoreProof & FILEACL_ACTION_DOWNLOAD_N)
        && fileACL.postActStoreProof.push(FILEACL_ACTION_DOWNLOAD_S);
    (authority & FILEACL_ACTION_ACCESSAUTH_READ_N)
        && fileACL.authority.push(FILEACL_ACTION_ACCESSAUTH_READ_S);
    (postActStoreProof & FILEACL_ACTION_ACCESSAUTH_READ_N)
        && fileACL.postActStoreProof.push(FILEACL_ACTION_ACCESSAUTH_READ_S);
    (authority & FILEACL_ACTION_ACCESSAUTH_WRITE_N)
        && fileACL.authority.push(FILEACL_ACTION_ACCESSAUTH_WRITE_S);
    (postActStoreProof & FILEACL_ACTION_ACCESSAUTH_WRITE_N)
        && fileACL.postActStoreProof.push(FILEACL_ACTION_ACCESSAUTH_WRITE_S);
};

const getCollectionData = async (type, resource, params) => {
    const filter = cloneDeep(params.filter);
    const converter = (value) => {
        value.id = value[resDataIdField[resource]]; // 后续查询单个资源时需要提供转换后的ID
        if (resource === RESOURCE_ACCESSAUTHS) converterFileACLAction(value); // 将表示权限/访问动作的Number转为Array[String]
        return value;
    };
    switch (type) {
        case GET_MANY_REFERENCE:
            filter[params.target] = params.id;
            break;
        default:
            break;
    }

    // Convert the filter fields "uploadTimeFrom" and "uploadTimeTo" 
    // from UTC string to unix timestamp
    const { uploadTimeFrom, uploadTimeTo } = filter;
    if (uploadTimeFrom) filter.uploadTimeFrom = new Date(uploadTimeFrom).getTime();
    if (uploadTimeTo) filter.uploadTimeTo = new Date(uploadTimeTo).getTime();

    const { requesterID, certName, prvKey } = await getDefaultQueryKeypair();

    const { page, perPage } = params.pagination;
    const { field, order } = params.sort;
    const urlPath = `/${resource}`;
    const query = {
        page,
        perPage,
        sortBy: `${field}_${order}`,
        ...filter,
        requesterID,
        certName,
        timestamp: Date.now(),
        signAlg,
        urlPath,
    };
    const signature = Sign({
        prvKey, data: getOriginDataToBeSigned(query), alg: signAlg,
    }).toString("hex");
    query.signature = signature;
    const url = `${urlPath}?${stringify(query)}`;
    return fetch(url)
        .then((response) => {
            if (response.status === 401) {throw new Error("Unauthorized");}
            return response.json();
        }).then((res) => {
            const collection = res[resCollectionDataName[resource]].map(converter);
            return {
                data: collection,
                total: res.totalCount,
            };
        }).catch(() => (
            {
                data: [],
                total: 0,
            }
        ));
};

const getOneData = async (type, resource, params) => {
    let urlPath;
    let requesterID;
    let certName;
    let prvKey;
    switch (type) {
        case GET_ONE: {
            urlPath = `/${resource}/${params.id}`;
            const queryKeypair = await getDefaultQueryKeypair();
            requesterID = queryKeypair.requesterID;
            certName = queryKeypair.certName;
            prvKey = queryKeypair.prvKey;
            break;
        }
        case "DOWNLOAD": {
            urlPath = `/${resource}/${params.id}/download`;
            const { keypair, password } = params;
            requesterID = keypair.ownerCreditCode;
            certName = keypair.name;
            const prvKeyObj = ImportKey(keypair.kp.prvKeyPEM, password);
            if (prvKeyObj.pubKeyHex === undefined) {
                prvKeyObj.pubKeyHex = ImportKey(keypair.kp.pubKeyPEM).pubKeyHex;
            }
            prvKey = GetKeyPEM(prvKeyObj);
            break;
        }
        default:
            break;
    }
    const query = {
        requesterID,
        certName,
        timestamp: Date.now(),
        signAlg,
        urlPath,
    };
    const signature = Sign({
        prvKey, data: getOriginDataToBeSigned(query), alg: signAlg,
    }).toString("hex");
    query.signature = signature;
    const url = `${urlPath}?${stringify(query)}`;

    if (type === "DOWNLOAD") {return fetch(url)
            .then(async (response) => { 
                if(response.status !== 200) {
                    const body = await response.json();
                    const errorMsg = `${body.message}:\n${JSON.stringify(body.errors.map(e => e.msg))}`;
                    throw new Error(errorMsg);
                }
                else return response.blob();
            })
            .then(res => ({ data: res }));}
    return fetch(url)
        .then(response => response.json())
        .then((res) => {
            const data = res[resSingleDataName[resource]];
            data.id = data[resDataIdField[resource]]; // 保持与GET_List的结果数据项id一致
            if (resource === RESOURCE_ACCESSAUTHS) converterFileACLAction(data);
            return { data };
        });
};

const createFile = async (resource, params) => {
    const {
 filesToUpload, requesterID, keypairID, password 
} = params.data;

    const hashAlg = "sha256";

    const files = filesToUpload.map(value => value.rawFile);
    const fileHashPromises = filesToUpload.map(value => new Response(value.rawFile)
        .arrayBuffer()
        .then(file => GetHashVal({ data: Buffer.from(file), alg: hashAlg })
            .toString("hex"),
        ),
    );
    const fileHashes = await Promise.all(fileHashPromises)
        .then(fileHashArray => JSON.stringify(fileHashArray));

    const { prvKey, pubKey, certName } = await indexDataProvider(GET_ONE, "keypairs", { id: keypairID })
        .then(keypair => ({
            prvKey: keypair.data.kp.prvKeyPEM,
            pubKey: keypair.data.kp.pubKeyPEM,
            certName: keypair.data.name,
        })).catch((e) => Promise.reject(e));
    const prvKeyObj = ImportKey(prvKey, password);
    if (prvKeyObj.pubKeyHex === undefined) prvKeyObj.pubKeyHex = ImportKey(pubKey).pubKeyHex;

    const urlPath = `/${resource}`;
    const body = {
        fileHashes,
        hashAlg,
        requesterID,
        certName,
        timestamp: Date.now(),
        signAlg,
        urlPath,
    };
    const signature = Sign({
        prvKey: GetKeyPEM(prvKeyObj), data: getOriginDataToBeSigned(body), alg: signAlg,
    }).toString("hex");
    body.signature = signature;

    const formData = new FormData();
    files.forEach((file) => {
        formData.append("files", file);
    });

    Object.keys(body).forEach((key) => {
        formData.append(key, body[key]);
    });

    return fetch(urlPath, {
        method: "POST",
        body: formData,
    })
        .then(response => response.json())
        .then((res) => {
            if (res.errors) {throw new Error(JSON.stringify(res.errors));}
            return { data: { id: 0, ...res } };
        })
        .catch((e) => Promise.reject(e));
};

const createOrUpdateAccessAuth = async (type, resource, params) => {
    const {
        file,
        userID,
        authority,
        restrictionType,
        restrictionContent,
        postActStoreProof,
        requesterID,
        keypairID,
        password,
    } = params.data;
    const url = type === CREATE
        ? `/${RESOURCE_FILES}/${file.fileID}/accessAuths`
        : `/${RESOURCE_FILES}/${file.fileID}/accessAuths/${file.fileID}-${userID}`;

    const { prvKey, pubKey, certName } = await indexDataProvider(GET_ONE, "keypairs", { id: keypairID })
        .then(keypair => ({
            prvKey: keypair.data.kp.prvKeyPEM,
            pubKey: keypair.data.kp.pubKeyPEM,
            certName: keypair.data.name,
        })).catch((e) => Promise.reject(e));
    const prvKeyObj = ImportKey(prvKey, password);
    if (prvKeyObj.pubKeyHex === undefined) prvKeyObj.pubKeyHex = ImportKey(pubKey).pubKeyHex;

    const body = {
        objectUserID: userID,
        accessRestrictionType: restrictionType,
        requesterID,
        timestamp: Date.now(),
        signAlg,
        certName,
        urlPath: url,
    };
    let authorityNumber = 0;
    let postActStoreProofNumber = 0;
    if (authority.includes(FILEACL_ACTION_DOWNLOAD_S)) {
        authorityNumber |= FILEACL_ACTION_DOWNLOAD_N;
    }
    if (postActStoreProof.includes(FILEACL_ACTION_DOWNLOAD_S)) {
        postActStoreProofNumber |= FILEACL_ACTION_DOWNLOAD_N;
    }
    if (authority.includes(FILEACL_ACTION_ACCESSAUTH_READ_S)) {
        authorityNumber |= FILEACL_ACTION_ACCESSAUTH_READ_N;
    }
    if (postActStoreProof.includes(FILEACL_ACTION_ACCESSAUTH_READ_S)) {
        postActStoreProofNumber |= FILEACL_ACTION_ACCESSAUTH_READ_N;
    }
    if (authority.includes(FILEACL_ACTION_ACCESSAUTH_WRITE_S)) {
        authorityNumber |= FILEACL_ACTION_ACCESSAUTH_WRITE_N;
    }
    if (postActStoreProof.includes(FILEACL_ACTION_ACCESSAUTH_WRITE_S)) {
        postActStoreProofNumber |= FILEACL_ACTION_ACCESSAUTH_WRITE_N;
    }
    body.accessAuthority = authorityNumber;
    body.accessPostProofStorage = postActStoreProofNumber;

    let accessRestrictionContent;
    if (restrictionType === "UNRESTRICTED") accessRestrictionContent = {};
    if (restrictionType === "TIME_RANGE") accessRestrictionContent = restrictionContent;
    if (restrictionType === "COUNT") accessRestrictionContent = { ...restrictionContent, remainCount: restrictionContent.initialCount };
    body.accessRestrictionContent = JSON.stringify(accessRestrictionContent);

    const signature = Sign({
        prvKey: GetKeyPEM(prvKeyObj), data: getOriginDataToBeSigned(body), alg: signAlg,
    }).toString("hex");
    body.signature = signature;

    return fetch(url, {
        method: type === CREATE ? "POST" : "PUT",
        body: JSON.stringify(body),
        headers: new Headers({ "Content-Type": "application/json" }),
    }).then(response => response.json())
        .then((res) => {
            const data = res[resSingleDataName[resource]];
            data.id = data[resDataIdField[resource]];
            converterFileACLAction(data);
            return { data };
        }).catch(e => Promise.reject(e));
};

const create = (resource, params) => {
    switch (resource) {
        case RESOURCE_FILES:
            return createFile(resource, params);
        case RESOURCE_ACCESSAUTHS:
            return createOrUpdateAccessAuth(CREATE, resource, params);
        default:
            return Promise.reject(new Error(`Unsupported resource name ${resource} to create`));
    }
};

const update = (resource, params) => {
    switch (resource) {
        case RESOURCE_ACCESSAUTHS:
            return createOrUpdateAccessAuth(UPDATE, resource, params);
        default:
            return Promise.reject(new Error(`Unsupported resource name ${resource} to update`));
    }
};

export default (type, resource, params) => {
    log(type, resource, params);

    switch (type) {
        case GET_MANY_REFERENCE:
        case GET_LIST:
            return getCollectionData(type, resource, params);
        case GET_ONE:
        case "DOWNLOAD":
            return getOneData(type, resource, params);
        case CREATE:
            return create(resource, params);
        case UPDATE:
            return update(resource, params);
        default:
            break;
    }
};
