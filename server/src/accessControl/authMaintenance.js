import _ from "lodash";
import { prisma } from "../db/generated/prisma-client";
import { AccessRestrictionEnumTypes } from "../validate/enumValues";
import { COMBINATION_DELIMITER } from "../const";

export const prismaFragmentFileACLWithFileID = `
    fragment FileACLWithFileID on fileACL {
        fileIDCombineUserID 
        userID
        authority
        restrictionType
        restrictionContent
        postActStoreProof
        file {
            fileID
        }
    }
`;

export const getOneFileACL = (fileIDCombineUserID) => {
    if (!_.isString(fileIDCombineUserID)) {
        return Promise.reject(new TypeError("Bad param(s) to call getOneFileACL function, require string"));
    }
    return prisma.fileACL({
        fileIDCombineUserID,
    }).$fragment(prismaFragmentFileACLWithFileID);
};

export const getFileACLCollection = ({
    filter: {
        fileID = "",
        objectUserID = "",
        accessAuthority = -1,
        accessRestrictionType = "",
        accessPostProofStorage = -1,
    } = {},
    orderBy = "userID_ASC",
    pagination: { page = 1, perPage = 10 } = { page: 1, perPage: 10 },
} = {}) => {
    if (!_.isString(fileID)
        || !_.isString(objectUserID)
        || (accessAuthority !== -1 && !_.inRange(accessAuthority, 0, 16))
        || (accessRestrictionType !== "" && !_.includes(AccessRestrictionEnumTypes, accessRestrictionType))
        || (accessPostProofStorage !== -1 && !_.inRange(accessPostProofStorage, 0, 16))
        || (!_.endsWith(orderBy, "_ASC") && !_.endsWith(orderBy, "_DESC"))
        || !_.inRange(page, 1, 2 ** 31) || !_.inRange(perPage, 1, 2 ** 31)) {
        return Promise.reject(new TypeError("Bad param(s) to call getFileACLCollection function"));
    }
    const prismaFilter = { AND: [] };
    if (fileID !== "") prismaFilter.AND.push({ file: { fileID } });
    if (objectUserID !== "") prismaFilter.AND.push({ userID_contains: objectUserID });
    if (accessAuthority !== -1) { // 计算包含该权限的所有权限值
        const authoritys = [accessAuthority];
        let count = 1;
        let iterator = 0b0001;
        while (iterator < 16) {
            // eslint-disable-next-line no-bitwise
            if (!(accessAuthority & iterator)) {
                for (let i = 0; i < count; i++) {
                    // eslint-disable-next-line no-bitwise
                    authoritys.push(authoritys[i] | iterator);
                }
                count = authoritys.length;
            }
            // eslint-disable-next-line no-bitwise
            iterator <<= 1;
        }
        prismaFilter.AND.push({ authority_in: authoritys });
    }
    if (accessRestrictionType !== "") prismaFilter.AND.push({ restrictionType: accessRestrictionType });
    if (accessPostProofStorage !== -1) {
        prismaFilter.AND.push({ 
            postActStoreProof: accessPostProofStorage,
        });
    }
    return prisma.fileACLs({
        where: prismaFilter,
        orderBy, 
        skip: perPage * (page - 1),
        first: perPage,
    }).$fragment(prismaFragmentFileACLWithFileID)
        .then(fileACLs => prisma.fileACLsConnection({
            where: prismaFilter,
        }).aggregate()
            .count()
            .then(totalCount => ({ fileACLs, totalCount })),
        );
};

export const createFileACL = ({
    fileID, objectUserID, accessAuthority, 
    accessRestrictionType, accessRestrictionContent,
    accessPostProofStorage,
}) => {
    if (!_.isString(fileID) || !_.isString(objectUserID)
        || !_.inRange(accessAuthority, 0, 16)
        || _.indexOf(AccessRestrictionEnumTypes, 
            accessRestrictionType) === -1 
        || _.isUndefined(accessRestrictionContent) || _.isNull(accessRestrictionContent)
        || !_.inRange(accessPostProofStorage, 0, 16)
    ) {
        return Promise.reject(new Error("Bad param(s) to call createFileACL function"));
    }
    return prisma.createFileACL({
        userID: objectUserID,
        fileIDCombineUserID: `${fileID}${COMBINATION_DELIMITER}${objectUserID}`,
        authority: accessAuthority,
        restrictionType: accessRestrictionType,
        restrictionContent: accessRestrictionContent,
        postActStoreProof: accessPostProofStorage,
        file: {
            connect: {
                fileID,
            },
        },
    }).$fragment(prismaFragmentFileACLWithFileID);
};

export const updateFileACL = ({
    accessAuthID, accessAuthority, 
    accessRestrictionType, accessRestrictionContent,
    accessPostProofStorage,
}) => {
    if (!_.isString(accessAuthID)
        || (accessAuthority && !_.inRange(accessAuthority, 0, 16))
        || (accessRestrictionType && _.indexOf(AccessRestrictionEnumTypes, 
            accessRestrictionType) === -1)
        || (accessRestrictionContent && !_.isPlainObject(accessRestrictionContent))
        || (accessPostProofStorage && !_.inRange(accessPostProofStorage, 0, 16))
    ) {
        return Promise.reject(new Error("Bad param(s) to call updateFileACL function"));
    }
    const updateData = {};
    if (accessAuthority) updateData.authority = accessAuthority;
    if (accessRestrictionType) updateData.restrictionType = accessRestrictionType;
    if (accessRestrictionContent) updateData.restrictionContent = accessRestrictionContent;
    if (accessPostProofStorage) updateData.postActStoreProof = accessPostProofStorage;
    return prisma.updateFileACL({
        where: {
            fileIDCombineUserID: accessAuthID,
        },
        data: updateData,
    }).$fragment(prismaFragmentFileACLWithFileID);
};

// export const createOrUpdateFileACL = ({
//     fileID, objectUserID, accessAuthority, accessRestrictionType,
//     accessRestrictionContent, accessPostProofStorage,
// }) => {
//     if (!_.isString(fileID) || !_.isString(objectUserID)
//         || !_.inRange(accessAuthority, 0, 16)
//         || _.indexOf(AccessRestrictionType, 
//             accessRestrictionType) === -1 
//         || _.isUndefined(accessRestrictionContent) || _.isNull(accessRestrictionContent)
//         || !_.inRange(accessPostProofStorage, 0, 16)
//     ) {
//         return Promise.reject(new Error("Bad param(s) to call createOrUpdateFileACL function"));
//     }

//     return prisma.upsertFileACL({
//         where: {
//             fileIDCombineUserID: `${fileID}-${objectUserID}`,
//         },
//         update: {
//             authority: accessAuthority,
//             restrictionType: accessRestrictionType,
//             restrictionContent: accessRestrictionContent,
//             postActStoreProof: accessPostProofStorage,
//         },
//         create: {
//             userID: objectUserID,
//             fileIDCombineUserID: `${fileID}-${objectUserID}`,
//             authority: accessAuthority,
//             restrictionType: accessRestrictionType,
//             restrictionContent: accessRestrictionContent,
//             postActStoreProof: accessPostProofStorage,
//             file: {
//                 connect: {
//                     fileID,
//                 },
//             },
//         },
//     });
// }; 
