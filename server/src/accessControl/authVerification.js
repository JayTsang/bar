/* eslint-disable no-bitwise */
import _ from "lodash";
import { getOneFileACL } from "./authMaintenance";
import { getFile } from "../fileAccess/fileMetaMaintenance";
import serverInnerLogger from "../log/serverInnerLog";
import { COMBINATION_DELIMITER } from "../const";

const downloadOp = "download";
const authQueryOp = "authRead";
const authUpdateOp = "authWrite";

const prismaFragmentFileACLOnlyWithFileID = `
    fragment FileACLOnlyWithFileID on fileACL {
        file {
            fileID
        }
    }`;

/**
 * File access authority verification function
 * @param {Object} requestInfo - 用于权限验证的请求信息
 * @param {String} requestInfo.fileID - 文件资源标识
 * @param {String} requestInfo.userID - 用户标识
 * @param {String} requestInfo.operation - 操作类型，可为:"download","authRead", "authWrite"三者之一
 * @returns {Promise<Boolean>} - 表示权限验证成功否
 */
const authVerify = ({ fileID, userID, operation }) => {
    if (!_.isString(fileID) || !_.isString(userID) || !_.isString(operation)
        || _.indexOf([downloadOp, authQueryOp, authUpdateOp], operation) === -1) {
        return Promise.reject(new Error("Bad param(s) to call authVerify function"));
    }
    return getOneFileACL(`${fileID}${COMBINATION_DELIMITER}${userID}`).then(async (fileACL) => {
        if (!fileACL) {
            if (fileID === "ACCESSAUTHID_NOT_FOUND") {
                return Promise.reject(new Error(
                    "Not found accessAuth",
                ));
            }
            const fileExists = await getFile(fileID);
            if (!fileExists) {
                return Promise.reject(new Error(
                    `No such file:${fileID}`,
                )); 
            }
            return false;
        }
        const { authority, restrictionType, restrictionContent } = fileACL;
        let pass = false;
        switch (operation) {
            case downloadOp:
                pass = (authority & 0b1000) !== 0;
                break;
            case authQueryOp:
                pass = (authority & 0b0010) !== 0;
                break;
            case authUpdateOp:
                pass = (authority & 0b0001) !== 0;
                break;
            default:
        }
        const now = new Date().toISOString();
        if (pass) {
            switch (restrictionType) {
                case "TIME_RANGE":
                    pass = restrictionContent.from <= now
                        && restrictionContent.to >= now;
                    break;
                case "COUNT":
                    pass = restrictionContent.remainCount > 0;
                    break;
                default:
            }
        }
        return pass;
    }); 
};

const authVerificationHandler = async (req, res, next) => {
    let userID;
    let objectFileID;
    let operation;
    switch (`${req.method}-${req.baseUrl}${req.route.path}`) {
        case "GET-/files/:fileID/download":
            objectFileID = req.params.fileID;
            userID = req.query.requesterID;
            operation = downloadOp;
            break;
        case "GET-/accessAuths/":
            objectFileID = req.query.fileID;
            userID = req.query.requesterID;
            operation = authQueryOp;
            break;
        case "GET-/files/:fileID/accessAuths":
            objectFileID = req.params.fileID;
            userID = req.query.requesterID;
            operation = authQueryOp;
            break;
        case "GET-/accessAuths/:accessAuthID": {
            const objectFileACL = (await getOneFileACL(req.params.accessAuthID)
                .$fragment(prismaFragmentFileACLOnlyWithFileID));
            if (objectFileACL) objectFileID = objectFileACL.file.fileID; 
            else objectFileID = "ACCESSAUTHID_NOT_FOUND"; // Normal fileID is a hash value so it's ok 
            userID = req.query.requesterID;
            operation = authQueryOp;
            break; 
        }
        case "GET-/files/:fileID/accessAuths/:accessAuthID":
            objectFileID = req.params.fileID;
            userID = req.query.requesterID;
            operation = authQueryOp;
            break;
        case "POST-/accessAuths/":
            objectFileID = req.body.fileID;
            userID = req.body.requesterID;
            operation = authUpdateOp;
            break;
        case "POST-/files/:fileID/accessAuths":
            objectFileID = req.params.fileID; 
            userID = req.body.requesterID;
            operation = authUpdateOp;
            break;
        case "PUT-/accessAuths/:accessAuthID": {
            const objectFileACL = (await getOneFileACL(req.params.accessAuthID)
                .$fragment(prismaFragmentFileACLOnlyWithFileID));
            if (objectFileACL) objectFileID = objectFileACL.file.fileID; 
            else objectFileID = "ACCESSAUTHID_NOT_FOUND"; // Normal fileID is a hash value so it's ok 
            userID = req.body.requesterID;
            operation = authUpdateOp;
            break;
        }
        case "PUT-/files/:fileID/accessAuths/:accessAuthID":
            objectFileID = req.params.fileID;
            userID = req.body.requesterID;
            operation = authUpdateOp;
            break;
        default:
            operation = null;
    }
    authVerify({ fileID: objectFileID, userID, operation }).then((pass) => {
        if (!pass) {
            serverInnerLogger.error(`Not pass the authority verification for ${objectFileID} ${userID} ${operation}, send the result message`);
            res.status(401)
                .json({ message: "You are unauthorized to make this request" })
                .end();
        } else {
            serverInnerLogger.info(`Pass the authority verification for ${objectFileID} ${userID} ${operation}`);
            next();
        } 
        return 0;
    }).catch((err) => {
        serverInnerLogger.error("Failed when verifying authority, error: ", err);
        res.status(404)
            .json({ message: err.message })
            .end();
    });
};
export default authVerificationHandler;
