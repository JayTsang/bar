import fs from "fs-extra";
import _ from "lodash";
import { fileMetaInitialize, getFile } from "./fileMetaMaintenance";
import serverInnerLogger from "../log/serverInnerLog";

/**
 * 
 * @param {String} fileID - 文件资源的唯一标识
 * @returns {Promise<String>} - 文件资源的存储相对路径
 */
export const downloadFile = (fileID) => {
    if (!_.isString(fileID)) {
        return Promise.reject(new TypeError("Bad param fileID to call getFile function, require a string"));
    }
    return getFile(fileID).then((fileMeta) => {
        if (!fileMeta) {
            return Promise.reject(new Error(`Not found fileMeta for the fileID: ${fileID}`));
        }
        serverInnerLogger.info(`Successfully query fileMeta from prisma for the file ${fileMeta.fileID}`);
        return fileMeta.storagePath;
    });
};

/**
 * 
 * @param {Object} fileInfo - 文件资源的相关信息
 * @param {String} fileInfo.tempPath - 文件的暂存绝对路径
 * @param {String} fileInfo.storagePath - 文件的正式存储绝对路径
 * @returns {Promise<Object>} - 文件资源的元数据信息
 */
export const storeFile = ({ 
    tempPath, storagePath, originName, hashAlg, hash, ownerID, timestamp, signature,
}) => {
    if (!_.isString(tempPath) || !_.isString(storagePath) || !_.isString(originName)
        || !_.isString(hashAlg) || !_.isString(hash) || !_.isString(ownerID)
        || !_.isInteger(timestamp) || !_.isString(signature)) {
        return Promise.reject(new TypeError("Bad params to call storeFile function"));
    }
    return fs.pathExists(tempPath).then((exists) => {
        if (!exists) {
            return Promise.reject(new Error(`The temp path: "${tempPath}" doesn't exist`));
        }
        return fs.move(tempPath, storagePath)
            .then(() => {
                serverInnerLogger.info(`Move file from temp path: ${tempPath} to formal path ${storagePath}`);
                return fileMetaInitialize({
                    storagePath, originName, hashAlg, hash, ownerID, timestamp, signature,
                }).then((res) => {
                    serverInnerLogger.info(`Successfully initialize fileMeta into prisma for the file ${res.fileID}`);
                    return res;
                });
            })
            .catch(err => Promise.reject(new Error(`File storage error: ${err.message}`)));
    });
};
