import { GetHashVal } from "rclink/lib/crypto";
import config from "config";
import fs from "fs-extra";
import path from "path";
import { prisma } from "../db/generated/prisma-client";
import { COMBINATION_DELIMITER } from "../const";

/**
 * 
 * @param {String} fileID - 文件资源id
 * @returns {Promise<Object>} fileMeta - 文件元数据
 */
export const getFile = fileID => prisma.fileMeta({ fileID });

/**
 * 
 * @param {*} param0 
 */
export const fileMetaInitialize = ({
    storagePath, originName, hashAlg, hash, ownerID, timestamp, signature,
}) => fs.stat(storagePath).then((stats) => {
    const { mtime, size } = stats;
    const fileID = GetHashVal({ data: `${signature}-${hash}` }).toString("hex");
    const storageRelativePath = storagePath.replace(new RegExp(`${config.get("File.fileStorageDir")}${path.sep}`), "");
    return prisma.createFileMeta({
        fileID,
        hash,
        hashAlg,
        name: originName,
        size,
        uploadTime: new Date(timestamp).toISOString(),
        storageTime: mtime.toISOString(),
        storagePath: storageRelativePath,
        fileACLs: {
            create: [{ // fileACL initialization atomically
                fileIDCombineUserID: `${fileID}${COMBINATION_DELIMITER}${ownerID}`,
                userID: ownerID,
                authority: 15,
                restrictionType: "UNRESTRICTED",
                restrictionContent: {},
            }],
        },
    });
});
