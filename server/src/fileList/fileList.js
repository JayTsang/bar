import _ from "lodash";
import { prisma } from "../db/generated/prisma-client";
import { FileShareEnumStatus } from "../validate/enumValues";

const [
    FILESHARESTATUS_OWNED,
    FILESHARESTATUS_OWNED_AND_SHARE_TO,
    FILESHARESTATUS_OWNED_AND_NOT_SHARE_TO,
    FILESHARESTATUS_SHARED_FROM,
    FILESHARESTATUS_ALL,
] = FileShareEnumStatus;

const getFileList = ({
    filter: {
        fileID = "",
        name = "",
        hash = "",
        uploadTime: { from = 0, to = Date.now() } = { from: 0, to: Date.now() },
        size: { min = 0, max = (2 ** 31) - 1 } = { min: 0, max: (2 ** 31) - 1 },
        accessAuth: { shareStatus = FILESHARESTATUS_ALL, userID = "" } = { shareStatus: FILESHARESTATUS_ALL, userID: "" },
    },
    orderBy = "fileID_ASC",
    pagination: { page = 1, perPage = 10 } = { page: 1, perPage: 10 },
}) => {
    if (!_.isString(fileID) || !_.isString(name) || !_.isString(hash)
        || !_.isInteger(from) || !_.isInteger(to)
        || !_.isInteger(min) || !_.isInteger(max)
        || !_.isString(orderBy) 
        || !_.isInteger(page) || !_.isInteger(perPage)
        || !_.isString(userID) || _.indexOf(FileShareEnumStatus, shareStatus) === -1) {
        return Promise.reject(new Error("Bad param(s) to call getFileList function"));
    }
    const prismaFilter = {};
    if (fileID !== "") {
        prismaFilter.fileID_contains = fileID;
    }
    if (name !== "") {
        prismaFilter.name_contains = name;
    }
    if (hash !== "") {
        prismaFilter.hash_contains = hash;
    }
    prismaFilter.uploadTime_gte = new Date(from).toISOString();
    prismaFilter.uploadTime_lte = new Date(to).toISOString();
    prismaFilter.size_gte = min;
    prismaFilter.size_lte = max;

    if (userID !== "") {
        switch (shareStatus) {
            case FILESHARESTATUS_OWNED:
                prismaFilter.fileACLs_some = { userID, authority: 15 };
                break;
            case FILESHARESTATUS_OWNED_AND_SHARE_TO:
                prismaFilter.AND = [
                    {
                        fileACLs_some: { userID, authority: 15 },
                    },
                    {
                        fileACLs_some: { authority: 8 },
                    },
                ];
                break;
            case FILESHARESTATUS_OWNED_AND_NOT_SHARE_TO:
                prismaFilter.AND = [
                    {
                        fileACLs_some: { userID, authority: 15 },
                    },
                    {
                        fileACLs_none: { authority: 8 },
                    },
                ];
                break;
            case FILESHARESTATUS_SHARED_FROM:
                prismaFilter.fileACLs_some = { userID, authority: 8 };
                break;
            case FILESHARESTATUS_ALL:
                prismaFilter.OR = [
                    {
                        fileACLs_some: { userID, authority: 15 },
                    },
                    {
                        fileACLs_some: { userID, authority: 8 },
                    },
                ];
                break;
            default:
                return Promise.reject(new Error("Bad shareStatus"));
        }
    }

    return prisma.fileMetas({
        where: prismaFilter,
        orderBy,
        skip: (page - 1) * perPage,
        first: perPage, 
    }).then(fileList => prisma.fileMetasConnection({
        where: prismaFilter,
    }).aggregate()
        .count()
        .then(totalCount => ({ fileList, totalCount })));
};

export default getFileList;
