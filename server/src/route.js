import filesRouter from "./api/files";
import accessAuthsRouter from "./api/accessAuths";
import proofsRouter from "./api/proofs";

// URLs
/* 
file:
DONE    - GET  /files : get a file list
DONE    - GET  /files/fileID : get a metadata for a file with the specific fileID
DONE    - POST /files : create a new File or multi new files
DONE    - GET  /files/fileID/download : download a file binary data for a specific fileID
accessAuth:
DONE    - GET  /accessAuths : get an accessAuth list 
DONE    - GET  /files/fileID/accessAuths : get accessAuth list for the specific fileID
DONE    - GET  /accessAuths/accessAuthID : get an accessAuth for the specific accessAuthID 
DONE    - GET  /files/fileID/accessAuths/accessAuthID : get an accessAuth for 
               the specific fileID and the accessAuthID
DONE    - POST   /accessAuths : create a new accessAuth
DONE    - POST   /files/fileID/accessAuths : create a new accessAuth
DONE    - PUT    /accessAuths/accessAuthID : update a accessAuth
DONE    - PUT    /files/fileID/accessAuths/accessAuthID : update a accessAuth 
DROP    - DELETE /accessAuths/accessAuthID : delete a accessAuth
DROP    - DELETE /files/fileID/accessAuths/accessAuthID : delete a accessAuth 
proof:
DONE    - GET  /proofs : get a proof list 
DONE    - GET  /proofs/proofID : get a proof
DONE    - GET  /files/fileID/proofs : get a proof list for a specific fileID
DONE    - GET  /files/fileID/proofs/proofID : get a proof for a specific fileID and proofID
*/
const setRoute = (app) => {
    app
        .use("/files", filesRouter)
        .use("/accessAuths", accessAuthsRouter)
        .use("/proofs", proofsRouter);
};

export default setRoute;
