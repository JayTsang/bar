import cfg from "config";
import { Observer } from "../blockchainSync/syncBlockHandlerOfCustom";
import parseWorldStateValue from "../blockchainSync/nodeJava/parseWorldStateValue";
import { COMBINATION_DELIMITER } from "../const";

const { chaincodeName } = cfg.get("RepChain");

class RestricKeypairObserver extends Observer {
    constructor(prisma) {
        super(prisma, {
            chaincodeName, 
            functions: ["restrictKeypairOnlyForQuery"],
        });
    }

    update(worldState) {
        if (!this.isMatched(worldState)) return Promise.resolve(0); 
        const keypairOnlyForQueryBuf = Buffer.from(worldState.value, "hex");
        const keypairOnlyForQuery = parseWorldStateValue("KeypairOnlyForQuery", keypairOnlyForQueryBuf);

        const { accountCreditCode, certName, isRestricted } = keypairOnlyForQuery;
        const creditCodeCombineName = `${accountCreditCode}${COMBINATION_DELIMITER}${certName}`; 

        return this.prisma.updateCert({
            where: { creditCodeCombineName },
            data: { isRestricted },
        });
    }
}

export default RestricKeypairObserver;
