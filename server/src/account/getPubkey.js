import { isString } from "lodash";
import { prisma } from "../db/generated/prisma-client";
import { COMBINATION_DELIMITER } from "../const";

const getPubkey = (userID, certName) => {
    if (!isString(userID) || !isString(certName)) {
        return Promise.reject(new TypeError("Bad param to call getPubkey function"));
    }

    const creditCodeCombineName = `${userID}${COMBINATION_DELIMITER}${certName}`;
    return prisma.cert({ creditCodeCombineName }).then((cert) => {
        if (!cert) {
            return { pubKey: null, status: null, isRestricted: null };
        }
        return { pubKey: cert.certPEM, status: cert.status, isRestricted: cert.isRestricted };
    });
};
export default getPubkey;
