import { updateProofStatus } from "./proofMaintenance";
import { ProofEnumStatus } from "../validate/enumValues";
import serverInnerLogger from "../log/serverInnerLog";
import { getWSConnections } from "../api/realtime";

const [PROOF_STATUS_CONFIRMED] = ProofEnumStatus;

class ProofTxObserver {
    constructor(filter) {
        this.updateProofStatus = updateProofStatus;
        this.filter = filter;
    }

    update(transaction) {
        const { txid, block: { hash, timestamp } } = transaction;
        const updateParams = {
            txid,
            status: PROOF_STATUS_CONFIRMED,
            blockHash: hash,
            blockTimestamp: new Date(timestamp).getTime(),
        };
        this.updateProofStatus(updateParams)
            .then((r) => {
                serverInnerLogger.info(`Successfully update fileOperationProof(txid:${r.txid}) with new status ${r.status}`);
                const wsConnections = getWSConnections();
                wsConnections.forEach((wsConnection) => {
                    const proofStr = JSON.stringify(r);
                    wsConnection.sendUTF(proofStr);
                    const logMessage1 = `Sent the fileOperationProof(txid:${r.txid}) updated through the websocket connection`;
                    const logMessage2 = `(peer: ${wsConnection.remoteAddress}) to the client`;
                    serverInnerLogger.info(`${logMessage1}${logMessage2}`);
                });
                return 0;
            })
            .catch((err) => {
                serverInnerLogger.error(`Failed to update status of fileOperationProof(txid:${txid})\n`, err);
            });
    }
}

export default ProofTxObserver;
