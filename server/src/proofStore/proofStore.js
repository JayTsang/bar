/* eslint-disable no-bitwise */
import Transaction from "rclink/lib/transaction";
import RestAPI from "rclink/lib/rest";
import config from "config";
import fs from "fs-extra";
import _ from "lodash";
// import uuidv1 from "uuid/v1";
import { storeProof, updateProofStatus } from "./proofMaintenance";
import { getOneFileACL } from "../accessControl/authMaintenance";
import { OperationEnumTypes, ProofEnumStatus, ProofContractFunctionEnumNames } from "../validate/enumValues";
import getOriginDataToBeSigned from "../validate/originDataTBS";
import { COMBINATION_DELIMITER } from "../const";
import serverInnerLogger from "../log/serverInnerLog";

const CONFIG_SERVER_CRYPTO = "ServerCrypto";
const CONFIG_REPCHAIN = "RepChain";

const TX_TYPE_INVOKE = "CHAINCODE_INVOKE";
const [OP_TYPE_UPLOAD, OP_TYPE_DOWNLOAD, OP_TYPE_AUTH_UPDATE] = OperationEnumTypes;
const [, PROOF_STATUS_WAITING, PROOF_STATUS_FAILED] = ProofEnumStatus;
const [CCF_NAME_DAS, CCF_NAME_D, CCF_NAME_AAA] = ProofContractFunctionEnumNames; 

// TODO: 当server与RepChain之间出现网络故障后，应能将未能上链的文件操作证据重新上链
const storeFileOperationProof = async ({
    file, operationType, operationParams, requestParams, 
}) => {
    if (!_.isPlainObject(file)
        || !_.isPlainObject(operationParams)
        || !_.isPlainObject(requestParams)
        || _.indexOf(OperationEnumTypes, operationType) === -1) {
        throw Promise.reject(new Error("Bad param(s) to call storeFileOperationProof function"));
    }
    const { fileID } = file;
    const operatorID = requestParams.requesterID;

    let proofNeedStore;
    let contractFunction;
    let contractFunctionArgs;

    const fileACL = await getOneFileACL(`${fileID}${COMBINATION_DELIMITER}${operatorID}`);
    if (!fileACL) {
        return Promise.reject(new Error(`Not found fileACL record for ${fileID}-${operatorID}, store proof failed`));
    }
    const { postActStoreProof } = fileACL;
    switch (operationType) {
        case OP_TYPE_UPLOAD:
            proofNeedStore = true;
            contractFunction = CCF_NAME_DAS;
            contractFunctionArgs = {
                fileID,
                hash: file.hash,
                hashAlg: file.hashAlg,
                name: file.name,
                size: file.size,
                uploadTimestamp: new Date(file.uploadTime).getTime(),
                storageTimestamp: new Date(file.storageTime).getTime(),
                ownerCreditID: operatorID,
                ownerCertName: requestParams.certName,
                request: getOriginDataToBeSigned(requestParams, "010"), 
                signature: requestParams.signature,
                signAlg: requestParams.signAlg,
            };
            break;
        case OP_TYPE_DOWNLOAD:
            proofNeedStore = (postActStoreProof & 0b1000) !== 0;
            contractFunction = CCF_NAME_D;
            contractFunctionArgs = {
                fileID,
                requesterCreditID: operatorID,
                requesterCertName: requestParams.certName,
                timestamp: requestParams.timestamp,
                request: getOriginDataToBeSigned(requestParams, "010"), 
                signature: requestParams.signature,
                signAlg: requestParams.signAlg,               
            };
            break;
        case OP_TYPE_AUTH_UPDATE:
            proofNeedStore = (postActStoreProof & 0b0001) !== 0;
            contractFunction = CCF_NAME_AAA;
            contractFunctionArgs = {
                fileID,
                accessAuthID: file.fileACLID,
                objectUserID: operationParams.objectUserID,
                accessAuthority: operationParams.authority, 
                accessRestrictionType: operationParams.restrictionType,
                accessRestrictionContent: JSON.stringify(operationParams.restrictionContent),
                accessPostProofStorage: operationParams.postActStoreProof,
                requesterCreditID: operatorID,
                requesterCertName: requestParams.certName,
                timestamp: requestParams.timestamp,
                request: getOriginDataToBeSigned(requestParams, "010"), 
                signature: requestParams.signature,
                signAlg: requestParams.signAlg,
            };
            break;
        default:
            proofNeedStore = true;
    }
    if (!proofNeedStore) {
        return Promise.resolve(`Ignore proof store for operation: ${fileID}-${operationType}-by-${operatorID}`);
    }

    const { chaincodeName, chaincodeVersion } = config.get(CONFIG_REPCHAIN);
    const blockchainTransaction = new Transaction({
        type: TX_TYPE_INVOKE,
        chaincodeName,
        chaincodeVersion,
        chaincodeInvokeParams: {
            chaincodeFunction: contractFunction,
            chaincodeFunctionArgs: [JSON.stringify(contractFunctionArgs)],
        },
    });
    const {
        prvKeyFilePath, prvKeyPassword, pubKeyFilePath, alg, creditCode, certName, 
    } = config.get(CONFIG_SERVER_CRYPTO);
    const prvKey = fs.readFileSync(prvKeyFilePath).toString();
    const pubKey = fs.readFileSync(pubKeyFilePath).toString();
    const blockchainTransactionBytes = blockchainTransaction.sign({ 
        prvKey, pubKey, alg, creditCode, certName, pass: prvKeyPassword,
    });
    // const blockchainTransaction = { txid: `mock-txid-${uuidv1()}`, txTimestamp: Date.now() };

    const txid = blockchainTransaction.getTxMsg().id;
    const localProof = await storeProof({
        fileID, 
        operationType, 
        operationParams, 
        operatorID,
        txid,
        txTimestamp: blockchainTransaction.getTxMsg().signature.tmLocal.seconds * 1000,
        status: PROOF_STATUS_WAITING,
    });

    const { server, port } = config.get(CONFIG_REPCHAIN);
    const blockchainRestAPIClient = new RestAPI(`${server}:${port}`);
    blockchainRestAPIClient.sendTransaction(blockchainTransactionBytes)
        .then((result) => {
            serverInnerLogger.info(
                `Have sent a transaction(txid:${txid}) to store proof, got the result from RepChain peer: ${JSON.stringify(result)}`,
            );
            if (result.err) {
                updateProofStatus({ txid, status: PROOF_STATUS_FAILED });
                serverInnerLogger.error(`Error for transaction(txid:${txid}) to store proof`, result.err);
            }
            return 0;
        });
    
    return Promise.resolve(localProof);
};

export default storeFileOperationProof;
