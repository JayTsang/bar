import _ from "lodash";
import { prisma } from "../db/generated/prisma-client";
import { OperationEnumTypes, ProofEnumStatus, MatchEnumModes } from "../validate/enumValues";

const [PROOF_STATUS_CONFIRMED] = ProofEnumStatus;

export const proofWithLinkTypeIDFragment = `
    fragment ProofWithLinkTypeID on Proof { 
        id
        file {
            fileID
        }
        operationType
        operationParams
        operatorID
        txid
        transaction {
            id
        }
        txTimestamp
        blockHash
        blockTimestamp
        status
    }
    `;

export const getProofCollection = ({
    filter: {
        fileID: { fileID = "", matchMode = "CONTAINS" } = { fileID: "", matchMode: "CONTAINS" },
        operationType = "",
        operatorID = "",
        status = "",
    } = {},
    sortBy = "txTimestamp_ASC",
    pagination: { page = 1, perPage = 10 } = { page: 1, perPage: 10 },
} = {}) => {
    if (!_.isString(fileID) || !_.indexOf(MatchEnumModes, matchMode) === -1
        || (!_.isEmpty(operationType) && _.indexOf(OperationEnumTypes, operationType) === -1)
        || (!_.isEmpty(status) && _.indexOf(ProofEnumStatus, status) === -1)
        || !_.isString(operatorID)
        || !_.isString(sortBy) || (!sortBy.endsWith("_ASC") && !sortBy.endsWith("_DESC"))
        || !_.isInteger(page) || !_.isInteger(perPage)) {
        return Promise.reject(new Error("Bad param(s) to call getProofCollection function"));
    }
    const prismaFilter = { AND: [] };
    if (fileID !== "") prismaFilter.AND.push({ file: matchMode === "CONTAINS" ? { fileID_contains: fileID } : { fileID } });
    if (operationType !== "") prismaFilter.AND.push({ operationType });
    if (operatorID !== "") prismaFilter.AND.push({ operatorID_contains: operatorID });
    if (status !== "") prismaFilter.AND.push({ status });
    return prisma.fileOperationProofs({
        where: prismaFilter,
        orderBy: sortBy,
        skip: (page - 1) * perPage,
        first: perPage, 
    }).$fragment(proofWithLinkTypeIDFragment)
        .then(proofs => prisma.fileOperationProofsConnection({
            where: prismaFilter,
        }).aggregate()
            .count()
            .then(totalCount => ({ proofs, totalCount })),
        );
};

export const getOneProof = (proofID) => {
    if (!_.isString(proofID)) return Promise.reject(new Error("Bad param to call getOneProof function"));
    return prisma.fileOperationProof({
        txid: proofID,
    }).$fragment(proofWithLinkTypeIDFragment);
};

export const storeProof = ({
    fileID, operationType, operationParams, operatorID, txid, txTimestamp, status,
}) => {
    if (!_.isString(fileID)
        || _.indexOf(OperationEnumTypes, operationType) === -1
        || !_.isPlainObject(operationParams)
        || !_.isString(operatorID)
        || !_.isString(txid)
        || !_.isInteger(txTimestamp)
        || _.indexOf(ProofEnumStatus, status) === -1) {
        return Promise.reject(new Error("Bad param(s) to call storeProof function"));
    }
    return prisma.createFileOperationProof({
        file: {
            connect: { fileID },
        },
        operationType,
        operationParams,
        operatorID,
        txid,
        txTimestamp: new Date(txTimestamp),
        status,
    }).$fragment(proofWithLinkTypeIDFragment);
};

export const updateProofStatus = async ({
    txid, status, blockHash, blockTimestamp,
}) => {
    if (!_.isString(txid) 
        || _.indexOf(ProofEnumStatus, status) === -1
        || (blockHash && !_.isString(blockHash))
        || (blockTimestamp && !_.isInteger(blockTimestamp))) {
        return Promise.reject(new Error("Bad param(s) to call updateProofStatus function "));
    }

    const data = { status };
    if (blockHash) data.blockHash = blockHash;
    if (blockTimestamp) data.blockTimestamp = new Date(blockTimestamp);
    if (status === PROOF_STATUS_CONFIRMED && await prisma.transaction({ txid })) {
        data.transaction = { connect: { txid } };
    }

    return prisma.updateFileOperationProof({
        data,
        where: {
            txid,
        },
    }).$fragment(proofWithLinkTypeIDFragment);
};
