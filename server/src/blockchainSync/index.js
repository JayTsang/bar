/*
 * Copyright  2018 Linkel Technology Co., Ltd, Beijing
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BA SIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import cfg from "config";
import { prisma } from "../db/generated/prisma-client";
import Syncher from "./sync";
import {
    SyncBlockchainDataObservable,
    SyncTransactionObservable,
    AccountObserver,
    CertificateObserver,
} from "./syncBlockHandlerOfCustom";
import ProofTxObserver from "../proofStore/proofTransactionObserver";
import RestricKeypairObserver from "../account/restrictKeypairObserver";
import serverInnerLogger from "../log/serverInnerLog";

let pushSyncTimeout;
let pullSyncTimeout;

export const startBlockchainSync = () => {
    if (!cfg.get("Synchronize.enable")) return;

    // 观察者模式，构建区块链数据同步后的文件操作存证业务数据更新操作
    const syncTransactionObservable = new SyncTransactionObservable();
    const { chaincodeName } = cfg.get("RepChain");
    const proofTransactionObserver = new ProofTxObserver({
        type: "CHAINCODE_INVOKE",
        chaincodeID: {
            chaincodeName,
        },
    });
    syncTransactionObservable.subscribe(proofTransactionObserver);

    // 对同步区块链数据后，构建用户账户信息和证书信息
    const syncBlockchainDataObservable = new SyncBlockchainDataObservable();
    const accountObserver = new AccountObserver(prisma);
    const certificateObserver = new CertificateObserver(prisma);
    syncBlockchainDataObservable.subscribe(accountObserver);
    syncBlockchainDataObservable.subscribe(certificateObserver);
    const restrictKeypairObserver = new RestricKeypairObserver(prisma);
    syncBlockchainDataObservable.subscribe(restrictKeypairObserver);

    const sync = new Syncher(
        prisma,
        cfg.get("RepChain.default.url_api"),
        cfg.get("RepChain.default.url_subscribe"),
        { syncTransactionObservable, syncBlockchainDataObservable },
    );

    // 拉取主动同步
    if (cfg.get("Synchronize.PullSyncType") === "PERIODICALLY") {
        pullSyncTimeout = setInterval(
            () => {
                sync.StartPullBlocks();
            },
            cfg.get("Synchronize.PullSyncInterval"),
        );
    } else {
        pullSyncTimeout = setTimeout(
            () => {
                sync.StartPullBlocks();
            },
            cfg.get("Synchronize.PullSyncInterval"),
        );
    }

    // 实时推送被动同步
    pushSyncTimeout = setTimeout(
        () => {
            sync.startSyncPush();
        },
        cfg.get("Synchronize.PushSyncInterval"),
    );

    serverInnerLogger.info("Started blockchain synchronization procedure");
};

export const stopBlockchainSync = () => {
    clearTimeout(pushSyncTimeout);
    clearTimeout(pullSyncTimeout);
    serverInnerLogger.info("Stoped blockchain synchronization procedure");
};

// startBlockchainSync();
