
import { isMatch } from "lodash";
import parseWorldStateValue from "./nodeJava/parseWorldStateValue";
import { COMBINATION_DELIMITER } from "../const";

const ECODE_HEX = "hex";

const txGraphqlFragment = `
    fragment TransactionWithAffluentInfo on Transaction {
        type
        txid
        orderInBlock
        chaincodeID {
            chaincodeName
            version
        }
        chaincodeDeployParams {
            timeout
            codePackage
            legalProse
            codeLanguageType
        }
        chaincodeInvokeParams {
            function
            args
        }
        chaincodeSetStateParams {
            state
        }
        block {
            version
            hash
            height
            transCount
            timestamp
            stateHash
        }
        signature {
            certID {
                creditCode
                certName
            }
            timestamp 
            signature
        }
        timestamp
    }
    `;

export class SyncBlockchainDataObservable {
    constructor() {
        this.observers = [];
    }

    isSubscribed(observer) {
        return this.observers.includes(observer);
    }

    subscribe(observer) {
        if (!this.isSubscribed(observer)) {
            this.observers.push(observer);
        }
    }

    unSubscribe(observer) {
        const index = this.observers.indexOf(observer);
        if (index > -1) this.observers.splice(index, 1);
    }

    async notify(data) {
        for (const observer of this.observers) {
            // eslint-disable-next-line no-await-in-loop
            await observer.update(data);
        }
    }
}

export class Observer {
    constructor(prisma, filter) {
        this.prisma = prisma;
        this.filter = filter;
    }

    isMatched(worldState) {
        return isMatch(worldState, this.filter);
    }

    // eslint-disable-next-line class-methods-use-this
    update() {
        return Promise.resolve(this.filter);
    }
}

export class AccountObserver extends Observer {
    constructor(prisma) {
        super(prisma, {
            chaincodeName: "ContractCert",
            functions: ["SignUpSigner"],
        });
    }

    update(worldState) {
        if (!this.isMatched(worldState)) return Promise.resolve(0);
        const accountBuf = Buffer.from(worldState.value, ECODE_HEX);
        const account = parseWorldStateValue("Account", accountBuf);

        return this.prisma.upsertAccount({
            where: {
                creditCode: account.creditCode,
            },
            update: { ...account },
            create: { ...account },
        });
    }
}

export class CertificateObserver extends Observer {
    constructor(prisma) {
        super(prisma, [
            {
                chaincodeName: "ContractCert",
                functions: ["SignUpCert"],
            },
            {
                chaincodeName: "ContractCert",
                functions: ["SignUpSigner"],
            },
        ]);
    }

    // @Override
    isMatched(worldState) { // 不能只依据影响worldstate value值的合约方法是否含有SignUpCert来判断其表示证书
        return isMatch(worldState, this.filter[0]) && !isMatch(worldState, this.filter[1]);
    }

    update(worldState) {
        if (!this.isMatched(worldState)) return Promise.resolve(0);
        const certBuf = Buffer.from(worldState.value, ECODE_HEX);
        let cert = parseWorldStateValue("Certificate", certBuf);
        const { creditCode, name } = worldState;
        const creditCodeCombineName = `${creditCode}${COMBINATION_DELIMITER}${name}`;
        cert = {
            ...cert, creditCode, name, creditCodeCombineName,
        };

        return this.prisma.upsertCert({
            where: {
                creditCodeCombineName,
            },
            update: { ...cert },
            create: { ...cert, account: { connect: { creditCode } } },
        });
    }
}

export class SyncTransactionObservable {
    constructor() {
        this.observers = [];
        this.txGraphqlFragment = txGraphqlFragment;
    }

    isSubscribed(observer) {
        return this.observers.includes(observer);
    }

    // eslint-disable-next-line class-methods-use-this
    isMatchFilter(transaction, observer) {
        return isMatch(transaction, observer.filter);
    }

    subscribe(observer) {
        if (!this.isSubscribed(observer)) {
            this.observers.push(observer);
        }
    }

    unSubscribe(observer) {
        const index = this.observers.indexOf(observer);
        if (index > -1) this.observers.splice(index, 1);
    }

    notifyOne(observer, transaction) {
        if (this.isSubscribed(observer)
            && this.isMatchFilter(transaction, observer)) {
            observer.update(transaction);
        }
    }

    notifyAll(transaction) {
        this.observers.forEach((observer) => {
            if (this.isMatchFilter(transaction, observer)) {
                observer.update(transaction);
            }
        });
    }
}
