/*
 * Copyright  2018 Linkel Technology Co., Ltd, Beijing
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BA SIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/* eslint-disable no-bitwise */
/* eslint-disable no-await-in-loop */
import { Crypto } from "rclink";
import URL from "url";
import { isFinite, find, uniq } from "lodash";
import { COMBINATION_DELIMITER } from "../const";

const ECODE_BIN = "hex";
const ECODE_HEX = "hex";


class BlockStorager {
    constructor(prisma, pullUrl, customObservable) {
        this.prisma = prisma;
        this.default_NetId = "";
        this.default_NetName = "DEFAULT_NET";
        this.customObservable = customObservable;
        const p = URL.parse(pullUrl);
        this.seedip = p.hostname;
        console.log(this.seedip);

        this.lastBlockHash = "";
        this.lastheight = 0;
        this.transCount = 0;
        this.lastBlockTime = 0;
        this.blockCount = 0;
        this.tps = 0.0;
        this.tm_lastBlock = process.uptime();
        return this;
    }

    getLastSyncHeight() {
        return this.lastheight;
    }

    getLastBlockHash() {
        return this.lastBlockHash;
    }

    setBlockCount(blkcount) {
        this.blockCount = ~~blkcount;
        console.log(this.blockCount);
    }

    setBlockInc() {
        this.blockCount = this.blockCount + 1;
        console.log(this.blockCount);
    }

    async InitStorager() {
        let net = null;
        await this.prisma.networks({ where: { seedip: this.seedip } })
            .then((result) => {
                if (result != null && result.length > 0) {
                    [net] = result;
                }
                return 0;
            }).catch(err => console.log(err));

        if (net == null) {
            await this.prisma.createNetwork({
                name: this.default_NetName,
                syncHeight: 0,
                seedip: this.seedip,
                lastBlockHash: "",
                lastBlockTime: 0,
                tps: 0.0,
                blockCount: 0,
                transCount: 0,
            }).then((result) => { net = result; })
                .catch(err => console.log(err));
        }

        if (net != null) {
            this.default_NetId = net.id;
            this.default_NetName = net.name;
            if (!isFinite(net.syncHeight)) {
                this.lastheight = 0;
            } else this.lastheight = net.syncHeight;
            this.lastBlockHash = net.lastBlockHash;
            if (!isFinite(net.transCount)) {
                this.transCount = 0;
            } else this.transCount = net.transCount;

            this.lastBlockTime = net.lastBlockTime;
            if (!isFinite(net.tps)) {
                this.tps = 0;
            } else this.tps = net.tps;
        }
        console.log("storager init finish");
    }


    async saveBlock(blk) {
        console.log(`=======${blk.height},${blk.endorsements[0].tmLocal.seconds}`);
        const blkData = {
            version: blk.version,
            hash: Buffer.from(blk.hashOfBlock).toString(ECODE_BIN),
            transCount: blk.transactions.length,
            timestamp: new Date(blk.endorsements[0].tmLocal.seconds * 1000 - 8 * 3600 * 1000),
            height: blk.height.low,
            prevHash: null,
            stateHash: blk.stateHash.toString(ECODE_BIN),
        };

        const endorsements = blk.endorsements.map((value) => {
            const endorsement = {};
            const certIDId = Crypto
                .GetHashVal({ data: `${value.certId.creditCode}-${value.certId.certName}` })
                .toString(ECODE_BIN);
            return this.prisma.$exists.certID({ fieldsHash: certIDId }).then((r) => {
                if (r) {
                    endorsement.certID = { connect: { fieldsHash: certIDId } };
                } else {
                    endorsement.certID = {
                        create: {
                            fieldsHash: certIDId,
                            creditCode: value.certId.creditCode,
                            certName: value.certId.certName,
                        },
                    };
                }
                endorsement.timestamp = new Date(value.tmLocal.seconds * 1000 - 8 * 3600 * 1000);
                endorsement.signature = value.signature.toString(ECODE_BIN);
                return endorsement;
            });
        });
        blkData.endorsements = { create: await Promise.all(endorsements) };

        if (blk.previousBlockHash) {
            blkData.prevHash = Buffer.from(blk.previousBlockHash).toString(ECODE_BIN);
            if (blkData.prevHash !== "") {
                blkData.prevBlock = { connect: { hash: blkData.prevHash } };
            }
        }
        // 判断当前的块是不是能够接到最后一个块
        if (blkData.prevHash === this.lastBlockHash) {
            // 可以保存,继续检查该块是否存在
            await this.saveBlockForOnly(blkData, blk);
            await this.refreshCache4Async(blk, blkData.hash);
            console.log(`save block success,block height=${this.lastheight}`);
            return 1;
        }
        console.log("do not save ,current block is not next block,reset pull,please");
        return 0;
    }

    async refreshCache4Async(blk, blkHash) {
        // console.log('netid='+this.default_NetId);
        const netData = this.statisNetData(blk, blkHash);
        // console.log(netData);
        await this.prisma.updateNetwork({
            where: { id: this.default_NetId },
            data: netData,
        });
        this.lastheight = netData.syncHeight;
        this.lastBlockHash = netData.lastBlockHash;
        this.transCount = netData.transCount;
        this.lastBlockTime = netData.lastBlockTime;
        this.tps = netData.tps;
    }

    statisNetData(blk, blkHash) {
        let tps = 0;
        const ctime = ~~blk.endorsements[0].tmLocal.seconds;
        const tmNow = process.uptime();
        if (this.tm_lastBlock !== null) {
            const tmSpan = tmNow - this.tm_lastBlock;
            console.log(`tmSpan:${tmSpan}`);

            if (tmSpan < 10000) {
                tps = blk.transactions.length / tmSpan;
                tps = Math.floor(tps * 10) / 10;
            }
        }
        console.log(`tps:${tps}`);
        this.tm_lastBlock = tmNow;

        console.log(`this.blockcount=${this.blockCount}`);

        const netData = {
            syncHeight: ~~blk.height.low, // this.lastheight+1, 
            blockCount: this.blockCount,
            transCount: ~~this.transCount + blk.transactions.length,
            lastBlockHash: blkHash,
            lastBlockTime: ctime,
            tps,
        };
        return netData;
    }

    async saveBlockForOnly(blkData, blk) {
        const isexist = await this.prisma.$exists.block({ hash: blkData.hash });
        if (!isexist) {
            console.log("start save block");

            await this.prisma.createBlock(blkData);
            // update the the prevBlock
            if (blkData.prevHash !== "") {
                await this.prisma.updateBlock(
                    {
                        where: { hash: blkData.prevHash },
                        data: { followBlock: { connect: { hash: blkData.hash } } },
                    },
                );
            }
            console.log("save block finish");

            await this.saveWorldStates(blk);
            console.log("save WorldStates through OperLog finished");

            await this.saveTranscations(blk.transactions, blkData.hash);
        }
    }

    async saveWorldStates(blk) {
        for (const tr of blk.transactionResults) {
            if (tr.ol) {
                const tx = find(blk.transactions, t => tr.txId === t.id);
                const { chaincodeName } = tx.cid;
                const chaincodeFunctions = [];
                if (tx.ipt) {
                    chaincodeFunctions.push(tx.ipt.function);
                }
                for (const operLog of tr.ol) {
                    const chaincodeNameCombineKey = `${chaincodeName}${COMBINATION_DELIMITER}${operLog.key}`;
                    const isExisted = await this.prisma.$exists.worldState({
                        chaincodeNameCombineKey,
                    });
                    let worldState;
                    if (isExisted) {
                        worldState = await this.prisma.worldState({
                            chaincodeNameCombineKey,
                        });
                        worldState = await this.prisma.updateWorldState({
                            where: {
                                chaincodeNameCombineKey,
                            },
                            data: {
                                value: operLog.newValue.toString(ECODE_HEX),
                                functions: {
                                    set: uniq(worldState.functions.concat(chaincodeFunctions)),
                                },
                            },
                        })
                            .catch((e) => {
                                console.error(e);
                            });
                    } else {
                        worldState = await this.prisma.createWorldState({
                            chaincodeNameCombineKey,
                            chaincodeName,
                            key: operLog.key,
                            value: operLog.newValue.toString(ECODE_HEX),
                            functions: { set: chaincodeFunctions },
                        });
                    }

                    // 根据目前RepChain的proto数据结构定义，
                    // 如果worldstate与Certificate相关,
                    // 这里需要到交易数据的合约方法调用参数里获取证书名name和归属账户的信用代码creditCode,
                    // 并添加到notify方法的参数中
                    if (tx.ipt && ["SignUpCert", "UpdateCertStatus"].includes(tx.ipt.function)) {
                        const arg = JSON.parse(tx.ipt.args[0]);
                        // eslint-disable-next-line camelcase
                        const { credit_code, name } = arg;
                        worldState = { ...worldState, creditCode: credit_code, name };
                    }

                    // notify worldstate observers
                    await this.customObservable.syncBlockchainDataObservable.notify(worldState);
                }
            }
        }
    }

    async saveTranscations(txs, blkHash) {
        for (let cidx = 0; cidx < txs.length; cidx++) {
            const tx = txs[cidx];
            const txData = {
                txid: tx.id,
                orderInBlock: cidx + 1,
                blockId: blkHash,
                block: { connect: { hash: blkHash } },
                timestamp: new Date(tx.signature.tmLocal.seconds * 1000 - 8 * 3600 * 1000),
            };

            const ChaincodeIDId = Crypto
                .GetHashVal({ data: `${tx.cid.chaincodeName}-${tx.cid.version}` })
                .toString(ECODE_BIN);
            if (await this.prisma.$exists.chaincodeID({ fieldsHash: ChaincodeIDId })) {
                txData.chaincodeID = { connect: { fieldsHash: ChaincodeIDId } };
            } else {
                txData.chaincodeID = {
                    create: {
                        fieldsHash: ChaincodeIDId,
                        chaincodeName: tx.cid.chaincodeName,
                        version: tx.cid.version,
                    },
                };
            }

            txData.signature = {
                create: {
                    signature: tx.signature.signature.toString(ECODE_HEX),
                    timestamp: new Date(tx.signature.tmLocal.seconds * 1000 - 8 * 3600 * 1000),
                },
            };
            const certIDId = Crypto
                .GetHashVal({ data: `${tx.signature.certId.creditCode}-${tx.signature.certId.certName}` })
                .toString(ECODE_BIN);
            if (await this.prisma.$exists.certID({ fieldsHash: certIDId })) {
                txData.signature.create.certID = { connect: { fieldsHash: certIDId } };
            } else {
                txData.signature.create.certID = {
                    create: {
                        fieldsHash: certIDId,
                        creditCode: tx.signature.certId.creditCode,
                        certName: tx.signature.certId.certName,
                    },
                };
            }

            // console.log("----------------tx.type="+JSON.stringify(tx))
            if (tx.type === 1) {
                txData.type = "CHAINCODE_DEPLOY";
                const chaincodeDeployParamsId = Crypto.GetHashVal({
                    data: `${tx.spec.timeout}-${tx.spec.codePackage}-${tx.spec.legalProse}-${tx.spec.ctype}`,
                }).toString(ECODE_BIN);
                if (await this.prisma.$exists.chaincodeDeploy({
                    fieldsHash: chaincodeDeployParamsId,
                })) {
                    txData.chaincodeDeployParams = {
                        connect: { fieldsHash: chaincodeDeployParamsId },
                    };
                } else {
                    txData.chaincodeDeployParams = {
                        create: {
                            fieldsHash: chaincodeDeployParamsId,
                            timeout: tx.spec.timeout,
                            codePackage: tx.spec.codePackage,
                            legalProse: tx.spec.legalProse,
                            codeLanguageType: tx.spec.ctype === 1 ? "CODE_JAVASCRIPT" : "CODE_SCALA",
                        },
                    };
                }
            } else if (tx.type === 2) {
                txData.type = "CHAINCODE_INVOKE";
                const chaincodeInvokeParamsId = Crypto
                    .GetHashVal({ data: `${tx.ipt.function}-${tx.ipt.args}` })
                    .toString(ECODE_BIN);
                if (await this.prisma.$exists.chaincodeInput({
                    fieldsHash: chaincodeInvokeParamsId,
                })) {
                    txData.chaincodeInvokeParams = {
                        connect: { fieldsHash: chaincodeInvokeParamsId },
                    };
                } else {
                    txData.chaincodeInvokeParams = {
                        create: {
                            fieldsHash: chaincodeInvokeParamsId,
                            function: tx.ipt.function,
                            args: { set: tx.ipt.args },
                        },
                    };
                }
            } else if (tx.type === 3) {
                txData.type = "CHAINCODE_SET_STATE";
                if (await this.InitStorager.prisma.$exists.chaincodeState({ state: tx.state })) {
                    txData.chaincodeSetStateParams = { connect: { state: tx.state } };
                } else {
                    txData.chaincodeSetStateParams = {
                        create: {
                            state: tx.state,
                        },
                    };
                }
                // console.log("3:action="+txData.action+",ipt="+txData.state);
            } else {
                // console.log("4:action="+txData.action+",ipt="+txData.ipt+",tx.type="+tx.type);
            }

            // console.log("---------data---------"+JSON.stringify(txData));
            const transactionWithAffluentInfo = await this.prisma.upsertTransaction({
                where: {
                    txid: txData.txid,
                },
                update: {
                    blockId: txData.blockId,
                    block: txData.block,
                },
                create: txData,
            }).$fragment(this.customObservable.syncTransactionObservable.txGraphqlFragment);

            try {
                this.customObservable.syncTransactionObservable
                    .notifyAll(transactionWithAffluentInfo);
            } catch (err) {
                console.log(`callback =${this.customObservable.syncTransactionObservable},invoke error=${err}`);
            }
            console.log(`save trans i=${cidx},txid=${txData.txid}`);
        }
    }
}

export default BlockStorager;
