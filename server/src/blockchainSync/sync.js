/*
 * Copyright  2018 Linkel Technology Co., Ltd, Beijing
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BA SIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import { rep } from "rclink/lib/protos/peer";
import { EventTube, RestAPI } from "rclink";
import BlockStorager from "./saveblock";

const { Block, Event } = rep.protos;

class Syncher {
    constructor(prisma, apiUrl, wsUrl, customObservable) {
        this.prisma = prisma;
        this.apiUrl = apiUrl;
        this.wsUrl = wsUrl;
        this.storage = new BlockStorager(prisma, this.apiUrl, customObservable);
        this.storage.InitStorager();
        this.isPulling = false;
        this.isPushing = false;
        return this;
    }


    startSyncPush() {
        const self = this;
        // 启动push方式的实时区块数据同步
        const et = new EventTube(this.wsUrl, ((evt) => {
            // console.log('+++++++entry evt');
            const msg = Event.decode(Buffer.from(evt.data));
            // console.log('$$$$$$$$$$message decode');
            // 出块通知 TODO 确保块内全部交易写入
            // console.log(JSON.stringify(msg));
            // console.log('msg.action='+msg.action+',msg.to='+msg.to);
            if (msg.action === 2 && msg.from !== "Block") {
                // self.storage.setBlockInc();
                // var blk = msg.blk;
                // console.log('########block')
                if (!self.isPulling && !self.isPushing) {
                    // console.log('push start pull');
                    self.isPushing = true;
                    self.storage.setBlockCount(msg.blk.height);
                    self.BlockToStore(Block.encode(msg.blk).finish())
                        .then(() => {
                            self.isPushing = false;
                            return 0;
                        });
                    // self.StartPullBlocks();
                    console.log("push start ok");
                }
            }
        }));
        self.et = et;
    }

    async StartPullBlocks() {
        // 启动pull方式的区块数据同步
        // console.log(JSON.stringify(this.storage));
        // console.log(this);
        const self = this;
        // If we are pulling or pushing already, just skip
        if (self.isPulling || self.isPushing) return;
        this.isPulling = true;
        const hLocal = this.storage.getLastSyncHeight();
        console.log(`hLocal=${hLocal}`);

        const hRemote = await this.getNetWorkHeight().catch((err) => {
            self.isPulling = false;
            console.error(err);
        });
        if (hRemote > -1) {
            console.log(`remoteheight=${hRemote}`);
            if (hRemote > hLocal) {
                // 需要拉取
                this.storage.setBlockCount(hRemote);
                const ra = new RestAPI(this.apiUrl);
                console.log(`prepare pull height=${hLocal + 1}`);
                this.pullBlock(ra, hLocal + 1, hRemote);
            } else {
                this.isPulling = false;
                console.log("StartPullBlocks,pull finish");
            }
        }
    }

    async getNetWorkHeight() {
        console.log(`remote-addr-rest = ${this.apiUrl}`);
        const ra = new RestAPI(this.apiUrl);
        // console.log("new ra ");
        const ci = await ra.chainInfo();
        // console.log("get ci ");
        if (ci != null) {
            console.log(JSON.stringify(ci));
            return ci.height;
        } 
        console.log(` chaininfo  is null=${this.apiUrl}`);
        return -1;
    }

    async BlockToStore(res) {
        const buf = res;
        const blk = Block.decode(buf);

        /* var crypto = require('crypto');
        var sha = crypto.createHash('SHA256');
        var blkHash = sha.update(buf).digest('hex'); */

        return this.storage.saveBlock(blk);
    }

    async pullBlock(ra, h, maxh) {
        const self = this;
        console.log(`pullBlock,prepare pull height=${h}`);
        const res = await ra.block(h, "STREAM").catch((err) => {
            self.isPulling = false;
            console.error(err);
        });
        const r = await this.BlockToStore(res);
        if (r === 1) {
            if (h < maxh) {
                this.pullBlock(ra, h + 1, maxh);
            } else {
                this.isPulling = false;
                console.log("pullBlock,pull finish");
            }
        }
    }
}


export default Syncher;
