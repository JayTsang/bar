const java = require("java");
const fs = require("fs");
const path = require("path");

const dependencieDir = path.join(__dirname, "jars");
const dependencies = fs.readdirSync(dependencieDir);
dependencies.forEach((element) => {
    java.classpath.push(path.join(dependencieDir, element));
});
const KryoInjection = java.import("com.twitter.chill.KryoInjection");

const [TCN_ACCOUNT, TCN_CERTIFICATE, TCN_KEYPAIR_ONLY_FOR_QUERY] = [ 
    "Account", "Certificate", "KeypairOnlyForQuery",
];
const TCNs = [TCN_ACCOUNT, TCN_CERTIFICATE, TCN_KEYPAIR_ONLY_FOR_QUERY];

const parseWorldStateValue = (targetClassName, value) => {
    if (!TCNs.includes(targetClassName)) {
        const errMsg1 = `Bad param targetClassName, ${targetClassName} is not supported`;
        const errMsg2 = `, need one of [${TCNs}]`;
        throw new Error(`${errMsg1}${errMsg2}`);
    }
    if (!Buffer.isBuffer(value)) {
        throw new TypeError("Bad type of param value, need Buffer");
    }

    let bytes = [...value];
    bytes = bytes.map(byte => java.newByte(byte));
    const byteArray = java.newArray("byte", bytes);

    switch (targetClassName) {
        case TCN_ACCOUNT: {
            const account = KryoInjection.invertSync(byteArray).getSync();
            return {
                name: account.nameSync(), 
                creditCode: account.creditCodeSync(),
                phone: account.mobileSync(),
            };
        }
        case TCN_CERTIFICATE: {
            const certificate = KryoInjection.invertSync(byteArray).getSync();
            return {
                status: certificate.certValidSync(),
                certPEM: certificate.certificateSync(), 
            };
        }
        case TCN_KEYPAIR_ONLY_FOR_QUERY: {
            const keypairOnlyForQuery = KryoInjection.invertSync(byteArray).getSync();
            return JSON.parse(keypairOnlyForQuery);
        }
        default: 
            return {};
    }
};

module.exports = parseWorldStateValue;
