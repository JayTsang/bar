import config from "config";
import path from "path";
import { storeFile } from "../fileAccess/fileAccess";
import serverInnerLogger from "../log/serverInnerLog";
import storeFileOperationProof from "../proofStore/proofStore";

const storeHandler = (req, res) => {
    Promise.all(req.files.map((file, index) => storeFile({
        tempPath: file.path,
        storagePath: path.join(config.get("File.fileStorageDir"), file.filename),
        originName: file.originalname,
        hashAlg: req.body.hashAlg,
        hash: req.body.fileHashes[index],
        ownerID: req.body.requesterID,
        timestamp: Date.now(), // 文件上传时间，不使用客户端时间，而使用服务端本地时间以屏蔽不同客户端之间以及客户端与服务端的时间误差
        signature: req.body.signature, 
    })))
        .then((result) => {
            serverInnerLogger.info(`Uploaded ${result.length} file(s) successfully, send the result message`);
            
            // 文件上传操作存证
            Promise.all(result.map(file => storeFileOperationProof({
                file,
                operationType: "UPLOAD",
                operationParams: {},
                requestParams: req.body,
            }))).then((proofs) => {
                serverInnerLogger.info(`Successfully store ${proofs.length} file upload proof(s)`);
                return 0;
            }).catch((err) => {
                serverInnerLogger.error("Unsuccessfully store file upload operation proof(s): ", err);
            });

            res.status(201)
                .json({
                    message: "File(s) uploaded successfully",
                    uploadedFileCount: result.length,
                    fileMetas: result,
                })
                .end();
            return 0;
        })
        .catch((err) => {
            serverInnerLogger.error("Uploaded unsuccessfully, error: ", err, "send the result message");
            res.status(500)
                .json({ message: `Error: ${err.message}` })
                .end();
            return 0;
        });
};

export default storeHandler;
