import { Router } from "express";
import validateHandler from "../validate/validateHandler";
import { proofListQueryHandler, proofQueryHandler } from "./proofsQueryHandler";

const proofsRouter = Router();

proofsRouter.get("/", [validateHandler.proofListQuery, proofListQueryHandler]);
proofsRouter.get("/:proofID", [validateHandler.proofQuery, proofQueryHandler]);
export default proofsRouter;
