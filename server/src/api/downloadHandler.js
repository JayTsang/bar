import config from "config";
import path from "path";
import { downloadFile } from "../fileAccess/fileAccess";
import serverInnerLogger from "../log/serverInnerLog";
import storeFileOperationProof from "../proofStore/proofStore";

const downloadHandler = (req, res) => {
    downloadFile(req.params.fileID)
        .then((fp) => {
            res.download(path.join(config.get("File.fileStorageDir"), fp), (err) => {
                if (err) {
                    serverInnerLogger.error(`Unsuccessfully download file with the path: ${fp}, error: `, err);
                } else {
                    serverInnerLogger.info(`Successfully download file with the path: ${fp}, send the file data to client`);
                    // 文件下载操作存证
                    // eslint-disable-next-line promise/no-promise-in-callback
                    storeFileOperationProof({
                        file: { fileID: req.params.fileID },
                        operationType: "DOWNLOAD",
                        operationParams: {},
                        requestParams: req.query,
                    }).then(() => {
                        serverInnerLogger.info("Successfully store 1 file download proof");
                        return 0;
                    }).catch((error) => {
                        serverInnerLogger.error("Unsuccessfully store file download operation proof: ", error);
                    });
                }
            });
            return 0;
        })
        .catch((e) => {
            serverInnerLogger.error("Unsuccessfully download file, error: ", e, "send the result message");
            if (e.message === `Not found fileMeta for the fileID: ${req.params.fileID}`) {
                res.status(404)
                    .json({ message: e.message })
                    .end();
            } else {
                res.status(500)
                    .json({ message: e.message })
                    .end();
            }
        });
};

export default downloadHandler;
