import { Router } from "express";
import validateHandler from "../validate/validateHandler";
import authVerificationHandler from "../accessControl/authVerification";
import {
    fileListQueryHandler, fileQueryHandler,
    uploadHandler, storeHandler,
    downloadHandler,
} from "./filesHandler";
import { 
    accessAuthListQueryHandler, accessAuthQueryHandler, 
    accessAuthCreateHandler, accessAuthUpdateHandler,
} from "./accessAuthsHandler";
import { proofListQueryHandler, proofQueryHandler } from "./proofsQueryHandler";

const filesRouter = Router();

// Match the files apis
// GET /files
filesRouter.get("/", [validateHandler.fileListQuery, fileListQueryHandler]);
// GET /files/:fileID
filesRouter.get("/:fileID", [validateHandler.fileQuery, fileQueryHandler]);
// POST /files
filesRouter.post("/", [uploadHandler, validateHandler.upload, storeHandler]);
// GET /files/:fileID/download
filesRouter.get("/:fileID/download", [validateHandler.download, authVerificationHandler, downloadHandler]);

// Match the secondary level accessAuths apis
// GET /files/:fileID/accesssAuths
filesRouter.get("/:fileID/accessAuths", [validateHandler.accessAuthListQuery, authVerificationHandler,
    accessAuthListQueryHandler]);
// GET /files/:fileID/accesssAuths/:accessAuthID
filesRouter.get("/:fileID/accessAuths/:accessAuthID", [validateHandler.accessAuthQuery, authVerificationHandler,
    accessAuthQueryHandler]);
// POST /files/:fileID/accessAuths
filesRouter.post("/:fileID/accessAuths", [validateHandler.accessAuthCreate, authVerificationHandler,
    accessAuthCreateHandler]);
// PUT /files/:fileID/accessAuths/:accessAuthID
filesRouter.put("/:fileID/accessAuths/:accessAuthID", [validateHandler.accessAuthUpdate, authVerificationHandler,
    accessAuthUpdateHandler]);

// Match the secondary level proofs apis 
// GET /files/:fileID/proofs
filesRouter.get("/:fileID/proofs", [validateHandler.proofListQuery, proofListQueryHandler]); 
// GET /files/:fileID/proofs/:proofID
filesRouter.get("/:fileID/proofs/:proofID", [validateHandler.proofQuery, proofQueryHandler]); 

export default filesRouter;
