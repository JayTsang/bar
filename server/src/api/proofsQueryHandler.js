import serverInnerLogger from "../log/serverInnerLog";
import { getProofCollection, getOneProof } from "../proofStore/proofMaintenance";

export const proofListQueryHandler = (req, res) => {
    const { 
        operationType, operatorID, status, sortBy, perPage, page,
    } = req.query;
    const fileID = req.params.fileID 
        ? { fileID: req.params.fileID, matchMode: "EQUAL" }
        : { fileID: req.query.fileID, matchMode: "CONTAINS" };
    getProofCollection({
        filter: {
            fileID, operationType, operatorID, status,
        },
        sortBy,
        pagination: { perPage, page },
    }).then((proofs) => {
        serverInnerLogger.info(
            "Successfully query file operation proof collection, send the result message",
        );
        res.status(200)
            .json({ ...proofs })
            .end();
        return 0;
    }).catch((err) => {
        serverInnerLogger.error(
            "Unsuccessfully query file operation proof collection, send the result message ",
            err);
        res.status(500)
            .json({ message: err })
            .end();
    });
};

export const proofQueryHandler = (req, res) => {
    getOneProof(req.params.proofID).then((proof) => {
        serverInnerLogger.info("Successfully query one file operation proof, send the result message");
        res.status(200)
            .json({ proof })
            .end();
        return 0;
    }).catch((err) => {
        serverInnerLogger.error("Unsuccessfully query one file operation proof, send the result message ", err);
        res.status(500)
            .json({ message: err })
            .end();
    });
};
