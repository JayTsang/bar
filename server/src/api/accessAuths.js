import { Router } from "express";
import validateHandler from "../validate/validateHandler";
import authVerificationHandler from "../accessControl/authVerification";
import { 
    accessAuthListQueryHandler, accessAuthQueryHandler, 
    accessAuthCreateHandler, accessAuthUpdateHandler,
} from "./accessAuthsHandler";

const accessAuthsRouter = Router();


// GET /accessAuths
accessAuthsRouter.get("/", [validateHandler.accessAuthListQuery, authVerificationHandler,
    accessAuthListQueryHandler]);
// GET /accessAuths/:accessAuthID
accessAuthsRouter.get("/:accessAuthID", [validateHandler.accessAuthQuery, authVerificationHandler,
    accessAuthQueryHandler]);
// POST /accessAuths
accessAuthsRouter.post("/", [validateHandler.accessAuthCreate, authVerificationHandler,
    accessAuthCreateHandler]);
// PUT /accessAuths/:accessAuthID
accessAuthsRouter.put("/:accessAuthID", [validateHandler.accessAuthUpdate, authVerificationHandler,
    accessAuthUpdateHandler]);

export default accessAuthsRouter;
