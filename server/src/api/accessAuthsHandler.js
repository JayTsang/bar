import {
    getFileACLCollection, getOneFileACL, createFileACL, updateFileACL,
} from "../accessControl/authMaintenance";
import serverInnerLogger from "../log/serverInnerLog";
import storeFileOperationProof from "../proofStore/proofStore";

export const accessAuthListQueryHandler = (req, res) => {
    getFileACLCollection({
        filter: {
            fileID: req.params.fileID ? req.params.fileID : req.query.fileID,
            objectUserID: req.query.objectUserID,
            accessAuthority: req.query.accessAuthority,
            accessRestrictionType: req.query.accessRestrictionType,
            accessPostProofStorage: req.query.accessPostProofStorage,
        },
        orderBy: req.query.sortBy,
        pagination: { page: req.query.page, perPage: req.query.perPage },
    }).then((fileACLs) => {
        serverInnerLogger.info(`Successfully query fileACL collection for ${req.query.fileID} , send the result message`);
        res.status(200)
            .json({ ...fileACLs })
            .end();
        return 0;
    }).catch((err) => {
        serverInnerLogger.error(`Unsuccessfully when querying fileACL collection for ${req.query.fileID}, error: `, err);
        res.status(500)
            .json({ message: err })
            .end();
    });
};

export const accessAuthQueryHandler = (req, res) => {
    getOneFileACL(req.params.accessAuthID).then((fileACL) => {
        if (!fileACL) {
            serverInnerLogger.error(`Unsuccessfully when querying one fileACL for ${req.params.accessAuthID}, not found`);
            res.status(404)
                .json({ message: "Not found accessAuth" })
                .end();
        } else {
            serverInnerLogger.info(`Successfully query one fileACL for ${req.params.accessAuthID}, send the result message`);
            res.status(200)
                .json({ fileACL })
                .end();
        }
        return 0;
    });
};

export const accessAuthCreateHandler = (req, res) => {
    const {
        objectUserID, accessAuthority, accessRestrictionType,
        accessRestrictionContent, accessPostProofStorage,
    } = req.body;
    const fileID = req.params.fileID ? req.params.fileID : req.body.fileID;
    return createFileACL({
        fileID,
        objectUserID,
        accessAuthority,
        accessRestrictionType,
        accessRestrictionContent,
        accessPostProofStorage,
    })
        .then((fileACL) => {
            serverInnerLogger.info(`Successfully update(create new) fileACL for file ${fileID}, send the result message`);

            storeFileOperationProof({
                file: { fileID, fileACLID: fileACL.fileIDCombineUserID },
                operationType: "AUTH_UPDATE",
                operationParams: {
                    authUpdateType: "CREATE",
                    authority: fileACL.authority,
                    objectUserID: fileACL.userID,
                    restrictionType: fileACL.restrictionType,
                    restrictionContent: fileACL.restrictionContent,
                    postActStoreProof: fileACL.postActStoreProof,
                },
                requestParams: req.body,
            }).then(() => {
                serverInnerLogger.info("Successfully store 1 file authority update(create new) proof");
                return 0;
            }).catch((error) => {
                serverInnerLogger.error("Unsuccessfully store file authority update(create new) operation proof: ", error);
            });

            res.status(200)
                .json({ fileACL })
                .end();
            return 0;
        })
        .catch((err) => {
            serverInnerLogger.error(`Failed to update(create new) fileACL for file ${fileID}, error: `, err, "send the result message");
            res.status(500)
                .json({ message: `${err.message}` })
                .end();
        });
};

export const accessAuthUpdateHandler = (req, res) => {
    const { accessAuthID } = req.params;
    const {
        accessAuthority, accessRestrictionContent,
        accessRestrictionType, accessPostProofStorage,
    } = req.body;
    updateFileACL({
        accessAuthID,
        accessAuthority,
        accessRestrictionType,
        accessRestrictionContent,
        accessPostProofStorage,
    }).then((fileACL) => {
        serverInnerLogger.info(`Successfully update fileACL for file ${fileACL.file.fileID}, send the result message`);

        storeFileOperationProof({
            file: { fileID: fileACL.file.fileID, fileACLID: fileACL.fileIDCombineUserID },
            operationType: "AUTH_UPDATE",
            operationParams: {
                authUpdateType: "UPDATE",
                authority: fileACL.authority,
                objectUserID: fileACL.userID,
                restrictionType: fileACL.restrictionType,
                restrictionContent: fileACL.restrictionContent,
                postActStoreProof: fileACL.postActStoreProof,
            },
            requestParams: req.body,
        }).then(() => {
            serverInnerLogger.info("Successfully store 1 file authority update(update existed) proof");
            return 0;
        }).catch((error) => {
            serverInnerLogger.error("Unsuccessfully store file authority update(update existed) operation proof: ", error);
        });

        res.status(200)
            .json({ fileACL })
            .end();

        return 0;
    }).catch((err) => {
        serverInnerLogger.error(`Failed to update(update existed) fileACL for accessAuthID: ${accessAuthID}, error: `, err, "send the result message");
        res.status(500)
            .json({ message: `${err.message}` })
            .end();
    });
};
