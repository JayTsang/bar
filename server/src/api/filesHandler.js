import getFileList from "../fileList/fileList";
import serverInnerLogger from "../log/serverInnerLog";
import { getFile } from "../fileAccess/fileMetaMaintenance";

export const fileListQueryHandler = (req, res) => {
    const {
        fileID, name, hash, 
        uploadTimeFrom, uploadTimeTo, 
        sizeMin, sizeMax, 
        shareStatus, 
        sortBy, 
        perPage, page,
    } = req.query;
    const queryCriteria = {
        filter: {
            fileID,
            name,
            hash,
            uploadTime: { from: uploadTimeFrom, to: uploadTimeTo },
            size: { min: sizeMin, max: sizeMax },
            accessAuth: { shareStatus, userID: req.query.requesterID },
        },
        orderBy: sortBy,
        pagination: { perPage, page },
    };
    return getFileList(queryCriteria).then((fileList) => {
        serverInnerLogger.info("Successfully query fileList, send the result message");
        res.status(200)
            .json({ ...fileList })
            .end();
        return 0;
    }).catch((err) => {
        serverInnerLogger.error("Unsuccessfully query fileList, send the result message, ", err);
        res.status(500)
            .json({ message: err })
            .end();
        return 0;
    });
};

export const fileQueryHandler = (req, res) => {
    const { fileID } = req.params;
    return getFile(fileID).then((file) => {
        serverInnerLogger.info("Successfully query file, send the result message");
        res.status(200)
            .json({ file })
            .end();
        return 0;
    }).catch((err) => {
        serverInnerLogger.error("Unsuccessfully query file, send the result message, ", err);
        res.status(500)
            .json({ message: err })
            .end();
        return 0;
    });
};

export { default as uploadHandler } from "./uploadHandler";
export { default as storeHandler } from "./storeHandler";
export { default as downloadHandler } from "./downloadHandler";
