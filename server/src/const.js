export const COMBINATION_DELIMITER = "-COMBDELI-";

export const proofWithLinkTypeIDFragment = `
    fragment ProofWithLinkTypeID on Proof { 
        id
        file {
            fileID
        }
        operationType
        operationParams
        operatorID
        txid
        transaction {
            id
        }
        txTimestamp
        blockHash
        blockTimestamp
        status
    }
`;

export const prismaFragmentFileACLWithFileID = `
    fragment FileACLWithFileID on fileACL {
        fileIDCombineUserID 
        userID
        authority
        restrictionType
        restrictionContent
        postActStoreProof
        file {
            fileID
        }
    }
`;

export const prismaFragmentFileACLOnlyWithFileID = `
    fragment FileACLOnlyWithFileID on fileACL {
        file {
            fileID
        }
    }
`;
