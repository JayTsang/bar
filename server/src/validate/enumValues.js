export const AccessRestrictionEnumTypes = [
    "TIME_RANGE", 
    "COUNT", 
    "UNRESTRICTED",
];
export const FileShareEnumStatus = [
    "OWNED",
    "OWNED_AND_SHARED_TO_OTHERS",
    "OWNED_AND_NOT_SHARED_TO_OTHERS",
    "SHARED_FROM_OTHERS",
    "ALL",
];
export const OperationEnumTypes = [
    "UPLOAD",
    "DOWNLOAD",
    "AUTH_UPDATE",
];
export const ProofEnumStatus = [
    "CONFIRMED", 
    "WAITING", 
    "FAILED",
]; 
export const MatchEnumModes = [
    "EQUAL", 
    "CONTAINS",
];

export const ProofContractFunctionEnumNames = [
    "fileDeliverAndStore",
    "fileDownload",
    "fileAccessAuthAlter",
];
