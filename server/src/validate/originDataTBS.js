import _ from "lodash";

const getOriginDataToBeSigned = (fields = {}, concat = "010") => {
    // Concatenate fields with the concat string according to the alphabetic order of the keys
    const sortedFields = [];
    _(fields).keys().sort().each((key) => {
        if (key !== "files" && key !== "signature") {
            sortedFields.push(fields[key]); 
        }
    });
    let originDataToBeSigned = "";
    sortedFields.forEach((e) => {
        originDataToBeSigned += `${concat}${e}`;
    });
    return originDataToBeSigned;
};
export default getOriginDataToBeSigned;
