import { check, validationResult } from "express-validator/check";
import validator from "validator";
import { GetHashVal, VerifySign } from "rclink/lib/crypto";
import fs from "fs";
import _ from "lodash";
import { sanitize } from "express-validator/filter";
import getPubkey from "../account/getPubkey";
import getOriginDataToBeSigned from "./originDataTBS";
import serverInnerLogger from "../log/serverInnerLog";
import {
    FileShareEnumStatus, AccessRestrictionEnumTypes, OperationEnumTypes, ProofEnumStatus,
} from "./enumValues";

const nonEmptyErrorMsg = field => `The ${field} is required and should not be empty`;

// Common fields validation for every request 
let signAlg;
let requesterID;
let certName;
const commonFieldValidators = [
    check("requesterID").not().isEmpty()
        .withMessage(nonEmptyErrorMsg("requesterID"))
        .customSanitizer((value) => {
            requesterID = value;
            return value;
        }),
    check("timestamp").not().isEmpty()
        .withMessage(nonEmptyErrorMsg("timestamp"))
        .isLength({ min: 13, max: 13 })
        .withMessage("The timestamp's length should be 13")
        .isInt({ min: 0 })
        .withMessage("The timestamp should be a unix timestamp milliseconds value"),
    check("signAlg").not().isEmpty()
        .withMessage(nonEmptyErrorMsg("signAlg"))
        .customSanitizer((value) => {
            signAlg = value;
            return value;
        }),
    check("certName").not().isEmpty()
        .withMessage(nonEmptyErrorMsg("certName"))
        .customSanitizer((value) => {
            certName = value;
            return value;
        }),
    check("urlPath").not().isEmpty()
        .withMessage(nonEmptyErrorMsg("urlPath")),
    check("signature").not().isEmpty()
        .withMessage(nonEmptyErrorMsg("signature"))
        .custom((value, { req }) => getPubkey(requesterID, certName).then(({
            pubKey, status, isRestricted,
        }) => {
            if (!pubKey) throw new Error("Canot find the user public key or certificate");
            if (!status) throw new Error("The user certificate is forbidden to use any more");
            if (isRestricted
                && (req.method === "POST"
                    || req.method === "PUT"
                    || (req.method === "GET" && req.baseUrl === "/files" && req.route.path === "/:fileID/download")
                )
            ) {
                throw new Error("The keypair is restricted to query only already");
            }
            const originData = getOriginDataToBeSigned(req.method === "GET" ? req.query : req.body);
            if (!VerifySign({
                pubKey, sigValue: Buffer.from(value, "hex"), data: originData, alg: signAlg,
            })) {
                throw new Error("Bad signature or signAlg");
            }
            return true;
        })),
];

const timestampSanitizer = sanitize("timestamp").toInt(10);
const commonFieldSanitizers = [timestampSanitizer];

// Common fields validation for resource list query (files,accessAuths,proofs)
const commonResourceListQueryValidators = [
    check("sortBy").custom((value) => {
        if (value && !_.endsWith(value, "_ASC") && !_.endsWith(value, "_DESC")) {
            throw new Error("The sortBy must end with _ASC or with _DESC");
        }
        return true;
    }),
    check("perPage").custom((value) => {
        if (value && !validator.isInt(value) && !_.inRange(parseInt(value, 10), 0, 2 ** 31)) {
            throw new Error("The perPage must be an integer in range[0, 2**31)");
        }
        return true;
    }),
    check("page").custom((value) => {
        if (value && !validator.isInt(value) && !_.inRange(parseInt(value, 10), 0, 2 ** 31)) {
            throw new Error("The page must be an integer in range[0, 2**31)");
        }
        return true;
    }),
];

const paginationSanitizers = [
    sanitize("perPage").toInt(10),
    sanitize("page").toInt(10),
];

// Fields validation for GET /files (file list query request)
const fileListQueryValidators = [
    check("uploadTimeFrom").custom((value) => {
        if (value && !validator.isInt(value)) {
            throw new Error("The uploadTimeFrom must be an integer which is unix timestamp millisecond");
        }
        return true;
    }),
    check("uploadTimeTo").custom((value) => {
        if (value && !validator.isInt(value)) {
            throw new Error("The uploadTimeTo must be an integer which is unix timestamp millisecond");
        }
        return true;
    }),
    check("sizeMin").custom((value) => {
        if (value && !validator.isInt(value)) {
            throw new Error("The sizeMin must be an integer");
        }
        return true;
    }),
    check("sizeMax").custom((value) => {
        if (value && !validator.isInt(value)) {
            throw new Error("The sizeMax must be an integer");
        }
        return true;
    }),
    check("shareStatus").custom((value) => {
        if (value && !validator.isIn(value, FileShareEnumStatus)) {
            throw new Error(`The shareStatus must be one of ${FileShareEnumStatus}`);
        }
        return true;
    }),
].concat(commonResourceListQueryValidators).concat(commonFieldValidators);

const fileListQuerySanitizers = [
    sanitize("uploadTimeFrom").toInt(10),
    sanitize("uploadTimeTo").toInt(10),
    sanitize("sizeMin").toInt(10),
    sanitize("sizeMax").toInt(10),
].concat(paginationSanitizers);

// Fields validation for GET /files/:fileID (single file meta query request)
const fileQueryValidators = [].concat(commonFieldValidators);

// Fields validation for POST /files (file upload request)
const uploadValidators = [
    check("files").not().isEmpty()
        .withMessage(nonEmptyErrorMsg("files")),
    check("hashAlg").not().isEmpty()
        .withMessage(nonEmptyErrorMsg("hashAlg")),
    check("fileHashes").not().isEmpty()
        .withMessage(nonEmptyErrorMsg("fileHashes"))
        .custom((value, { req }) => {
            if (!value) return true; // if fileHashes is undefined, use the previous error message
            let fileHashes;
            try {
                fileHashes = JSON.parse(value);
                if (!_.isArray(fileHashes)) {
                    throw new Error();
                }
                _(fileHashes).each((hash) => {
                    if (!_.isString(hash)) throw new Error();
                });
            } catch (e) {
                throw new Error(`The fileHashes "${value}" is a bad json string of Array[String]`);
            }
            if (!req.files || fileHashes.length !== req.files.length) {
                throw new Error("The files and fileHashes contain different element counts");
            }
            _(fileHashes).each((element, index) => {
                if (!validator.isHash(element, req.body.hashAlg)) {
                    throw new Error(`The file hash "${element}" is not a hash of algorithm ${req.body.hashAlg}`);
                }
                const hash = GetHashVal({ data: fs.readFileSync(req.files[index].path), alg: req.body.hashAlg }).toString("hex");
                if (hash !== element) {
                    throw new Error(`The file hash "${element}" mismatched the file uploaded`);
                }
            });
            return true;
        }),
].concat(commonFieldValidators);

const fileHashesSanitizer = sanitize("fileHashes").customSanitizer(value => JSON.parse(value));

// Fields validation for GET /files/:fileID/download (file download request)
const downloadValidators = [].concat(commonFieldValidators);

// Fields validation for 
// POST /accessAuths, 
// POST /files/:fileID/accessAuths (access authority create request)
const accessAuthCreateValidators = [
    check("fileID").custom((value, { req }) => {
        if (!value && !req.params.fileID) return false;
        return true;
    }).withMessage(nonEmptyErrorMsg("fileID")),
    check("objectUserID").not().isEmpty()
        .withMessage(nonEmptyErrorMsg("objectUserID")),
    check("accessAuthority").not().isEmpty()
        .withMessage(nonEmptyErrorMsg("accessAuthority"))
        .isInt({ min: 0, max: 15 })
        .withMessage("The accessAuthority should be an integer between 0 and 15"),
    check("accessRestrictionType").not().isEmpty()
        .withMessage(nonEmptyErrorMsg("accessRestrictionType"))
        .isIn(AccessRestrictionEnumTypes)
        .withMessage(`The accessRestrictionType should be one of ${AccessRestrictionEnumTypes}`),
    check("accessRestrictionContent").not().isEmpty()
        .withMessage(nonEmptyErrorMsg("accessRestrictionContent"))
        .isJSON()
        .withMessage("The accessRestrictionContent should be a json string"),
    check("accessPostProofStorage").not().isEmpty()
        .withMessage(nonEmptyErrorMsg("accessPostProofStorage"))
        .isInt({ min: 0, max: 15 })
        .withMessage("The accessPostProofStorage should be an integer between 0 and 15"),

].concat(commonFieldValidators);

const accessAuthCreateOrUpdateSanitizers = [
    sanitize("accessAuthority").toInt(10),
    sanitize("accessPostProofStorage").toInt(10),
    sanitize("accessRestrictionContent")
        .customSanitizer(value => JSON.parse(value)),
];

// Fields validation for 
// PUT /accessAuths/:accessAuthID, 
// PUT /files/:fileID/accessAuths/:accessAuthID (access authority update request)
const accessAuthUpdateValidators = [
    check("accessAuthority").custom((value) => {
        if (value && !_.inRange(value, 0, 16)) {
            return false;
        }
        return true;
    }).withMessage("The accessAuthority should be an integer between 0 and 15"),
    check("accessRestrictionType").custom((value) => {
        if (value && !validator.isIn(value, AccessRestrictionEnumTypes)) {
            return false;
        }
        return true;
    }).withMessage(`The accessRestrictionType should be one of ${AccessRestrictionEnumTypes}`),
    check("accessRestrictionContent").custom((value) => {
        if (value && !validator.isJSON(value)) {
            return false;
        }
        return true;
    }).withMessage("The accessRestrictionContent should be a json string"),
    check("accessPostProofStorage").custom((value) => {
        if (value && !_.inRange(value, 0, 16)) {
            return false;
        }
        return true;
    }).withMessage("The accessPostProofStorage should be an integer between 0 and 15"),
].concat(commonFieldValidators);

// Field validation for GET /accessAuths, GET /files/:fileID/accessAuths
const accessAuthListQueryValidators = [
    check("fileID").custom((value, { req }) => {
        if (!value && !req.params.fileID) return false;
        return true;
    }).withMessage(nonEmptyErrorMsg("fileID")),
].concat([
    accessAuthUpdateValidators[0],
    accessAuthUpdateValidators[1],
    accessAuthUpdateValidators[3],
]).concat(commonResourceListQueryValidators)
    .concat(commonFieldValidators);

const accessAuthListQuerySanitizers = accessAuthCreateOrUpdateSanitizers.slice(0, 2)
    .concat(paginationSanitizers);

// Fields validation for 
// GET /accessAuths/:accessAuthID, 
// GET /files/:fileID/accessAuths/:accessAuthID
const accessAuthQueryValidators = [].concat(commonFieldValidators);

// Fields validation for 
// GET /proofs 
// GEt /files/:fileID/proofs (file operation proof list query request)
const proofListQueryValidators = [
    check("operationType").custom((value) => {
        if (value && !validator.isIn(value, OperationEnumTypes)) {
            return false;
        }
        return true;
    }).withMessage(`The operationType should be one of ${OperationEnumTypes}`),
    check("status").custom((value) => {
        if (value && !validator.isIn(value, ProofEnumStatus)) {
            return false;
        }
        return true;
    }).withMessage(`The operationType should be one of ${ProofEnumStatus}`),
].concat(commonResourceListQueryValidators).concat(commonFieldValidators);

const proofListQuerySanitizers = [].concat(paginationSanitizers);

// Fields validation for
// GET /proofs/:proofsID
// GET /files/:fileID/proofs/:proofsID
const proofQueryValidators = [].concat(commonFieldValidators);

const fieldErrorHandler = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        serverInnerLogger.error(`Not pass the request params validation for request ${req.method}-${req.originalUrl}`, "send the result message");
        res.status(400).json({
            message: "Bad request, with error(s)",
            errors: errors.array(),
        });
    } else {
        serverInnerLogger.info(`Pass the request params validation for request ${req.method}-${req.originalUrl}`);
        next();
    }
};

const validateHandlers = {
    fileListQuery:
        fileListQueryValidators.concat([fieldErrorHandler,
            ...fileListQuerySanitizers, ...commonFieldSanitizers]),
    fileQuery:
        fileQueryValidators.concat([fieldErrorHandler, ...commonFieldSanitizers]),
    upload: uploadValidators.concat([fieldErrorHandler,
        fileHashesSanitizer, ...commonFieldSanitizers]),
    download:
        downloadValidators.concat([fieldErrorHandler, ...commonFieldSanitizers]),
    accessAuthListQuery:
        accessAuthListQueryValidators.concat([fieldErrorHandler,
            ...accessAuthListQuerySanitizers, ...commonFieldSanitizers]),
    accessAuthQuery:
        accessAuthQueryValidators.concat([fieldErrorHandler, ...commonFieldSanitizers]),
    accessAuthCreate:
        accessAuthCreateValidators.concat([fieldErrorHandler,
            ...accessAuthCreateOrUpdateSanitizers, ...commonFieldSanitizers]),
    accessAuthUpdate:
        accessAuthUpdateValidators.concat([fieldErrorHandler,
            ...accessAuthCreateOrUpdateSanitizers, ...commonFieldSanitizers]),
    proofListQuery:
        proofListQueryValidators.concat([fieldErrorHandler,
            ...proofListQuerySanitizers, ...commonFieldSanitizers]),
    proofQuery:
        proofQueryValidators.concat([fieldErrorHandler, ...commonFieldSanitizers]),
    default:
        (req, res, next) => next(),
};

export default validateHandlers;
