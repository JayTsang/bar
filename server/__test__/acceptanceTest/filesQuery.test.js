import request from "supertest";
import { Sign } from "rclink/lib/crypto";
import app, { server } from "../../src/server";
import getOriginDataToBeSigned from "../../src/validate/originDataTBS";
import { createSomeTestRecords, clearTestRecords } from "../helpers/prismaOps";

describe("Files query API test", () => {
    const testIdentifier = "files-api-test";
    const agent = request(app);
    const baseUrl = "/files";
    const signAlg = "sha256";
    let testFileMetas;
    let testUsers;

    beforeAll(async () => createSomeTestRecords({
        userCount: 2,
        fileOwnedCountEachUser: 100,
        fileSharedCountEachUser: 10,
        identifier: testIdentifier,
    }).then((res) => {
        testFileMetas = res.fileMetas;
        testUsers = res.users;
        return 0;
    }), 15 * 1000);

    afterAll(() => clearTestRecords(testIdentifier).then(server.close()));

    describe("GET /files API test, query file list", () => {
        test("Should get success when querying file list with good params", async () => {
            const queryWithoutSignature = {
                fileID: testIdentifier,
                uploadTimeFrom: new Date(0).getTime(),
                sizeMin: 1,
                shareStatus: "OWNED",
                sortBy: "size_DESC",
                page: 1,
                perPage: 5,
                requesterID: testUsers[0].userID,
                timestamp: Date.now(),
                signAlg,
                certName: testUsers[0].certificateName,
                urlPath: baseUrl,
            };
            await agent.get(baseUrl)
                .query({
                    ...queryWithoutSignature,
                    signature: Sign({ prvKey: testUsers[0].prvKeyPEM, data: getOriginDataToBeSigned(queryWithoutSignature, "010"), alg: signAlg })
                        .toString("hex"),
                }).then((res) => {
                    expect(res.status).toBe(200);
                    expect(res.body.totalCount).toBe(100);
                    expect(res.body.fileList.length).toBeGreaterThan(0);
                    expect(res.body.fileList.length)
                        .toBeLessThanOrEqual(queryWithoutSignature.perPage);
                    expect(res.body.fileList[0].uploadTime
                    >= new Date(queryWithoutSignature.uploadTimeFrom).toISOString(),
                    ).toBeTruthy();
                    expect(res.body.fileList[0].size)
                        .toBeGreaterThanOrEqual(queryWithoutSignature.sizeMin);
                    return 0;
                });
        });

    // TODO: implement failed test
    });
    describe("GET /files/:fileID API test", () => {
        test("Should get success when querying a single file with good params", () => {
            const url = `${baseUrl}/${testFileMetas[0].fileID}`;
            const queryWithoutSignature = {
                requesterID: testUsers[0].userID, 
                timestamp: Date.now(), 
                signAlg,
                certName: testUsers[0].certificateName,
                urlPath: url,
            };
            return agent.get(url)
                .query({
                    ...queryWithoutSignature,
                    signature: Sign({ prvKey: testUsers[0].prvKeyPEM, data: getOriginDataToBeSigned(queryWithoutSignature, "010"), alg: signAlg }).toString("hex"),
                }).then((res) => {
                    expect(res.status).toBe(200);
                    expect(res.body.file).toBeDefined();
                    expect(res.body.file.fileID).toBe(testFileMetas[0].fileID);
                    return 0;
                });
        });
    });
    // TODO: implement failed test
});
