/* eslint-disable max-len */
import request from "supertest";
import { Sign } from "rclink/lib/crypto";
// import path from "path";
// import fs from "fs-extra";
import app, { server } from "../../src/server";
import getOriginDataToBeSigned from "../../src/validate/originDataTBS";
import { clearTestRecords, createSomeTestRecords } from "../helpers/prismaOps";
import { COMBINATION_DELIMITER } from "../../src/const";

describe("AccessAuths API test", () => {
    let requesterID;
    const signAlg = "sha256";
    let certName;
    let prvk;
    const agent = request(app);
    const baseUrl = "/accessAuths";

    const testIdentifier = "accessAuths-api-test";
    let testFileMetas;
    let testFileACLs;
    let testUsers;
    beforeAll(() => createSomeTestRecords(
        {
            userCount: 2,
            fileOwnedCountEachUser: 5,
            fileSharedCountEachUser: 2,
            identifier: testIdentifier,
        },
    ).then((res) => {
        testFileMetas = res.fileMetas;
        testFileACLs = res.fileACLs;
        testUsers = res.users;
        requesterID = testUsers[0].userID;
        certName = testUsers[0].certificateName;
        prvk = testUsers[0].prvKeyPEM;
        return 0;
    }));
    afterAll(() => clearTestRecords(testIdentifier).then(server.close()));

    describe("Create fileACL API test", () => {
        const objectUserIDPrefix = "user-to-grant";

        afterAll(() => clearTestRecords(testIdentifier, objectUserIDPrefix));

        describe("Create fileACL from primary level API test", () => {
            it("Should get success when creating a new fileACL for an existed and authorized fileID", async () => {
                const url = baseUrl;
                const bodyWithoutSignature = {
                    fileID: testFileMetas[0].fileID,
                    objectUserID: `${objectUserIDPrefix}-0`,
                    accessAuthority: 8,
                    accessRestrictionType: "TIME_RANGE",
                    accessRestrictionContent: JSON.stringify({
                        from: new Date().toISOString(),
                        to: new Date(Date.now() + 3000).toISOString(),
                    }),
                    accessPostProofStorage: 9,
                    requesterID,
                    timestamp: Date.now(),
                    signAlg,
                    certName,
                    urlPath: url,
                };
                await agent.post(url)
                    .send({
                        ...bodyWithoutSignature,
                        signature: Sign({ prvKey: prvk, data: getOriginDataToBeSigned(bodyWithoutSignature, "010"), alg: signAlg })
                            .toString("hex"),
                    }).then((res) => {
                        expect(res.status).toBe(200);
                        expect(res.body.fileACL).toBeDefined();
                        expect(res.body.fileACL.fileIDCombineUserID)
                            .toBe(`${bodyWithoutSignature.fileID}${COMBINATION_DELIMITER}${bodyWithoutSignature.objectUserID}`);
                        return 0;
                    });
            });
        }); 
        describe("Create fileACL from secondary level API test", () => {
            it("Should get success when creating a new fileACL for an existed and authorized fileID", async () => {
                const url = `/files/${testFileMetas[0].fileID}/accessAuths`;
                const bodyWithoutSignature = {
                    objectUserID: `${objectUserIDPrefix}-1`,
                    accessAuthority: 8,
                    accessRestrictionType: "TIME_RANGE",
                    accessRestrictionContent: JSON.stringify({
                        from: new Date().toISOString(),
                        to: new Date(Date.now() + 3000).toISOString(),
                    }),
                    accessPostProofStorage: 9,
                    requesterID,
                    timestamp: Date.now(),
                    signAlg,
                    certName,
                    urlPath: url,
                };
                await agent.post(url)
                    .send({
                        ...bodyWithoutSignature,
                        signature: Sign({ prvKey: prvk, data: getOriginDataToBeSigned(bodyWithoutSignature, "010"), alg: signAlg })
                            .toString("hex"),
                    }).then((res) => {
                        expect(res.status).toBe(200);
                        expect(res.body.fileACL).toBeDefined();
                        expect(res.body.fileACL.fileIDCombineUserID)
                            .toBe(`${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${bodyWithoutSignature.objectUserID}`);
                        return 0;
                    });
            });
        }); 
    });
    describe("Update fileACL API test", () => {
        describe("Update fileACL from primary level API test", () => {
            it("Should get success when updating a fileACL for an existed and authorized fileIDCombinedUserID", async () => {
                const url = `${baseUrl}/${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[1].userID}`;
                const bodyWithoutSignature = {
                    // accessAuthority: 10,
                    accessPostProofStorage: 11,
                    requesterID,
                    timestamp: Date.now(),
                    signAlg,
                    certName,
                    urlPath: url,
                };
                await agent.put(url)
                    .send({
                        ...bodyWithoutSignature,
                        signature: Sign({ prvKey: prvk, data: getOriginDataToBeSigned(bodyWithoutSignature, "010"), alg: signAlg })
                            .toString("hex"),
                    }).then((res) => {
                        expect(res.status).toBe(200);
                        expect(res.body.fileACL).toBeDefined();
                        expect(res.body.fileACL.fileIDCombineUserID)
                            .toBe(`${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[1].userID}`);
                        return 0;
                    }).catch((err) => {
                        console.error(err);
                    });
            });
        }); 
        describe("Update fileACL from secondary level API test", () => {
            it("Should get success when updating a fileACL for an existed and authorized fileIDCombineUserID", async () => {
                const url = `/files/${testFileMetas[0].fileID}/accessAuths/${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[1].userID}`;
                const bodyWithoutSignature = {
                    accessRestrictionContent: JSON.stringify({
                        from: new Date().toISOString(),
                        to: new Date(Date.now() + 3000).toISOString(),
                    }),
                    requesterID,
                    timestamp: Date.now(),
                    signAlg,
                    certName,
                    urlPath: url,
                };
                await agent.put(url)
                    .send({
                        ...bodyWithoutSignature,
                        signature: Sign({ prvKey: prvk, data: getOriginDataToBeSigned(bodyWithoutSignature, "010"), alg: signAlg })
                            .toString("hex"),
                    }).then((res) => {
                        expect(res.status).toBe(200);
                        expect(res.body.fileACL).toBeDefined();
                        expect(res.body.fileACL.fileIDCombineUserID)
                            .toBe(`${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[1].userID}`);
                        return 0;
                    });
            });
        }); 
    });
    describe("Query fileACL collection API test", () => {
        describe("Query fileACL collection from primary level api test", () => {
            it("Should get success when querying fileACL collection with the specific fileID", () => {
                const { fileID } = testFileMetas[0]; 
                const url = baseUrl; 
                const queryWithoutSignature = {
                    fileID,
                    perPage: 10,
                    page: 1,
                    requesterID,
                    timestamp: Date.now(),
                    signAlg,
                    certName,
                    urlPath: url,
                };
                return agent.get(url)
                    .query({
                        ...queryWithoutSignature,
                        signature: Sign({ prvKey: prvk, data: getOriginDataToBeSigned(queryWithoutSignature, "010"), alg: signAlg })
                            .toString("hex"),
                    }).then((res) => {
                        expect(res.status).toBe(200);
                        expect(res.body.fileACLs).toBeDefined();
                        expect(res.body.totalCount).toBe(2);
                        expect(res.body.fileACLs.length).toBe(2);
                        expect(res.body.fileACLs[0].fileIDCombineUserID).toBe(`${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[0].userID}`);
                        expect(res.body.fileACLs[1].fileIDCombineUserID).toBe(`${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[1].userID}`);
                        return 0;
                    });
            });

            it("Should get success when querying fileACL collection with the specific objectUserID", () => {
                const { fileID } = testFileMetas[0]; 
                const url = baseUrl; 
                const queryWithoutSignature = {
                    fileID,
                    requesterID,
                    objectUserID: testUsers[0].userID,
                    timestamp: Date.now(),
                    signAlg,
                    certName,
                    urlPath: url,
                };
                return agent.get(url)
                    .query({
                        ...queryWithoutSignature,
                        signature: Sign({ prvKey: prvk, data: getOriginDataToBeSigned(queryWithoutSignature, "010"), alg: signAlg })
                            .toString("hex"),
                    }).then((res) => {
                        expect(res.status).toBe(200);
                        expect(res.body.fileACLs).toBeDefined();
                        expect(res.body.totalCount).toBe(1);
                        expect(res.body.fileACLs.length).toBe(1);
                        expect(res.body.fileACLs[0].fileIDCombineUserID).toBe(`${fileID}${COMBINATION_DELIMITER}${testUsers[0].userID}`);
                        return 0;
                    });
            });

            // TODO: failed test implement
        });
        describe("Query fileACL collection from secondary level api test", () => {
            it("Should get success when querying fileACL collection", () => {
                const { fileID } = testFileMetas[0]; 
                const url = `/files/${fileID}/accessAuths`; 
                const queryWithoutSignature = {
                    requesterID,
                    timestamp: Date.now(),
                    signAlg,
                    certName,
                    urlPath: url,
                };
                return agent.get(url)
                    .query({
                        ...queryWithoutSignature,
                        signature: Sign({ prvKey: prvk, data: getOriginDataToBeSigned(queryWithoutSignature, "010"), alg: signAlg })
                            .toString("hex"),
                    }).then((res) => {
                        expect(res.status).toBe(200);
                        expect(res.body.fileACLs).toBeDefined();
                        expect(res.body.totalCount).toBe(2);
                        expect(res.body.fileACLs.length).toBe(2);
                        expect(res.body.fileACLs[0].fileIDCombineUserID).toBe(`${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[0].userID}`);
                        expect(res.body.fileACLs[1].fileIDCombineUserID).toBe(`${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[1].userID}`);
                        return 0;
                    });
            });

            it("Should get success when querying fileACL collection with the specific objectUserID", () => {
                const { fileID } = testFileMetas[0]; 
                const url = `/files/${fileID}/accessAuths`; 
                const queryWithoutSignature = {
                    requesterID,
                    objectUserID: testUsers[0].userID,
                    timestamp: Date.now(),
                    signAlg,
                    certName,
                    urlPath: url,
                };
                return agent.get(url)
                    .query({
                        ...queryWithoutSignature,
                        signature: Sign({ prvKey: prvk, data: getOriginDataToBeSigned(queryWithoutSignature, "010"), alg: signAlg })
                            .toString("hex"),
                    }).then((res) => {
                        expect(res.status).toBe(200);
                        expect(res.body.fileACLs).toBeDefined();
                        expect(res.body.fileACLs.length).toBe(1);
                        expect(res.body.fileACLs[0].fileIDCombineUserID).toBe(`${fileID}${COMBINATION_DELIMITER}${testUsers[0].userID}`);
                        return 0;
                    });
            });
        });
    });
    describe("Query fileACL api test", () => {
        describe("Query fileACL from primary level api test", () => {
            it("Should get success when querying fileACL with an existed and authorized accessAuthID", () => {
                const url = `${baseUrl}/${testFileACLs[0].fileIDCombineUserID}`; 
                const queryWithoutSignature = {
                    requesterID,
                    timestamp: Date.now(),
                    signAlg,
                    certName,
                    urlPath: url,
                };
                return agent.get(url)
                    .query({
                        ...queryWithoutSignature,
                        signature: Sign({ prvKey: prvk, data: getOriginDataToBeSigned(queryWithoutSignature, "010"), alg: signAlg })
                            .toString("hex"),
                    }).then((res) => {
                        expect(res.status).toBe(200);
                        expect(res.body.fileACL).toBeDefined();
                        expect(res.body.fileACL.fileIDCombineUserID).toBe(`${testFileACLs[0].fileIDCombineUserID}`);
                        return 0;
                    });               
            }); 
            it("Should get failed when querying fileACL with a non-existed accessAuthID", (done) => {
                const url = `${baseUrl}/non-existed`; 
                const queryWithoutSignature = {
                    requesterID,
                    timestamp: Date.now(),
                    signAlg,
                    certName,
                    urlPath: url,
                };
                return agent.get(url)
                    .query({
                        ...queryWithoutSignature,
                        signature: Sign({ prvKey: prvk, data: getOriginDataToBeSigned(queryWithoutSignature, "010"), alg: signAlg })
                            .toString("hex"),
                    }).end((err, res) => {
                        expect(res.status).toBe(404);
                        expect(res.body.message).toBe("Not found accessAuth");
                        done();
                    });               
            });
            it("Should get failed when querying fileACL with a non-authorized accessAuthID", (done) => {
                const url = `${baseUrl}/${testFileACLs[0].fileIDCombineUserID}`; 
                const queryWithoutSignature = {
                    requesterID: testUsers[1].userID,
                    timestamp: Date.now(),
                    signAlg,
                    certName: testUsers[1].certificateName,
                    urlPath: url,
                };
                agent.get(url)
                    .query({
                        ...queryWithoutSignature,
                        signature: Sign({ prvKey: testUsers[1].prvKeyPEM, data: getOriginDataToBeSigned(queryWithoutSignature, "010"), alg: signAlg })
                            .toString("hex"),
                    }).end((err, res) => {
                        expect(res.status).toBe(401);
                        expect(res.body.message).toBe("You are unauthorized to make this request");
                        done();
                    });               
            });
        }); 
        describe("Query fileACL from secondary level api test", () => {
            it("Should get success when querying fileACL with an existed and authorized accessAuthID", () => {
                const url = `/files/${testFileMetas[0].fileID}/accessAuths/${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[0].userID}`; 
                const queryWithoutSignature = {
                    requesterID,
                    timestamp: Date.now(),
                    signAlg,
                    certName,
                    urlPath: url,
                };
                return agent.get(url)
                    .query({
                        ...queryWithoutSignature,
                        signature: Sign({ prvKey: prvk, data: getOriginDataToBeSigned(queryWithoutSignature, "010"), alg: signAlg })
                            .toString("hex"),
                    }).then((res) => {
                        expect(res.status).toBe(200);
                        expect(res.body.fileACL).toBeDefined();
                        expect(res.body.fileACL.fileIDCombineUserID).toBe(`${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[0].userID}`);
                        return 0;
                    });               
            }); 
            it("Should get failed when querying fileACL with a non-existed fileID", (done) => {
                const url = `/files/non-existed/accessAuths/${testFileMetas[0]}${COMBINATION_DELIMITER}${testUsers[0]}`; 
                const queryWithoutSignature = {
                    requesterID,
                    timestamp: Date.now(),
                    signAlg,
                    certName,
                    urlPath: url,
                };
                return agent.get(url)
                    .query({
                        ...queryWithoutSignature,
                        signature: Sign({ prvKey: prvk, data: getOriginDataToBeSigned(queryWithoutSignature, "010"), alg: signAlg })
                            .toString("hex"),
                    }).end((err, res) => {
                        expect(res.status).toBe(404);
                        expect(res.body.message).toBe("No such file:non-existed");
                        done();
                    });               
            });
            it("Should get failed when querying fileACL with a non-existed accessAuthID", (done) => {
                const url = `/files/${testFileMetas[0].fileID}/accessAuths/non-existed`; 
                const queryWithoutSignature = {
                    requesterID,
                    timestamp: Date.now(),
                    signAlg,
                    certName,
                    urlPath: url,
                };
                return agent.get(url)
                    .query({
                        ...queryWithoutSignature,
                        signature: Sign({ prvKey: prvk, data: getOriginDataToBeSigned(queryWithoutSignature, "010"), alg: signAlg })
                            .toString("hex"),
                    }).end((err, res) => {
                        expect(res.status).toBe(404);
                        expect(res.body.message).toBe("Not found accessAuth");
                        done();
                    });               
            });
            it("Should get failed when querying fileACL with a non-authorized accessAuthID", (done) => {
                const url = `/files/${testFileMetas[0].fileID}/accessAuths/${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[0].userID}`; 
                const queryWithoutSignature = {
                    requesterID: testUsers[1].userID,
                    timestamp: Date.now(),
                    signAlg,
                    certName: testUsers[1].certificateName,
                    urlPath: url,
                };
                agent.get(url)
                    .query({
                        ...queryWithoutSignature,
                        signature: Sign({ prvKey: testUsers[1].prvKeyPEM, data: getOriginDataToBeSigned(queryWithoutSignature, "010"), alg: signAlg })
                            .toString("hex"),
                    }).end((err, res) => {
                        expect(res.status).toBe(401);
                        expect(res.body.message).toBe("You are unauthorized to make this request");
                        done();
                    });               
            });
        });
    });
});
