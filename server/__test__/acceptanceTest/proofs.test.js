import request from "supertest";
import { Sign } from "rclink/lib/crypto";
import _ from "lodash";
import { client as WebSocketClient } from "websocket";
import app, { server } from "../../src/server";
import getOriginDataToBeSigned from "../../src/validate/originDataTBS";
import { createSomeTestRecords, clearTestRecords } from "../helpers/prismaOps";
import uploadOneFile from "../helpers/uploadFile";
import { startBlockchainSync, stopBlockchainSync } from "../../src/blockchainSync";
import { ProofEnumStatus } from "../../src/validate/enumValues";

describe("proofs API test, query file operation proof(s)", () => {
    const testIdentifier = "proofs-api-test";
    const agent = request(app);
    const signAlg = "sha256";

    let testFileOperationProofs;
    let testFileMetas;
    let testUsers;

    beforeAll(() => createSomeTestRecords({
        userCount: 2,
        fileOwnedCountEachUser: 5,
        fileSharedCountEachUser: 2,
        identifier: testIdentifier,
    }).then((res) => {
        testUsers = res.users;
        testFileOperationProofs = res.fileOperationProofObjs;
        testFileMetas = res.fileMetas;

        startBlockchainSync();

        return 0;
    }));

    afterAll(() => clearTestRecords(testIdentifier).then(() => {
        server.close();
        stopBlockchainSync();
        return 0;
    }));

    it("Should get success when querying proof collection from primary level api", () => {
        const url = "/proofs";
        const queryWithoutSignature = {
            fileID: testIdentifier,
            sortBy: "operationType_ASC",
            perPage: 5,
            page: 1,
            requesterID: testUsers[0].userID,
            timestamp: Date.now(),
            signAlg,
            certName: testUsers[0].certificateName,
            urlPath: url,
        };
        return agent.get(url)
            .query({
                ...queryWithoutSignature,
                signature: Sign({
                    prvKey: testUsers[0].prvKeyPEM,
                    data: getOriginDataToBeSigned(queryWithoutSignature, "010"),
                    alg: signAlg,
                }).toString("hex"),
            }).then((res) => {
                expect(res.body.proofs).toBeDefined();
                expect(res.body.totalCount).toBe(14);
                expect(res.body.proofs.length).toBe(5);
                expect(JSON.stringify(res.body.proofs)).toBe(
                    JSON.stringify(_.sortBy(testFileOperationProofs,
                        value => value.operationType).slice(0, 5),
                    ),
                );
                return 0;
            });
    });

    it("Should get success when querying proof collection from secondary level api", () => {
        const url = `/files/${testFileMetas[0].fileID}/proofs`;
        const queryWithoutSignature = {
            sortBy: "operationType_ASC",
            perPage: 20,
            page: 1,
            requesterID: testUsers[0].userID,
            timestamp: Date.now(),
            signAlg,
            certName: testUsers[0].certificateName,
            urlPath: url,
        };
        return agent.get(url)
            .query({
                ...queryWithoutSignature,
                signature: Sign({
                    prvKey: testUsers[0].prvKeyPEM,
                    data: getOriginDataToBeSigned(queryWithoutSignature, "010"),
                    alg: signAlg,
                }).toString("hex"),
            }).then((res) => {
                expect(res.body.proofs).toBeDefined();
                expect(res.body.totalCount).toBe(2);
                expect(res.body.proofs.length).toBe(2); // 1 UPLOAD and 1 AUTH_UPDATE
                return 0;
            });
    });

    it("Should get success when querying one proof from primary level api", () => {
        const url = `/proofs/${testFileOperationProofs[0].txid}`;
        const queryWithoutSignature = {
            requesterID: testUsers[0].userID,
            timestamp: Date.now(),
            signAlg,
            certName: testUsers[0].certificateName,
            urlPath: url,
        };
        return agent.get(url)
            .query({
                ...queryWithoutSignature,
                signature: Sign({
                    prvKey: testUsers[0].prvKeyPEM,
                    data: getOriginDataToBeSigned(queryWithoutSignature, "010"),
                    alg: signAlg,
                }).toString("hex"),
            }).then((res) => {
                expect(res.body.proof).toBeDefined();
                expect(JSON.stringify(res.body.proof))
                    .toBe(JSON.stringify(testFileOperationProofs[0]));
                return 0;
            });
    });

    it("Should get success when querying one proof from secondary level api", () => {
        const url = `/files/${testFileMetas[0].fileID}/proofs/${testFileOperationProofs[0].txid}`;
        const queryWithoutSignature = {
            requesterID: testUsers[0].userID,
            timestamp: Date.now(),
            signAlg,
            certName: testUsers[0].certificateName,
            urlPath: url,
        };
        return agent.get(url)
            .query({
                ...queryWithoutSignature,
                signature: Sign({
                    prvKey: testUsers[0].prvKeyPEM,
                    data: getOriginDataToBeSigned(queryWithoutSignature, "010"),
                    alg: signAlg,
                }).toString("hex"),
            }).then((res) => {
                expect(res.body.proof).toBeDefined();
                expect(JSON.stringify(res.body.proof))
                    .toBe(JSON.stringify(testFileOperationProofs[0]));
                return 0;
            });
    });

    describe("realtime api test, get file operation proofs updated status", () => {
        it("should get the file operation proofs status updated through the websocket realtime api", (done) => {
            let fileIDUploaded;
            const webSocketUrl = "ws://localhost:5000/proofs";
            const webSocketClient = new WebSocketClient();
            webSocketClient.on("connect", (connection) => {
                connection.on("message", (message) => {
                    expect(message.type).toBe("utf8");
                    expect(message.utf8Data).toBeDefined();
                    const proof = JSON.parse(message.utf8Data);
                    expect(proof.file.fileID).toBe(fileIDUploaded);
                    const [, WAITING] = ProofEnumStatus;
                    expect(proof.status).not.toBe(WAITING);
                    connection.close();
                    done();
                });

                // upload a file to create a new operation proof to be confirmed into blockchain
                uploadOneFile({
                    agent,
                    filePath: __filename,
                    ownerID: testUsers[0].userID,
                    certName: testUsers[0].certificateName,
                    prvk: testUsers[0].prvKeyPEM,
                }).then((fileID) => {
                    fileIDUploaded = fileID;
                    return 0;
                });
            });
            webSocketClient.connect(webSocketUrl, "echo-protocol", "test");
        }, 50000);
    });

    // TODO: Bad params api tests
});
