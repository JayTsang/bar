import request from "supertest";
import path from "path";
import fs from "fs-extra";
import config from "config";
import { GetHashVal, Sign } from "rclink/lib/crypto";
import rimraf from "rimraf";
import app, { server } from "../../src/server";
import getOriginDataToBeSigned from "../../src/validate/originDataTBS";
import { createSomeTestUsers, clearTestRecords } from "../helpers/prismaOps";
import uploadOneFile from "../helpers/uploadFile";

describe("Files upload and download api test", () => {
    const hashAlg = "sha1";
    let requesterID;
    let prvk;
    const signAlg = "sha256";
    let certName;
    const agent = request(app);
    const baseUrl = "/files";
    let userUnauthorized;

    beforeAll(() => createSomeTestUsers(2).then((users) => {
        const user = users[0];
        requesterID = user.userID;
        certName = user.certificateName;
        prvk = user.prvKeyPEM;
        [, userUnauthorized] = users;
        return 0;
    }));

    afterAll(() => clearTestRecords().then(() => server.close()));

    describe("POST /files API test, upload file(s)", () => {
        // You should place the files in the testFilesDir directory before run this test suite
        const testFilesDir = config.get("File.testFilesDir");
        if (!fs.existsSync(testFilesDir)
        || fs.readdirSync(testFilesDir).length === 0) {
            throw Error("You should create 'testFiles' directory firstly, and put your files into it.");
        }

        const items = fs.readdirSync(testFilesDir);

        const testUploadsDir = config.get("File.fileUploadDir");
        rimraf.sync(testUploadsDir);

        const url = baseUrl;

        test("Should get success when uploading a single file", () => {
            let fileHashes = GetHashVal({ data: fs.readFileSync(path.join(testFilesDir, items[0])), alg: hashAlg }).toString("hex");
            fileHashes = JSON.stringify([fileHashes]);
            const timestamp = Date.now();
            const bodyWithoutSignature = {
                fileHashes, hashAlg, requesterID, timestamp, signAlg, certName, urlPath: url, 
            };
            return agent
                .post(url)
                .field(bodyWithoutSignature) 
                .field("signature", Sign({ prvKey: prvk, data: getOriginDataToBeSigned(bodyWithoutSignature, "010"), alg: signAlg })
                    .toString("hex"))
                .attach("files", path.join(testFilesDir, items[0]))
                .then((res) => {
                    expect(res.status).toBe(201);
                    expect(res.body.message).toBe("File(s) uploaded successfully");
                    expect(res.body.uploadedFileCount).toBe(1);
                    expect(res.body.fileMetas).toBeDefined();
                    expect(res.body.fileMetas.length).toBe(1);
                    return 0;
                });
        });

        test("Should get success when uploading multi files", () => {
            const timestamp = Date.now();
            let fileHashes = [];
            const bodyWithoutSignature = {
                hashAlg, requesterID, timestamp, signAlg, certName, urlPath: url,
            };
            const interVal = agent.post(url);
            items.forEach((item) => {
                fileHashes.push(GetHashVal({ data: fs.readFileSync(path.join(testFilesDir, item)), alg: hashAlg }).toString("hex"));
                interVal.attach("files", path.join(testFilesDir, item));
            });
            fileHashes = JSON.stringify(fileHashes);
            bodyWithoutSignature.fileHashes = fileHashes;
            return interVal
                .field(bodyWithoutSignature)
                .field("signature", Sign({ prvKey: prvk, data: getOriginDataToBeSigned(bodyWithoutSignature, "010"), alg: signAlg })
                    .toString("hex"))
                .then((res) => {
                    expect(res.status).toBe(201);
                    expect(res.body.message).toBe("File(s) uploaded successfully");
                    expect(res.body.uploadedFileCount).toBe(items.length);
                    expect(res.body.fileMetas).toBeDefined();
                    expect(res.body.fileMetas.length).toBe(items.length);
                    return 0;
                });
        });

        test("Should get failed when uploading files with too many fields", (done) => {
            let interVal = agent.post(url);
            for (let i = 0; i <= config.get("File.uploadLimits.fields"); i++) {
                interVal.field("hashAlg", hashAlg);
            }
            interVal.end((err, res) => {
                expect(res.status).toBe(400);
                expect(res.body.message).toBe("Bad request, Too many fields");
            });

            interVal = agent.post(url);
            for (let i = 0; i <= config.get("File.uploadLimits.files"); i++) {
                interVal.attach("files", path.join(testFilesDir, items[0]));
            }
            interVal.end((err, res) => {
                expect(res.status).toBe(400);
                expect(res.body.message).toBe("Bad request, Too many files");
                done();
            });
        });

        test("Should get failed when calling upload api with empty field params", (done) => {
            agent.post(url)
                .end((err, res) => {
                    expect(res.status).toBe(400);
                    expect(res.body.message).toBe("Bad request, with error(s)");
                    expect(res.body.errors.length).toBeGreaterThan(0);
                    expect(res.body.errors[0].msg).toBe("The files is required and should not be empty");
                    expect(res.body.errors[1].msg).toBe("The hashAlg is required and should not be empty");
                    expect(res.body.errors[2].msg).toBe("The fileHashes is required and should not be empty");
                    expect(res.body.errors[3].msg).toBe("The requesterID is required and should not be empty");
                    expect(res.body.errors[4].msg).toBe("The timestamp is required and should not be empty");
                    expect(res.body.errors[7].msg).toBe("The signAlg is required and should not be empty");
                    expect(res.body.errors[8].msg).toBe("The certName is required and should not be empty");
                    expect(res.body.errors[9].msg).toBe("The urlPath is required and should not be empty");
                    expect(res.body.errors[10].msg).toBe("The signature is required and should not be empty");
                    done();
                });
        });

        test("Should get failed when uploading file(s) with bad fields", async () => {
            await agent.post(url)
                .field("hashAlg", hashAlg)
                .field("fileHashes", "njkjkk2398sac") // bad hash
                .field("requesterID", requesterID) 
                .field("timestamp", "1278678sdsa") // bad timestamp
                .field("signAlg", signAlg)
                .field("certName", certName)
                .field("urlPath", url)
                .field("signature", "hsjdkhsiuasdui") // bad signature 
                .attach("files", path.join(testFilesDir, items[0]))
                .catch((err) => {
                    const { status, body } = err.response;
                    expect(status).toBe(400);
                    expect(body.message).toBe("Bad request, with error(s)");
                    expect(body.errors.length).toBeGreaterThan(0);
                    expect(body.errors[0].msg).toBe("The fileHashes \"njkjkk2398sac\" is a bad json string of Array[String]");
                    expect(body.errors[1].msg).toBe("The timestamp's length should be 13");
                    expect(body.errors[2].msg).toBe("The timestamp should be a unix timestamp milliseconds value");
                    expect(body.errors[3].msg).toBe("Bad signature or signAlg");
                });
            await agent.post(url)
                .field("hashAlg", hashAlg)
                .field("fileHashes", "[hdkjs928]") // bad hash
                .field("requesterID", requesterID)
                .field("timestamp", Date.now())
                .field("signAlg", signAlg)
                .field("certName", certName)
                .field("urlPath", url)
                .field("signature", "hsjdkhsiuasdui") // bad signature
                .attach("files", path.join(testFilesDir, items[0]))
                .catch((err) => {
                    const { status, body } = err.response;
                    expect(status).toBe(400);
                    expect(body.message).toBe("Bad request, with error(s)");
                    expect(body.errors.length).toBeGreaterThan(0);
                    expect(body.errors[0].msg).toBe("The fileHashes \"[hdkjs928]\" is a bad json string of Array[String]");
                    expect(body.errors[1].msg).toBe("Bad signature or signAlg");
                });
            await agent.post(url)
                .field("hashAlg", hashAlg)
                .field("fileHashes", "[\"hdkjs928\", \"sdfsfda\"]") // bad hash(too many)
                .field("requesterID", requesterID)
                .field("timestamp", Date.now())
                .field("signAlg", signAlg)
                .field("certName", certName)
                .field("urlPath", url)
                .field("signature", "hsjdkhsiuasdui") // bad signature
                .attach("files", path.join(testFilesDir, items[0]))
                .catch((err) => {
                    const { status, body } = err.response;
                    expect(status).toBe(400);
                    expect(body.message).toBe("Bad request, with error(s)");
                    expect(body.errors.length).toBeGreaterThan(0);
                    expect(body.errors[0].msg).toBe("The files and fileHashes contain different element counts");
                    expect(body.errors[1].msg).toBe("Bad signature or signAlg");
                });
            await agent.post(url)
                .field("hashAlg", hashAlg)
                .field("fileHashes", "[\"hdkjs928\", \"sdfsafds\"]") // bad hash
                .field("requesterID", requesterID)
                .field("timestamp", Date.now())
                .field("signAlg", signAlg)
                .field("certName", certName)
                .field("urlPath", url)
                .field("signature", "hsjdkhsiuasdui") // bad signature
                .attach("files", path.join(testFilesDir, items[0]))
                .attach("files", path.join(testFilesDir, items[1]))
                .catch((err) => {
                    const { status, body } = err.response;
                    expect(status).toBe(400);
                    expect(body.message).toBe("Bad request, with error(s)");
                    expect(body.errors.length).toBeGreaterThan(0);
                    expect(body.errors[0].msg).toBe(`The file hash "hdkjs928" is not a hash of algorithm ${hashAlg}`);
                    expect(body.errors[1].msg).toBe("Bad signature or signAlg");
                });

            const hash0 = GetHashVal({ data: fs.readFileSync(path.join(testFilesDir, items[0])), alg: hashAlg }).toString("hex"); 
            const hash1 = GetHashVal({ data: fs.readFileSync(path.join(testFilesDir, items[1])), alg: hashAlg }).toString("hex"); 
            const hashBad1 = hash1.replace(/0/, "1");
            const hashesBad = JSON.stringify([hash0, hashBad1]);
            await agent.post(url)
                .field("hashAlg", hashAlg)
                .field("fileHashes", hashesBad)// bad hash(replace character)
                .field("requesterID", requesterID)
                .field("timestamp", Date.now())
                .field("signAlg", signAlg)
                .field("certName", certName)
                .field("urlPath", url)
                .field("signature", "hsjdkhsiuasdui") // bad signature
                .attach("files", path.join(testFilesDir, items[0]))
                .attach("files", path.join(testFilesDir, items[1]))
                .catch((err) => {
                    const { status, body } = err.response;
                    expect(status).toBe(400);
                    expect(body.message).toBe("Bad request, with error(s)");
                    expect(body.errors.length).toBeGreaterThan(0);
                    expect(body.errors[0].msg).toBe(`The file hash "${hashBad1}" mismatched the file uploaded`);
                    expect(body.errors[1].msg).toBe("Bad signature or signAlg");
                });
 
            const hashes = JSON.stringify([hash0, hash1]);
            const timestamp = Date.now();
            await agent.post(url)
                .field("hashAlg", hashAlg)
                .field("fileHashes", hashes)
                .field("requesterID", requesterID)
                .field("timestamp", timestamp) 
                .field("signAlg", "md5") // bad signAlg
                .field("certName", certName)
                .field("urlPath", url)
                .field("signature", Sign({ 
                    prvKey: prvk,
                    data: getOriginDataToBeSigned({
                        hashAlg, hashes, requesterID, timestamp, signAlg, urlPath: url, certName,
                    }, "010"),
                    alg: signAlg, 
                }).toString("hex")) 
                .attach("files", path.join(testFilesDir, items[0]))
                .attach("files", path.join(testFilesDir, items[1]))
                .catch((err) => {
                    const { status, body } = err.response;
                    expect(status).toBe(400);
                    expect(body.message).toBe("Bad request, with error(s)");
                    expect(body.errors.length).toBeGreaterThan(0);
                    expect(body.errors[0].msg).toBe("Bad signature or signAlg");
                });
        });
    });

    describe("GET /file API test, download file", () => {
        const testDownloadsDir = config.get("File.testDownloads");
        let url;

        fs.ensureDirSync(testDownloadsDir);

        beforeAll(async () => {
            const testFilesDir = config.get("File.testFilesDir");
            const items = fs.readdirSync(testFilesDir);

            return uploadOneFile({
                prvk,
                agent, 
                ownerID: requesterID,
                certName,
                filePath: path.join(testFilesDir, items[0]),
            }).then((fileID) => {
                url = `${baseUrl}/${fileID}/download`;
                return 0;
            });
        });

        test("Should get success when downloading a existed file", (done) => {
            const timestamp0 = Date.now();
            const timestamp1 = Date.now();
            const fileWStream = fs.createWriteStream(path.join(testDownloadsDir, "testDownload"));
            fileWStream.on("close", () => {
                const queryWithoutSignature = {
                    requesterID, timestamp: timestamp1, signAlg, certName, urlPath: url,
                };
                agent.get(url)
                    .query({
                        ...queryWithoutSignature,
                        signature: Sign({ prvKey: prvk, data: getOriginDataToBeSigned(queryWithoutSignature, "010"), alg: signAlg })
                            .toString("hex"),
                    })
                    .then((res) => {
                        expect(res.status).toBe(200);
                        done();
                        return 0;
                    });
            });

            const queryWithoutSignature = {
                requesterID, timestamp: timestamp0, signAlg, certName, urlPath: url,
            };

            agent.get(url)
                .query({
                    ...queryWithoutSignature,
                    signature: Sign({ prvKey: prvk, data: getOriginDataToBeSigned(queryWithoutSignature, "010"), alg: signAlg })
                        .toString("hex"),
                })
                .pipe(fileWStream);
        });

        // TODO: failed test for file download
        test("Should get failed when downloading a non-existing file", () => {
            const fileID = "file-not-exist";
            const timestamp = Date.now();
            const urlBad = `${baseUrl}/${fileID}/download`;
            const queryWithoutSignature = {
                requesterID, timestamp, signAlg, certName, urlPath: urlBad,
            };
            return agent.get(urlBad)
                .query({
                    ...queryWithoutSignature,
                    signature: Sign({ prvKey: prvk, data: getOriginDataToBeSigned(queryWithoutSignature, "010"), alg: signAlg })
                        .toString("hex"),
                })
                .catch((err) => {
                    const { status, body } = err.response;
                    expect(status).toBe(404);
                    expect(body.message).toBe(`No such file:${fileID}`);
                });
        });
        test("Should get failed when downloading a file with an unauthorized requesterID", () => {
            const timestamp = Date.now();
            const queryWithoutSignature = {
                requesterID: userUnauthorized.userID, 
                timestamp, 
                signAlg, 
                certName: userUnauthorized.certificateName, 
                urlPath: url, 
            };
            return agent.get(url)
                .query({
                    ...queryWithoutSignature,
                    signature: Sign({ 
                        prvKey: userUnauthorized.prvKeyPEM, 
                        data: getOriginDataToBeSigned(queryWithoutSignature, "010"), 
                        alg: signAlg, 
                    }).toString("hex"),
                })
                .catch((err) => {
                    const { status, body } = err.response;
                    expect(status).toBe(401);
                    expect(body.message).toBe("You are unauthorized to make this request");
                });
        });
    });   
});
