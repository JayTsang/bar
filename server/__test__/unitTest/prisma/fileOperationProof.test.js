import _ from "lodash";
import {
    getOneProof, getProofCollection, storeProof, updateProofStatus,
} from "../../../src/proofStore/proofMaintenance";
import { createSomeTestRecords, clearTestRecords } from "../../helpers/prismaOps";

describe("fileOperationProof functions test", () => {
    const identifier = "fileOperationProof-functions-test";
    let testFileOperationProofs;
    let testFileMetas;
    let testUsers;
    beforeAll(() => createSomeTestRecords({
        userCount: 2,
        fileOwnedCountEachUser: 5,
        fileSharedCountEachUser: 2,
        identifier,
    }).then((res) => {
        testFileOperationProofs = res.fileOperationProofObjs;
        testFileMetas = res.fileMetas;
        testUsers = res.users;
        return 0;
    }));
    afterAll(() => clearTestRecords(identifier));

    describe("getOneProof function test", () => {
        it("Should get one proof record when query with a existing proofID", async () => {
            const proofID = testFileOperationProofs[0].txid;
            const proof = await getOneProof(proofID);
            expect(proof).toBeDefined();
            expect(proof.txid).toBe(proofID);
        });

        it("Should get null proof record when query with a non-existing proofID", async () => {
            const proofID = "non-existing";
            const proof = await getOneProof(proofID);
            expect(proof).toBe(null);
        });
        // TODO: test error params
    });

    describe("getProofCollection function test", () => {
        it("Should get proof collection with 10 elements sorted by txTimestamp_ASC when querying with default params",
            async () => {
                const { proofs, totalCount } = await getProofCollection();
                expect(proofs).toBeDefined();
                expect(totalCount).toBe(14);
                expect(proofs.length).toBe(10);
                expect(JSON.stringify(_.sortBy(testFileOperationProofs, proof => proof.txTimestamp)
                    .slice(0, 10)),
                ).toBe(JSON.stringify(proofs));
            });
        it("Should get proof collection with elements when querying with the specific params",
            async () => {
                const params = {
                    filter: {
                        fileID: { fileID: "0", matchMode: "CONTAINS" },
                        operationType: "UPLOAD",
                        operatorID: testUsers[0].userID,
                    },
                };
                const { proofs, totalCount } = await getProofCollection(params);
                expect(proofs).toBeDefined();
                expect(totalCount).toBe(5);
                expect(proofs.length).toBe(5);
            });
    // TODO: test filter/sortBy/pagination params
    // TODO: test error params
    });

    describe("storeProof function test", () => {
        it("Should get a new created proof with good params", async () => {
            const proofParams = {
                fileID: testFileMetas[0].fileID, 
                operationType: "DOWNLOAD", 
                operationParams: {}, 
                operatorID: testUsers[0].userID, 
                txid: "test-txid-0", 
                txTimestamp: Date.now(), 
                status: "WAITING",
            };
            const proof = await storeProof(proofParams);
            expect(proof).toBeDefined();
            expect(proof.file.fileID).toBe(proofParams.fileID);
            expect(proof.operationType).toBe(proofParams.operationType);
            expect(proof.operatorID).toBe(proofParams.operatorID);
            expect(proof.txid).toBe(proofParams.txid);
            expect(proof.txTimestamp).toBe(new Date(proofParams.txTimestamp).toISOString());
            expect(proof.status).toBe(proofParams.status);
        });
        // TODO: test error params
    });

    describe("updateProofStatus function test", () => {
        it("Should get a updated proof with good params", async () => {
            const proofParams = {
                txid: testFileOperationProofs[0].txid, 
                status: "CONFIRMED",
                blockHash: "mock-block-hash0",
                blockTimestamp: Date.now(),
            };
            const proof = await updateProofStatus(proofParams);
            expect(proof).toBeDefined();
            expect(proof.file.fileID).toBe(testFileOperationProofs[0].file.fileID);
            expect(proof.operationType).toBe(testFileOperationProofs[0].operationType);
            expect(proof.operatorID).toBe(testFileOperationProofs[0].operatorID);
            expect(proof.txid).toBe(proofParams.txid);
            expect(proof.txTimestamp).toBe(testFileOperationProofs[0].txTimestamp);
            expect(proof.status).toBe(proofParams.status);
            expect(proof.blockHash).toBe(proofParams.blockHash);
            expect(proof.blockTimestamp).toBe(new Date(proofParams.blockTimestamp).toISOString());
        });
        // TODO: test error params
    });
});
