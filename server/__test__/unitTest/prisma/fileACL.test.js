import _ from "lodash";
import {
    getFileACLCollection, getOneFileACL, createFileACL, updateFileACL,
} from "../../../src/accessControl/authMaintenance";
import { createSomeTestRecords, clearTestRecords } from "../../helpers/prismaOps";
import { COMBINATION_DELIMITER } from "../../../src/const";

describe("fileACL functions test", () => {
    const identifier = "fileACL-functions-test";
    let testFileACLs;
    let testFileMetas;
    let testUsers;
    beforeAll(() => createSomeTestRecords({
        userCount: 2,
        fileOwnedCountEachUser: 5,
        fileSharedCountEachUser: 2,
        identifier,
    }).then((res) => {
        testFileACLs = res.fileACLs;
        testFileMetas = res.fileMetas;
        testUsers = res.users;
        return 0;
    }));
    afterAll(() => clearTestRecords(identifier));

    describe("getFileACLCollection function test", () => {
        test("Should get fileACL collection with 10 elements sorted by userID_ASC when querying with default params", 
            async () => {
                const { fileACLs, totalCount } = await getFileACLCollection();
                expect(fileACLs).toBeDefined();
                expect(totalCount).toBe(14);
                expect(fileACLs.length).toBe(10);
                expect(JSON.stringify(_.sortBy(testFileACLs, fileACL => fileACL.userID)
                    .slice(0, 10))).toBe(JSON.stringify(fileACLs));
            });

        test("Should get fileACL collection with the specific fileID when querying with filter param",
            async () => {
                const { fileACLs, totalCount } = await getFileACLCollection({ 
                    filter: { fileID: testFileMetas[0].fileID }, 
                });
                expect(fileACLs).toBeDefined();
                expect(totalCount).toBe(2);
                expect(fileACLs.length).toBe(2);
                expect(fileACLs[0].fileIDCombineUserID).toBe(`${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[0].userID}`);
                expect(fileACLs[1].fileIDCombineUserID).toBe(`${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[1].userID}`);
            });
    });
    describe("getOneFileACL function test", () => {
        test("Should get one fileACL with the specific fileIDCombineUserID", async () => {
            const fileACL = await getOneFileACL(`${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[0].userID}`);
            expect(fileACL).toBeDefined();
            expect(fileACL.fileIDCombineUserID).toBe(`${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[0].userID}`);
        });
    });
    describe("createFileACL function test", () => {
        test("Should create one new fileACL with the new fileID and objectUserID", async () => {
            const fileACL = await createFileACL({
                fileID: testFileMetas[0].fileID,
                objectUserID: "user-new",
                accessAuthority: 8,
                accessRestrictionType: "COUNT",
                accessRestrictionContent: { initialCount: 3, remainCount: 1 },
                accessPostProofStorage: 9,
            });
            expect(fileACL).toBeDefined();
            expect(fileACL.fileIDCombineUserID).toBe(`${testFileMetas[0].fileID}${COMBINATION_DELIMITER}user-new`);
            expect(fileACL.restrictionType).toBe("COUNT");
            expect(fileACL.restrictionContent).toEqual({ initialCount: 3, remainCount: 1 });
        });
        test("Should get error with the existed fileID and objectUserID", (done) => {
            createFileACL({
                fileID: testFileMetas[0].fileID,
                objectUserID: testUsers[0],
                accessAuthority: 8,
                accessRestrictionType: "COUNT",
                accessRestrictionContent: { initialCount: 3, remainCount: 1 },
                accessPostProofStorage: 9,
            }).catch((err) => {
                expect(err).toBeDefined();
                done();
            }); 
        });
    });
    describe("updateFileACL function test", () => {
        test("Should update one existed fileACL with the existed fileID and objectUserID", async () => {
            const fileACL = await updateFileACL({
                accessAuthID: `${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[0].userID}`,
                accessRestrictionType: "COUNT",
                accessRestrictionContent: { initialCount: 4, remainCount: 4 },
            });
            expect(fileACL).toBeDefined();
            expect(fileACL.fileIDCombineUserID).toBe(`${testFileMetas[0].fileID}${COMBINATION_DELIMITER}${testUsers[0].userID}`);
            expect(fileACL.restrictionType).toBe("COUNT");
            expect(fileACL.restrictionContent).toEqual({ initialCount: 4, remainCount: 4 });
        });
        test("Should get error with the non-existed fileID and objectUserID", (done) => {
            updateFileACL({
                accessAuthID: "non-existed",
                accessAuthority: 8,
                accessRestrictionType: "COUNT",
                accessRestrictionContent: { initialCount: 3, remainCount: 1 },
                accessPostProofStorage: 9,
            }).catch((err) => {
                expect(err).toBeDefined();
                done();
            }); 
        });
    });
});
