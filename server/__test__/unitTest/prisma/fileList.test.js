import _ from "lodash";
import getFileList from "../../../src/fileList/fileList";
import { createSomeTestRecords, clearTestRecords } from "../../helpers/prismaOps";

describe("Function getFileList test", () => {
    const userCount = 2;
    const fileOwnedCountEachUser = 5;
    const fileSharedCountEachUser = 2;
    const testRecordIdentifier = "getFileList-function-test";

    let testUsers;

    beforeAll(async () => createSomeTestRecords({
        userCount,
        fileOwnedCountEachUser,
        fileSharedCountEachUser,
        identifier: testRecordIdentifier,
    }).then((res) => {
        testUsers = res.users;
        return 0;
    }));
    
    afterAll(async () => clearTestRecords(testRecordIdentifier));

    test("Should get a list of fileMeta info when call getFileList function", async () => {
        const fileListWithSpecificFileID = (await getFileList({
            filter: { fileID: testRecordIdentifier },
        })).fileList;
        expect(fileListWithSpecificFileID.length).toBe(fileOwnedCountEachUser * userCount);
        const fileListWithSpecificFileName = (await getFileList({
            filter: { fileID: testRecordIdentifier, name: "lm" },
        })).fileList;
        expect(fileListWithSpecificFileName.length).toBeLessThanOrEqual(
            fileOwnedCountEachUser * userCount,
        );
        const fileListWithSpecificHash = (await getFileList({
            filter: { fileID: testRecordIdentifier, hash: "5" },
        })).fileList;
        expect(fileListWithSpecificHash.length).toBeLessThanOrEqual(
            fileOwnedCountEachUser * userCount,
        );
        const fileListWithSpecificUploadTime = (await getFileList({
            filter: { fileID: testRecordIdentifier, uploadTime: { to: Date.now() } },
        })).fileList;
        expect(fileListWithSpecificUploadTime.length).toBeLessThanOrEqual(
            fileOwnedCountEachUser * userCount,
        );
        const fileListWithSpecificSize = (await getFileList({
            filter: { fileID: testRecordIdentifier, size: { min: 1000 } },
        })).fileList;
        expect(fileListWithSpecificSize.length).toBeLessThanOrEqual(
            fileOwnedCountEachUser * userCount,
        );
        const fileListWithSpecificFileIDAndSortedByFileSize = (await getFileList({
            filter: { fileID: testRecordIdentifier },
            orderBy: "size_DESC",
        })).fileList;
        expect(JSON.stringify(_.sortBy(fileListWithSpecificFileID, file => file.size)
            .reverse())).toBe(JSON.stringify(fileListWithSpecificFileIDAndSortedByFileSize));
        const fileListWithSpecificFileIDAndPagination = (await getFileList({
            filter: { fileID: testRecordIdentifier },
            pagination: { page: 2, perPage: 3 },
        })).fileList;
        expect(JSON.stringify(fileListWithSpecificFileID.slice((2 - 1) * 3, (2 - 1) * 3 + 3)))
            .toBe(JSON.stringify(fileListWithSpecificFileIDAndPagination));
    });

    test("Should get a list of file when call getFileList function with access authority query criteria", async () => {
        const fileListWithSpecificOwner = (await getFileList({
            filter: {
                fileID: testRecordIdentifier,
                accessAuth: {
                    shareStatus: "OWNED",
                    userID: testUsers[0].userID,
                },
            },
        })).fileList;
        expect(fileListWithSpecificOwner.length).toBe(fileOwnedCountEachUser);

        const fileListWithSpecificOwnerAndSharedToOthers = (await getFileList({
            filter: {
                fileID: testRecordIdentifier,
                accessAuth: {
                    shareStatus: "OWNED_AND_SHARED_TO_OTHERS",
                    userID: testUsers[0].userID,
                },
            },
        })).fileList;
        expect(fileListWithSpecificOwnerAndSharedToOthers.length).toBe(fileSharedCountEachUser);

        const fileListWithSpecificOwnerAndNotSharedToOthers = (await getFileList({
            filter: {
                fileID: testRecordIdentifier,
                accessAuth: {
                    shareStatus: "OWNED_AND_NOT_SHARED_TO_OTHERS",
                    userID: testUsers[0].userID,
                },
            },
        })).fileList;
        expect(fileListWithSpecificOwnerAndNotSharedToOthers.length).toBe(
            fileOwnedCountEachUser - fileSharedCountEachUser,
        );

        const fileListWithSpecificUserIDAndSharedFromOthers = (await getFileList({
            filter: {
                fileID: testRecordIdentifier,
                accessAuth: {
                    shareStatus: "SHARED_FROM_OTHERS",
                    userID: testUsers[0].userID,
                },
            },
        })).fileList;
        expect(fileListWithSpecificUserIDAndSharedFromOthers.length).toBe(fileSharedCountEachUser);
    });
});
