import { GetHashVal, Sign } from "rclink/lib/crypto";
import fs from "fs-extra";
import getOriginDataToBeSigned from "../../src/validate/originDataTBS";

const uploadOneFile = ({
    agent, filePath, ownerID, certName, prvk, 
}) => {
    const hashAlg = "sha1";
    const signAlg = "sha256";
    const url = "/files";
    let fileHashes = GetHashVal({ data: fs.readFileSync(filePath), alg: hashAlg }).toString("hex");
    fileHashes = JSON.stringify([fileHashes]);
    const timestamp = Date.now();

    const bodyWithoutSignature = {
        fileHashes,
        hashAlg,
        requesterID: ownerID,
        timestamp, 
        signAlg,
        certName,
        urlPath: url,
    };
        
    return agent.post(url)
        .field(bodyWithoutSignature)
        .field("signature", Sign({ prvKey: prvk, data: getOriginDataToBeSigned(bodyWithoutSignature, "010"), alg: signAlg })
            .toString("hex"))
        .attach("files", filePath)
        .then((res) => {
            const { fileID } = res.body.fileMetas[0];
            return fileID;
        });
};

export default uploadOneFile;
