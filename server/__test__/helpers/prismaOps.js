// eslint-disable-next-line import/no-extraneous-dependencies
import faker from "faker";
import { Crypto } from "rclink";
import { prisma } from "../../src/db/generated/prisma-client";
import { proofWithLinkTypeIDFragment } from "../../src/proofStore/proofMaintenance";
import { prismaFragmentFileACLWithFileID } from "../../src/accessControl/authMaintenance";
import { COMBINATION_DELIMITER } from "../../src/const";

const createRestrictionObj = () => faker.random.arrayElement([
    {
        restrictionType: "TIME_RANGE",
        restrictionContent: { from: faker.date.past(), to: faker.date.future() },
    },
    {
        restrictionType: "COUNT",
        restrictionContent: {
            initialCount: faker.random.number({ min: 5, max: 8 }),
            remainCount: faker.random.number({ min: 0, max: 5 }),
        },
    },
]);

export const createSomeTestUsers = async (userCount, identifier = "test") => {
    if (userCount < 1) {
        throw Error("Bad params, userCount should not be less than 1");
    }
    const accounts = [];
    const certs = [];
    const users = [];
    for (let i = 0; i < userCount; i++) {
        accounts.push({
            name: `${identifier}-account-${i}`,
            creditCode: `${identifier}-creditCode-${i}`,
            phone: faker.phone.phoneNumber("132########"), 
        });

        const keypair = Crypto.CreateKeypair();
        const certPEM = Crypto.CreateSelfSignedCertificate({
            serialNumber: faker.random.number({ min: 100000000, max: 999999999 }), 
            sigAlg: "SHA256withECDSA", 
            DN: `/CN=${identifier}-${i}`, 
            notBefore: Date.now(),
            notAfter: Date.now() / 1000 + 365 * 24 * 3600,
            keypair, 
        });
        certs.push({
            creditCode: accounts[i].creditCode,
            name: `${identifier}-cert-${i}`,
            creditCodeCombineName: `${accounts[i].creditCode}${COMBINATION_DELIMITER}${identifier}-cert-${i}`,
            certPEM,
            status: true,
            account: {
                connect: { creditCode: accounts[i].creditCode },
            },
        });

        users.push({
            userID: accounts[i].creditCode,
            certificateName: certs[i].name,
            prvKeyPEM: Crypto.GetKeyPEM(keypair.prvKeyObj), 
        });
    }

    await Promise.all(accounts.map(
        account => prisma.createAccount(account)));
    await Promise.all(certs.map(cert => prisma.createCert(cert)));

    return users;
};

export const clearTestUsers = async (identifier) => {
    await prisma.deleteManyCerts({
        creditCodeCombineName_starts_with: `${identifier}`,
    });
    await prisma.deleteManyAccounts({
        creditCode_starts_with: `${identifier}`,
    });
};

export const createSomeTestRecords = async ({
    userCount = 2, fileOwnedCountEachUser = 5, fileSharedCountEachUser = 2, identifier = "test",
}) => {
    if (userCount < 2) {
        throw Error("Bad params, userCount must be greater than or equal to 2");
    }
    if (fileSharedCountEachUser > fileOwnedCountEachUser) {
        throw Error("Bad params, fileSharedCountEachUser must be less than or equal to fileOwnedCountEachUser");
    }
    const fileMetaObjs = [];
    const fileACLObjs = [];
    const fileOpProofObjs = [];

    const users = await createSomeTestUsers(userCount, identifier);
    
    for (let i = 0; i < userCount; i++) {
        for (let j = 0; j < fileOwnedCountEachUser; j++) {
            const fileID = `${identifier}-file-${i}+${j}`;
            const { userID } = users[i];
            const hashAlg = faker.random.arrayElement(["sha1", "sha256", "MD5"]);
            const fileMetaObj = {
                fileID,
                hash: Crypto.GetHashVal({ alg: hashAlg, data: faker.random.uuid() }).toString("hex"),
                hashAlg,
                name: faker.system.commonFileName(),
                size: faker.random.number({ min: 1 }),
                uploadTime: faker.date.past(),
                storageTime: faker.date.future(),
                storagePath: "/test",
                fileACLs: {
                    create: [{
                        fileIDCombineUserID: `${fileID}${COMBINATION_DELIMITER}${userID}`,
                        userID,
                        authority: 15,
                        restrictionType: "UNRESTRICTED",
                        restrictionContent: {},
                    }],
                },
            };
            fileMetaObjs.push(fileMetaObj);
            const fileOpProofObj = {
                file: {
                    connect: { fileID },
                },
                operationType: "UPLOAD",
                operationParams: {},
                operatorID: userID,
                txid: faker.random.uuid(),
                txTimestamp: new Date(),
                blockHash: Crypto.GetHashVal({ data: faker.random.uuid() }).toString("hex"),
                blockTimestamp: faker.date.future(),
                status: "CONFIRMED",
            };
            fileOpProofObjs.push(fileOpProofObj);
        }
        for (let j = 0; j < fileSharedCountEachUser; j++) {
            const fileID = `${identifier}-file-${i}+${j}`;
            const userID = i === userCount - 1 
                ? users[0].userID 
                : users[i + 1].userID;
            const fileACLObj = {
                file: {
                    connect: {
                        fileID,
                    },
                },
                userID,
                fileIDCombineUserID: `${fileID}${COMBINATION_DELIMITER}${userID}`,
                authority: 8,
                ...(createRestrictionObj()),
            };
            fileACLObjs.push(fileACLObj);
            const fileOpProofObj = {
                file: {
                    connect: { fileID },
                },
                operationType: "AUTH_UPDATE",
                operationParams: {
                    authUpdateType: "CREATE",
                    authority: 8,
                    objectUserID: userID,
                    restrictionType: fileACLObj.restrictionType,
                    restrictionContent: fileACLObj.restrictionContent,
                },
                operatorID: userID,
                txid: faker.random.uuid(),
                txTimestamp: new Date(),
                blockHash: Crypto.GetHashVal({ data: faker.random.uuid() }).toString("hex"),
                blockTimestamp: faker.date.future(),
                status: "CONFIRMED",
            };
            fileOpProofObjs.push(fileOpProofObj);
        }
    }
    const result = {};
    result.fileMetas = await Promise.all(fileMetaObjs.map(value => prisma.createFileMeta(value)));
    await Promise.all(fileACLObjs.map(value => prisma.createFileACL(value)));
    result.fileACLs = await prisma.fileACLs({ where: { userID_starts_with: `${identifier}` }, orderBy: "userID_ASC" })
        .$fragment(prismaFragmentFileACLWithFileID);
    result.fileOperationProofObjs = await Promise.all(
        fileOpProofObjs.map(value => prisma.createFileOperationProof(value)
            .$fragment(proofWithLinkTypeIDFragment)),
    );
    result.users = users;
    return result;
};

export const clearTestRecords = async (identifier = "test", clearSpecificFileACLIdentifier) => {
    if (clearSpecificFileACLIdentifier) {
        await prisma.deleteManyFileACLs({
            userID_starts_with: clearSpecificFileACLIdentifier,
        });
    } else {
        await prisma.deleteManyFileOperationProofs({
            operatorID_starts_with: identifier,
        });
        await prisma.deleteManyFileACLs({
            file: {
                fileOpProofs_every: {
                    id_in: [],
                },
            },
        });
        await prisma.deleteManyFileMetas({
            fileACLs_every: {
                id_in: [],
            },
        });

        await clearTestUsers(identifier);
    }
};
