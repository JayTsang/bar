# /bin/bash

cd $( dirname ${BASH_SOURCE[0]} )

cd ../../src/db/prisma

docker-compose down

docker volume rm prisma_mysql_sbr 

docker-compose up -d

echo "waiting..."
sleep 20s

prisma deploy