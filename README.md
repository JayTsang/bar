## 项目介绍
### 简介
本项目提供了一个基于区块链技术的可举证云存储应用实现方案。该方案利用区块链防篡改、可验证及可追溯的特性，使用户和服务提供方皆可对系统中发生的存储行为进行举证，解决了现有公有云存储和点对点存储方案中的难举证问题。本项目同时也为区块链应用的链下文件存储提供了可靠解决方案，能为用户提供文件存储及分享服务，并将所有存储及分享行为在区块链中进行防篡改与防抵赖存证。 

本项目所提出解决方案与传统云存储方案的对比如下图所示：
![](pics/sbr-vs-existing.png)

### 系统架构
本项目的系统架构如下图所示：
![](pics/sbr-construct.png)

从上至下分为了应用层、接口层、服务功能组件层以及数据层。
- 应用层：面向用户提供密钥对/公钥证书管理、文件操作以及文件操作存证结果查询等功能;
- 接口层：以Restful API形式提供文件上传、文件分享、文件下载等API接口;
- 服务功能组件层：文件存储以及文件操作存证等服务功能的核心逻辑;
- 数据层： 提供文件数据以及区块链数据的维护存储。
  + 文件对象数据的具体存储可选择基于Ceph的自建云存储服务以及已有第三方云存储服务。
  + 区块链数据的维护与存储使用由RepChain组建的联盟链
  + 应用服务端的本地数据维护与存储使用Prisma

### 技术组件
本项目所使用的主要开源技术组件包括:
- [RepChain](https://gitee.com/BTAJL/repchain)：轻量易用的区块链基础组件，可方便地建立许可链/联盟链网络环境;
- BAR([Base App of RepChain](https://gitee.com/linkel/bar)): RepChain基础应用，提供了区块链应用所需的基础功能，包括用户密钥对管理、公钥证书管理、交易数据同步与检索、区块数据同步与检索以及交易数据构建、交易提交等;
- [react-admin](https://github.com/marmelab/react-admin)：基于ES6、React以及Material Design实现的前端框架；
- [Express](https://expressjs.com/)： NodeJS Web应用框架;
- [Prisma](https://www.prisma.io/)：基于[GraphQL](https://graphql.org/)的数据库访问管理中间件，可替代传统ORMs，并使得底层数据库对开发者透明;
- [Docker](https://www.docker.com/)：本项目使用了Docker来管理运行[Prisma Server](https://hub.docker.com/r/prismagraphql/prisma)以及应用服务端本地数据库(默认为[MySQL](https://hub.docker.com/_/mysql))

## 应用运行
### 环境准备
1. 安装node，推荐使用[nvm](https://github.com/nvm-sh/nvm)安装管理node;
2. 安装yarn，可参考[这里](https://github.com/yarnpkg/yarn);
3. 安装docker(可参考[这里](https://docs.docker.com/install/))以及docker-compose(可参考[这里](https://docs.docker.com/compose/install/))
4. 安装prisma cli工具, 可参考[这里](https://www.prisma.io/docs/prisma-cli-and-configuration/using-the-prisma-cli-alx4/#installation)
5. 搭建RepChain网络，参考[这里](https://gitee.com/BTAJL/repchain)

### 运行
#### 安装依赖包并启动docker容器
使用git工具将本项目clone到本地之后，进入项目根目录, 运行以下命令:
- `yarn install-all` 安装依赖(同时安装前端以及后端各自的依赖包)
- `yarn createPrismaAndDB` 为后端创建运行prisma server以及database的docker容器

#### 后端配置
进入目录: server/config, 可编辑json格式配置文件：
- default.json: 正式运行环境下的配置内容
- test.json: 开发测试环境下的配置内容

主要配置项包括：
- File.fileUploadDir： 文件上传时的暂存目录
- File.fileStorageDir: 文件被正式存储的目录
- File.uploadLimits.fields: 上传文件时multipart/form-data的最大项目数
- File.uploadLimits.fileSize: 上传文件时每个文件的最大限制(字节数)
- File.uploadLimits.files: 单次上传文件时允许的最多文件数

- Log.api: 后端对Restful api调用情况的日志记录管理
- Log.serverInner: 后端对功能逻辑运行情况的日志记录管理

- ServerCrypto.prvKeyFilePath: 后端使用的私钥文件的存储路径，该私钥将被用于区块链交易的构建
- ServerCrypto.prvKeyPassword: 后端所使用私钥的解密密码(若该私钥已被加密，需要提供该密码以解密使用)
- ServerCrypto.pubKeyFilePath: 后端所使用的公钥证书文件的存储路径，该公钥将被用于区块链交易的构建
- ServerCrypto.alg: 后端构建签名交易所使用的算法
- ServerCrypto.creditCode: 后端所使用的RepChain账户信用代码
- ServerCrypto.certName: 后端所使用公钥证书的名称

- RepChain.default.url_subscribe: RepChain节点的Websocket API地址，用于同步RepChain区块链数据时的实时推送同步
- RepChain.default.url_api: RepChain节点的Restful API地址，用于同步RepChain区块链数据时的主动获取同步
- RepChain.server: RepChain代理节点地址,用于文件操作存证
- RepChain.port: RepChain代理节点的服务端口
- RepChain.chaincodeName: 文件操作存证合约名称
- RepChain.chaincodeVersion: 文件操作存证合约版本号

- Synchronize.enable: RepChain区块链数据同步开启选项，默认为true
- Synchronize.PullSyncType: 主动拉取同步方式的类型，可为"PERIODICALLY"(周期性拉取)或"ONCE"(只拉取一次)
- Synchronize.PullSyncInterval: 周期性主动拉取时的时间间隔
- Synchronize.PushSyncInterval: 实时被动推送同步开启的时间间隔
> Note: 上述配置中涉及文件路径的配置项，各文件路径需有相应的读写权限；若使用了相对路径，则是相对本项目的server目录

#### 前端配置
进入目录: src, 可编辑配置文件settings.js。
主要配置项目包括：
- RepChain.default.url_api: RepChain代理节点的Restful API地址，用于前端提交区块链交易
- Prisma.endpoint: prisma server http服务地址
- Prisma.url_subscribe: prisma server websocket服务地址
- Server.wsBaseUrl: 服务端websocket服务地址

#### 启动应用
1. 确认RepChain已正常运行;
2. 确认prisma server和database的docker容器已正常运行
3. 确认上述配置已完成；
4. 启动应用：
- `yarn start` 启动应用(同时启动前端和后端服务) 

## 自动化测试
后端测试：
- 在项目根目录下，进入server目录后运行：
`yarn test`

前端测试：

主要进行了e2e测试：
- 在项目根目录下，运行：
`yarn test-e2e`

### 测试技术
- 本项目采用Jest+Supertest作为nodejs端的自动化测试工具;
- 采用Jest+Puppeteer作为浏览器端的e2e自动化测试工具
