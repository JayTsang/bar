import puppeteer from "puppeteer-core";
import fs from "fs";
import path from "path";

const chromiumPath = "/usr/bin/chromium";

describe("puppeteer test", () => {
    let browser;
    let page;
    beforeAll(async () => {
        browser = await puppeteer.launch({
            executablePath: chromiumPath,
            headless: true,
        });
        page = await browser.newPage();
        return page.goto("https://example.com/");
    });

    afterAll(() => browser.close());

    it("should be titled 'Example' ", () => expect(page.title())
        .resolves.toMatch("Example"));

    it("should screenshot to a png", async () => {
        const filePath = path.join(__dirname, "example.png");
        await page.screenshot({ path: filePath }).catch((e) => {
            console.log(e);
        });
        expect(fs.existsSync(filePath)).toBeTruthy();
    });

    // puppeteer-core v1.17.0 must work with chromium v76.0
    it("should could print to a pdf", async () => {
        const filePath = path.join(__dirname, "example.pdf");
        await page.pdf({ path: filePath, format: "A4" });
        expect(fs.existsSync(filePath)).toBeTruthy();
    });
});
