/* eslint-disable no-await-in-loop */
import { setBrowserAndPages } from "../helpers/setSharedVariables";
import {
    newAccountCount,
    account1Name,
    account2Name,
    accountListUrl,
} from "../helpers/const";

const accountRegisterTest = () => describe("Accounts register test", () => {
    let deskPage;

    beforeAll(async () => {
        const browserAndPages = await setBrowserAndPages();
        deskPage = browserAndPages.page1;
    });

    const newAccountNames = [account1Name, account2Name];
    const testRegisterNewLocalAccount = (testTitle, accountID) => {
        it(testTitle, async () => {
            // register local account
            await deskPage.goto(accountListUrl);
            await deskPage.waitForSelector("a[aria-label='编辑']");
            const editButtons = await deskPage.$$("a[aria-label='编辑'");
            const accountEditButton = editButtons[newAccountCount - accountID];
            await accountEditButton.click();
            await expect(deskPage.$eval("#name", el => el.value)).resolves.toBe(newAccountNames[accountID - 1]);
            const accountCreditCode = await deskPage.$eval("#creditCode", el => el.value);
            await deskPage.waitForSelector("#registerAccount");
            await deskPage.click("#registerAccount");
            await deskPage.waitFor("label[for=selectAccountName]");
            await deskPage.click("label[for=selectAccountName]");
            const node1AccountOption = (await deskPage.$$("li[class^=MuiButtonBase"))[newAccountCount];
            await node1AccountOption.click(); // select node1 account to sign
            await deskPage.waitFor(550);
            await deskPage.click("#selectKeypairName");
            await deskPage.waitFor(550);
            await deskPage.waitFor("li[data-value='1']");
            await deskPage.click("li[data-value='1']"); // select node1 keypair sign
            await deskPage.waitFor(550);
            await deskPage.click("#submitTransaction");
            // verify registration of the account
            await deskPage.waitFor(550);
            await deskPage.goto(accountListUrl);
            await deskPage.waitForSelector("a[aria-label='显示']");
            const accountShowButtons = await deskPage.$$("a[aria-label='显示'");
            const accountShowButton = accountShowButtons[newAccountCount - accountID];
            await accountShowButton.click();
            await deskPage.waitForSelector("#creditCode");
            await expect(deskPage.$eval("#creditCode", el => el.innerHTML)).resolves
                .toBe(accountCreditCode);
            // wait until status to be registered 
            await deskPage.waitForSelector("#status");
            while (await deskPage.$eval("#status", el => el.innerHTML) !== "已注册到区块链") {
                await deskPage.waitForSelector("button[aria-label='刷新']");
                await deskPage.click("button[aria-label='刷新']");
            }
            await deskPage.waitForSelector("#status");
            await expect(deskPage.$eval("#status", el => el.innerHTML)).resolves
                .toBe("已注册到区块链");
        }, 18000);
    };

    testRegisterNewLocalAccount("should register the 1st new local account into blockchain successfully", 
        1,
    );
    testRegisterNewLocalAccount("should register the 2nd new local account into blockchain successfully", 
        2,
    );
});

export default accountRegisterTest;
