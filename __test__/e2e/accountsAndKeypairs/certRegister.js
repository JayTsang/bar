/* eslint-disable no-await-in-loop */
import {
    newAccountCount,
    newKeypairCountForEachNewAccount,
    keypairListUrl,
} from "../helpers/const";
import { setBrowserAndPages } from "../helpers/setSharedVariables";

const certRegisterTest = () => describe("Keypairs register test", () => {
    let deskPage;

    beforeAll(async () => {
        const browserAndPages = await setBrowserAndPages();
        deskPage = browserAndPages.page1;
    });

    const testCertRegister = (testTitle, keypairID) => {
        it(testTitle, async () => {
            await deskPage.goto(keypairListUrl);
            await deskPage.waitForSelector("a[aria-label='编辑']");
            const editButtons = await deskPage.$$("a[aria-label='编辑']");
            await editButtons[
                newAccountCount * newKeypairCountForEachNewAccount - keypairID
            ].click();
            const keypairSN = await deskPage.$eval("[id='kp.sn']", el => el.value);
            await deskPage.waitForSelector("#registerCert");
            await deskPage.click("#registerCert");
            await deskPage.waitFor("label[for=selectAccountName]");
            await deskPage.click("label[for=selectAccountName]");
            const node1AccountOption = (await deskPage.$$("li[class^=MuiButtonBase"))[newAccountCount];
            await node1AccountOption.click();
            await deskPage.waitFor(550);
            await deskPage.click("#selectKeypairName");
            await deskPage.waitFor(550);
            await deskPage.waitFor("li[data-value='1']");
            await deskPage.click("li[data-value='1']"); // select node1 keypair sign
            await deskPage.waitFor(550);
            await deskPage.click("#submitTransaction");
            // verify registration of the cert
            await deskPage.waitFor(550);
            await deskPage.goto(keypairListUrl);
            await deskPage.waitForSelector("a[aria-label='显示']");
            const showButtons = await deskPage.$$("a[aria-label='显示']");
            await showButtons[
                newAccountCount * newKeypairCountForEachNewAccount - keypairID
            ].click();
            await expect(deskPage.$eval("#sn", el => el.innerHTML))
                .resolves.toBe(keypairSN);
            await deskPage.click("#showCert");
            // wait until status to be registered
            await deskPage.waitForSelector("#registerStatus");
            while (await deskPage.$eval("#registerStatus", el => el.innerHTML) !== "已注册到区块链") {
                await deskPage.waitForSelector("button[aria-label='刷新']");
                await deskPage.click("button[aria-label='刷新']");
            }
            // await deskPage.click("#registerStatus");
            await deskPage.waitForSelector("#registerStatus");
            await expect(deskPage.$eval("#registerStatus", el => el.innerHTML))
                .resolves.toBe("已注册到区块链");
            await expect(deskPage.$eval("#enabledStatus", el => el.innerHTML))
                .resolves.toBe("已启用");
            await expect(deskPage.$eval("#restrictStatus", el => el.innerHTML))
                .resolves.toBe("可用于所有文件操作");
        }, 10000);
    };

    testCertRegister("should register the 1st cert into blockchain successfully", 1);
    testCertRegister("should register the 2nd cert into blockchain successfully", 2);
    testCertRegister("should register the 3rd cert into blockchain successfully", 3);
    testCertRegister("should register the 4th cert into blockchain successfully", 4);
});

export default certRegisterTest;
