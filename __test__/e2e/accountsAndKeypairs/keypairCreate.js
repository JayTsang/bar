import path from "path";
import { setBrowserAndPages } from "../helpers/setSharedVariables";
import {
    keypairListUrl,
    baseUrl,
    nodeCertName,
    accountNodeName,
    account2Name,
    account1Name,
} from "../helpers/const";

const keypairCreateTest = () => describe("Keypairs create test", () => {
    let deskPage;

    beforeAll(async () => {
        const browserAndPages = await setBrowserAndPages();
        deskPage = browserAndPages.page1;
    });

    it("should direct to the keypair list page when click the keypairs menuitem ", async () => {
        await deskPage.goto(baseUrl);
        await deskPage.waitForSelector("[class^=Menu-main]");
        await deskPage.click("a[role=menuitem][href='#/keypairs']");
        expect(deskPage.url()).toBe(keypairListUrl);
    });

    it("should direct to keypair create page when clicking the create button", async () => {
        await deskPage.goto(keypairListUrl);
        await deskPage.waitForSelector("a[role=button][aria-label='新建']");
        await deskPage.click("a[role=button][aria-label='新建']");
        expect(deskPage.url()).toBe(`${keypairListUrl}/create`);
    });

    it("should create the keypair/certificate belong to the node1 account successfully", async () => {
        // create
        await deskPage.goto(keypairListUrl);
        await deskPage.waitForSelector("a[role=button][aria-label='新建']");
        await deskPage.click("a[role=button][aria-label='新建']");
        await deskPage.click("#createMethod_import");
        const fileInput = await deskPage.$("input[type=file]");
        await fileInput.uploadFile(path.join(__dirname, "121000005l35120456.node1.pem"));
        await deskPage.waitForSelector("[class^=MuiSelect][role=button]");
        await deskPage.click("[class^=MuiSelect][role=button]");
        await deskPage.click("li[data-value='1']");
        await deskPage.waitFor(550);
        await deskPage.type("input[name=name]", nodeCertName);
        await deskPage.click("button[type='submit']");

        // show
        await deskPage.waitFor(250);
        await deskPage.waitForSelector("a[aria-label='显示']");
        expect(deskPage.url()).toBe(keypairListUrl);
        await deskPage.click("a[aria-label='显示']");
        await deskPage.waitForSelector("#ownerName");
        await expect(deskPage.$eval("#ownerName", el => el.innerHTML))
            .resolves.toBe(accountNodeName);
        await deskPage.click("#showCert");
        await expect(deskPage.$eval("#certName", el => el.innerHTML))
            .resolves.toBe(nodeCertName);
        await deskPage.waitForSelector("#registerStatus");
        await expect(deskPage.$eval("#registerStatus", el => el.innerHTML))
            .resolves.toBe("已注册到区块链");
        await expect(deskPage.$eval("#enabledStatus", el => el.innerHTML))
            .resolves.toBe("已启用");
        await expect(deskPage.$eval("#restrictStatus", el => el.innerHTML))
            .resolves.toBe("可用于所有文件操作");
    });

    const newAccountNames = [account1Name, account2Name];
    const testCreateKeypairForNewAccount = (testTitle, newCertID, newAccountID) => {
        it(testTitle, async () => {
            // create new keypair
            await deskPage.goto(keypairListUrl);
            await deskPage.waitForSelector("a[role=button][aria-label='新建']");
            await deskPage.click("a[role=button][aria-label='新建']");
            await deskPage.click("#createMethod_new");
            await deskPage.waitFor(250);
            await deskPage.click("input[type=radio][value=EC]");
            await deskPage.waitFor(250);
            const ECCurve = "secp256k1";
            await deskPage.click("#selectInputECCurve");
            await deskPage.click(`li[data-value='${ECCurve}']`);
            await deskPage.waitFor(550);
            const certSigAlgEC = "SHA1withECDSA";
            await deskPage.click("#selectInputCertSigAlgEC");
            await deskPage.click(`li[data-value='${certSigAlgEC}']`);
            await deskPage.waitFor(550);
            const [certValidityStart, certValidityEnd] = await deskPage.$$("[class=picker]");
            await certValidityStart.click();
            await deskPage.click("button[aria-label=OK]");
            await deskPage.waitFor(550);
            await certValidityEnd.click();
            await deskPage.click("h3");
            await deskPage.click("[class*='Year-selected']+[role='button']");
            await deskPage.click("button[aria-label=OK]");
            await deskPage.waitFor(550);
            await deskPage.click("li[class^='SimpleFormIterator'] button");
            await deskPage.click("li [class^=MuiSelect-root]");
            await deskPage.click("li[data-value=CN]");
            await deskPage.waitFor(550);
            const newCertName = `testCert-${newCertID}-forAccount-${newAccountID}`;
            await deskPage.type("input[id$=value][id^='cert.distinguishName']", newCertName);
            await deskPage.waitForSelector("label[for=selectInputOwnerAccount]");
            await deskPage.click("label[for=selectInputOwnerAccount]");
            await deskPage.click(`li[data-value='${newAccountID + 1}']`); // select the owner account
            await deskPage.waitFor(550);
            await deskPage.type("input[name=name]", newCertName);
            await deskPage.click("button[type=submit]");
            // verify the above creation
            await deskPage.waitFor(550);
            await deskPage.waitForSelector("a[role=button][aria-label='显示']");
            await deskPage.click("a[role=button][aria-label='显示']");
            await expect(deskPage.$eval("#algParam", el => el.innerHTML)).resolves
                .toBe(ECCurve);
            await expect(deskPage.$eval("#ownerName", el => el.innerHTML)).resolves
                .toBe(newAccountNames[newAccountID - 1]);
            await deskPage.click("#showCert");
            await expect(deskPage.$eval("#certName", el => el.innerHTML)).resolves
                .toBe(newCertName);
            await expect(deskPage.$eval("#certSigAlg", el => el.innerHTML)).resolves
                .toBe(certSigAlgEC);
            await expect(deskPage.$eval("#registerStatus", el => el.innerHTML)).resolves
                .toBe("未注册到区块链");
        }, 15000);
    };

    testCreateKeypairForNewAccount(
        "should create keypair/certificate: 1 for account 1 successfully",
        1, 1,
    );
    testCreateKeypairForNewAccount(
        "should create keypair/certificate: 2 for account 1 successfully",
        2, 1,
    );
    testCreateKeypairForNewAccount(
        "should create keypair/certificate: 3 for account 2 successfully",
        3, 2,
    );
    testCreateKeypairForNewAccount(
        "should create keypair/certificate: 4 for account 2 successfully",
        4, 2,
    );
});

export default keypairCreateTest;
