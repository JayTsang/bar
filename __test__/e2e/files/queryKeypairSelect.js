import { setBrowserAndPages } from "../helpers/setSharedVariables";
import {
    fileListUrl, 
    accountListUrl, 
    keypairListUrl, 
    newAccountCount, 
    newKeypairCountForEachNewAccount,
} from "../helpers/const";

const fileQueryKeypairSelectTest = () => {
    describe("File query keypair select test", () => {
        let deskPage;

        beforeAll(async () => {
            const browserAndPages = await setBrowserAndPages();
            deskPage = browserAndPages.page1;
        });

        it("should show the file query keypair select dialog when not selected yet", async () => {
            await deskPage.goto(fileListUrl);
            await deskPage.waitForSelector("[class^=MuiModal][role=dialog]");
            await expect(deskPage.$eval("[id='keypair-select-dialog-title']>h2", el => el.innerHTML))
                .resolves.toBe("选择默认检索密钥");
        });

        it("should go backwards when click cancel button on the dialog", async () => {
            await deskPage.goto(accountListUrl);
            await deskPage.goto(fileListUrl);
            await deskPage.waitForSelector("[class^=MuiModal][role=dialog]");
            await deskPage.click("button#cancel");
            expect(deskPage.url()).toBe(accountListUrl);
        });

        it("should disable the ok button when not select the query keypair", async () => {
            await deskPage.goto(fileListUrl);
            await deskPage.waitForSelector("[class^=MuiModal][role=dialog]");
            await expect(deskPage.$eval("button#ok", el => el.disabled))
                .resolves.toBeTruthy();
        });

        it("should select the file query keypair successfully", async () => {
            await deskPage.goto(fileListUrl);
            await deskPage.waitForSelector("[class^=MuiModal][role=dialog]");
            await deskPage.click("label[for='keypair-select-input-account']");
            const accountOptions = await deskPage.$$("li[class]");
            await accountOptions[newAccountCount - 1].click(); // select account 1
            await deskPage.waitFor(550);
            await deskPage.click("label[for='keypair-select-input-keypair']");
            const keypairOptions = await deskPage.$$("li[class]");
            await keypairOptions[
                newKeypairCountForEachNewAccount - 1
            ].click(); // select keypair 1(the first keypair of account 1)
            await deskPage.waitFor(550);
            await deskPage.click("button#ok");
            await deskPage.waitForSelector("[class^=list]");
            await deskPage.waitForSelector("p[class]");
            await expect(deskPage.$eval("p[class]", el => el.innerHTML))
                .resolves.toBe("结果为空");
            await deskPage.goto(keypairListUrl);
            await deskPage.waitForSelector("a[aria-label='显示']");
            const showButtons = await deskPage.$$("a[aria-label='显示']");
            await showButtons[newAccountCount * newKeypairCountForEachNewAccount - 1].click();
            await deskPage.click("#showCert");
            await deskPage.waitForSelector("#restrictStatus");
            await expect(deskPage.$eval("#restrictStatus", el => el.innerHTML))
                .resolves.toBe("只用于文件信息检索");
        });

        it("should show the file list when the query keypair is selected already", async () => {
            await deskPage.goto(fileListUrl);
            await deskPage.waitForSelector("[class^=list]");
            await deskPage.waitForSelector("p[class]");
            await expect(deskPage.$eval("p[class]", el => el.innerHTML))
                .resolves.toBe("结果为空");
        });
    });
};

export default fileQueryKeypairSelectTest;
