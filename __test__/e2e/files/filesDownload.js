import fs from "fs-extra";
import path from "path";
import { Crypto } from "rclink";
import { setBrowserAndPages } from "../helpers/setSharedVariables";
import {
    fileListUrl,
    newAccountCount,
} from "../helpers/const";

const filesDownloadTest = () => {
    describe("Files download test", () => {
        let deskPage;
        const downloadDir = "downloadedFileForTest";

        beforeAll(async () => {
            const browserAndPages = await setBrowserAndPages();
            deskPage = browserAndPages.page1;
        });

        afterAll(() => fs.remove(downloadDir));

        it("should download a file successfully", async () => {
            await deskPage.goto(fileListUrl);
            await deskPage.waitForSelector("a[aria-label='显示']");
            await deskPage.click("a[aria-label='显示']");
            const hash = await deskPage.$eval("#hash", el => el.innerHTML);
            const hashAlg = await deskPage.$eval("#hashAlg", el => el.innerHTML);
            const name = await deskPage.$eval("#name", el => el.innerHTML);
            await deskPage.click("button#download");
            await deskPage.waitForSelector("#keypair-select-dialog-title h2[class]");
            await expect(deskPage.$eval("#keypair-select-dialog-title h2[class]", el => el.innerHTML))
                .resolves.toBe("选择签名密钥");
            const [accountSelectInput, keypairSelectInput] = await deskPage.$$("[class^=MuiSelect-root]");
            await accountSelectInput.click(); 
            const accountOptions = await deskPage.$$("li[class]");
            await accountOptions[newAccountCount - 1].click();
            await deskPage.waitFor(550);
            await keypairSelectInput.click();
            const keypairOptions = await deskPage.$$("li[class]");
            await keypairOptions[0].click();
            await deskPage.waitFor(550);
            const client = await deskPage.target().createCDPSession();
            await client.send("Page.setDownloadBehavior", {
                behavior: "allow",
                downloadPath: downloadDir,
            });
            await deskPage.click("button#ok");
            await deskPage.waitFor(2000);
            const fileBuffer = fs.readFileSync(path.join(downloadDir, name));
            expect(Crypto.GetHashVal({ data: fileBuffer, alg: hashAlg }).toString("hex"))
                .toBe(hash);
        });
    });
};

export default filesDownloadTest;
