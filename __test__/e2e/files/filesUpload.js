import { isArray } from "lodash";
import { setBrowserAndPages } from "../helpers/setSharedVariables";
import {
    fileListUrl,
    newAccountCount,
} from "../helpers/const";

const filesUploadTest = () => {
    describe("Files upload test", () => {
        let deskPage;

        beforeAll(async () => {
            const browserAndPages = await setBrowserAndPages();
            deskPage = browserAndPages.page1;
        });

        const testFilesUpload = (testTitle, paths) => {
            it(testTitle, async () => {
                await deskPage.goto(fileListUrl);
                await deskPage.waitForSelector("a[aria-label='新建']");
                await deskPage.waitFor(850); // wait pagination info updated
                const countOfFileUploadedBefore = await deskPage.$$eval("[class^=MuiTablePagination] span", 
                    (spans) => {
                        if (spans.length === 0) return 0;
                        return parseInt(spans[1].innerHTML.split("/")[1], 10);
                    },
                );
                const countOfFileToUpload = isArray(paths) ? paths.length : 1;
                await deskPage.click("a[aria-label='新建']");
                const fileInput = await deskPage.$("input[type=file]");
                if (isArray(paths)) {
                    fileInput.uploadFile(...paths);
                } else {
                    fileInput.uploadFile(paths);
                }
                await deskPage.waitForSelector("[class^='MuiSelect-root']");
                const [
                    accountSelectInput,
                    keypairSelectInput,
                ] = await deskPage.$$("[class^='MuiSelect-root']");
                await accountSelectInput.click();
                const accountOptions = await deskPage.$$("li");
                await accountOptions[newAccountCount - 1].click();
                await deskPage.waitFor(550);
                await keypairSelectInput.click();
                const keypairOptions = await deskPage.$$("li");
                await keypairOptions[0].click();
                await deskPage.waitFor(550);
                await deskPage.click("button[type=submit]");
                await deskPage.waitFor(1850); // wait pagination info updated
                await deskPage.waitForSelector("[class^=MuiTablePagination] span");
                const countOfFileUploadedAfter = await deskPage.$$eval("[class^=MuiTablePagination] span", 
                    spans => parseInt(spans[1].innerHTML.split("/")[1], 10),
                );
                await expect(countOfFileUploadedAfter)
                    .toBe(countOfFileToUpload + countOfFileUploadedBefore);
            });
        };

        testFilesUpload("should upload a single file one time successfully", 
            "index.js",
        );

        testFilesUpload("should upload multi files one time successfully", [
            "example.test.js", "files/filesUpload.js", "helpers/const.js",
        ]);
    });
};

export default filesUploadTest;
