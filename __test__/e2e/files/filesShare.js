import { setBrowserAndPages, getAccount } from "../helpers/setSharedVariables";
import {
    fileListUrl,
    baseUrl,
    newAccountCount,
} from "../helpers/const";

const filesShareTest = () => {
    describe("Files share test", () => {
        let deskPage;

        beforeAll(async () => {
            const browserAndPages = await setBrowserAndPages();
            deskPage = browserAndPages.page1;
        });

        it("should share the file successfully", async () => {
            await deskPage.goto(fileListUrl);
            await deskPage.waitForSelector("a[aria-label='显示']");
            await deskPage.click("a[aria-label='显示']");
            await deskPage.click("#showACLs");
            await deskPage.waitForSelector("[class^=MuiTablePagination] span"); // wait pagination info updated
            const countOfACLsBefore = await deskPage.$$eval("[class^=MuiTablePagination] span",
                spans => parseInt(spans[1].innerHTML.split("/")[1], 10),
            );
            expect(countOfACLsBefore).toBe(1); // the one for the owner
            await expect(deskPage.$eval("td[class*=userID] span", el => el.innerHTML))
                .resolves.toBe(getAccount(1).creditCode); // the account 1
            await expect(deskPage.$eval("td[class*=restrictionType] span", el => el.innerHTML))
                .resolves.toBe("无限制");
            // create a new file ACL record
            await deskPage.click("a[aria-label='新建']");
            expect(deskPage.url()).toBe(`${baseUrl}/accessAuths/create`);
            await deskPage.type("input#userID", getAccount(2).creditCode); // share the file to the account 2
            await deskPage.waitForSelector("[class^=MuiSelect-root]");
            const [
                accessAuthSelect,
                accessRestrictTypeSelect,
                accessPostActStoreProofSelect,
                requesterIDSelect,
                keypairIDSelect,
            ] = await deskPage.$$("[class^=MuiSelect-root]");
            await accessAuthSelect.click();
            await deskPage.click("li[data-value=download]");
            await deskPage.keyboard.press("Escape");
            await deskPage.waitFor(550);
            await accessRestrictTypeSelect.click();
            await deskPage.click("li[data-value=TIME_RANGE]");
            await deskPage.waitFor(550);
            await deskPage.waitForSelector("[class=picker]");
            const [
                accessRestrictTimeStart,
                accessRestrictTimeEnd,
            ] = await deskPage.$$("[class=picker]");
            await accessRestrictTimeStart.click();
            await deskPage.click("button[aria-label=OK]");
            await deskPage.waitFor(550);
            await accessRestrictTimeEnd.click();
            await deskPage.click("h3");
            await deskPage.click("[class*='Year-selected']+[role='button']");
            await deskPage.click("button[aria-label=OK]");
            await deskPage.waitFor(550);
            await accessPostActStoreProofSelect.click();
            await deskPage.click("li[data-value=download]");
            await deskPage.keyboard.press("Escape");
            await deskPage.waitFor(550);
            await requesterIDSelect.click();
            const accountOptions = await deskPage.$$("li[class]");
            await accountOptions[newAccountCount - 1].click();
            await deskPage.waitFor(550);
            await keypairIDSelect.click();
            const keypairOptions = await deskPage.$$("li[class]");
            await keypairOptions[0].click();
            await deskPage.waitFor(550);
            await deskPage.click("button[type=submit]");

            // verify the creation above
            await deskPage.waitFor(1850); // wait for pagination info updated
            await deskPage.waitForSelector("[class^=MuiTablePagination] span");
            const countOfACLsAfter = await deskPage.$$eval("[class^=MuiTablePagination] span",
                spans => parseInt(spans[1].innerHTML.split("/")[1], 10),
            );
            expect(countOfACLsAfter).toBe(2);
            await expect(
                deskPage.$$("tbody tr[class]")
                    .then(trHandles => trHandles[1].$$("td[class]")
                        .then(tdHandles => tdHandles[1].$eval("span", span => span.innerHTML))))
                .resolves.toBe(getAccount(2).creditCode);
            await expect(
                deskPage.$$("tbody tr[class]")
                    .then(trHandles => trHandles[1].$$("td[class]") 
                        .then(tdHandles => tdHandles[2].$eval("span", span => span.innerHTML))))
                .resolves.toBe("下载");
        }, 15000);
    });
};

export default filesShareTest;
