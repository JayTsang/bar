/* eslint-disable no-await-in-loop */
import { setBrowserAndPages } from "../helpers/setSharedVariables";
import {
    fileListUrl,
} from "../helpers/const";

const filesProofQueryTest = () => {
    describe("Files proof query test", () => {
        let deskPage;

        beforeAll(async () => {
            const browserAndPages = await setBrowserAndPages();
            deskPage = browserAndPages.page1;
        });

        it("should query the file proofs successfully", async () => {
            await deskPage.goto(fileListUrl);
            await deskPage.waitForSelector("a[aria-label='显示']");
            await deskPage.click("a[aria-label='显示']");
            await deskPage.click("#showProofs");
            await deskPage.waitForSelector("[class^=MuiTablePagination] span");
            await deskPage.waitFor(550);
            const countOfProofs = await deskPage.$$eval("[class^=MuiTablePagination] span",
                spans => parseInt(spans[1].innerHTML.split("/")[1], 10),
            );
            expect(countOfProofs).toBe(2); // one for upload and the other for download
            
            await deskPage.waitForSelector("#proofStatus");
            let proofStatusChips = await deskPage.$$("#proofStatus");
            while (await proofStatusChips[0].$eval("span", el => el.innerHTML) === "上链等待中"
                || await proofStatusChips[1].$eval("span", el => el.innerHTML) === "上链等待中"
            ) {
                // await deskPage.waitForSelector("button[aria-label='刷新']");
                // await deskPage.click("button[aria-label='刷新']");
                await deskPage.waitForSelector("#proofStatus");
                proofStatusChips = await deskPage.$$("#proofStatus");
            }

            await deskPage.waitFor(550); // wait for element to be attached
            const [uploadProofStatusChip] = await deskPage.$$("#proofStatus");
            await expect(uploadProofStatusChip.$eval("span", el => el.innerHTML))
                .resolves.toBe("已上链确认");
            await uploadProofStatusChip.click();
            await expect(deskPage.$eval("#opType span", el => el.innerHTML))
                .resolves.toBe("操作类型：文件上传存储");
            const uploadTxid = await deskPage.$eval("a#confirmedTx", el => el.innerHTML);
            await deskPage.click("a#confirmedTx");
            await deskPage.waitForSelector("[class$=txid] [class^=MuiTypography]");
            await expect(deskPage.$eval("[class$=txid] [class^=MuiTypography]", el => el.innerHTML))
                .resolves.toBe(uploadTxid);
            await deskPage.goBack();

            await deskPage.waitFor(550); // wait for element to be attached
            const [, downloadProofStatusChip] = await deskPage.$$("#proofStatus");
            await expect(downloadProofStatusChip.$eval("span", el => el.innerHTML))
                .resolves.toBe("已上链确认");
            await downloadProofStatusChip.click();
            await expect(deskPage.$eval("#opType span", el => el.innerHTML))
                .resolves.toBe("操作类型：文件下载");
            const downloadTxid = await deskPage.$eval("a#confirmedTx", el => el.innerHTML);
            await deskPage.click("a#confirmedTx");
            await deskPage.waitForSelector("[class$=txid] [class^=MuiTypography]");
            await expect(deskPage.$eval("[class$=txid] [class^=MuiTypography]", el => el.innerHTML))
                .resolves.toBe(downloadTxid);
            await deskPage.goBack();

            await deskPage.waitFor(550);
        }, 50000);
    });
};

export default filesProofQueryTest;
