import indexPageTest from "./index";
import accountCreateTest from "./accountsAndKeypairs/accountCreate";
import keypairCreateTest from "./accountsAndKeypairs/keypairCreate";
import accountRegisterTest from "./accountsAndKeypairs/accountRegister";
import certRegisterTest from "./accountsAndKeypairs/certRegister";
import fileQueryKeypairSelectTest from "./files/queryKeypairSelect";
import filesUploadTest from "./files/filesUpload";
import filesDownloadTest from "./files/filesDownload";
import filesProofQueryTest from "./files/filesProofQuery";
import filesShareTest from "./files/filesShare";

indexPageTest();

accountCreateTest();
keypairCreateTest();
accountRegisterTest();
certRegisterTest();

fileQueryKeypairSelectTest();
filesUploadTest();
filesDownloadTest();
filesProofQueryTest();
filesShareTest();
