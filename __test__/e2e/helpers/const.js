export const baseUrl = "http://localhost:3000/#";

export const accountListUrl = `${baseUrl}/accounts`;

export const keypairListUrl = `${baseUrl}/keypairs`;

export const fileListUrl = `${baseUrl}/files`;

export const accountNodeName = "node1";
export const accountNodeCreditCode = "121000005l35120456";
export const nodeCertName = "node1";

export const newAccountCount = 2; 
export const account1Name = "TestAccount1";
export const account2Name = "TestAccount2";

export const newKeypairCountForEachNewAccount = 2;
