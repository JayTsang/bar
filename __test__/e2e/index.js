import { setBrowserAndPages } from "./helpers/setSharedVariables";

const indexPageTest = () => describe("Index page test", () => {
    let deskPage;

    beforeAll(async () => {
        const browserAndPages = await setBrowserAndPages();
        deskPage = browserAndPages.page1;
    });

    it("should show loading info firstly when loading data provider", async () => {
        await deskPage.waitForSelector("#data-provider-loading");
        const loadingText = await deskPage.$eval("#data-provider-loading", element => element.innerHTML);
        await deskPage.setRequestInterception(false);
        expect(loadingText).toBe("Loading");
    });
    
    it("should be titled 'Storage Based on RepChain'", async () => {
        const title = await deskPage.title();
        expect(title).toBe("Storage Based on RepChain");
    });

    it("should show the side menu", async () => {
        await deskPage.waitForSelector("[class^=Menu-main]");
        const sideMenu = await deskPage.$("[class^=Menu-main]");
        expect(sideMenu).not.toBeNull();
    });

    it("should show 6 menu item inside the side menu", async () => {
        await deskPage.waitForSelector("[class^=Menu-main]");

        const menuItemsCount = await deskPage.$$eval("a[role=menuitem]", elements => elements.length);
        expect(menuItemsCount).toBe(6);

        const menuItemIndex = await deskPage.$("a[role=menuitem][href='#/']");
        expect(menuItemIndex).not.toBeNull();
        const menuitemAccount = await deskPage.$("a[role=menuitem][href='#/accounts']");
        expect(menuitemAccount).not.toBeNull();
        const menuitemKeypair = await deskPage.$("a[role=menuitem][href='#/keypairs']");
        expect(menuitemKeypair).not.toBeNull();
        const menuitemBlock = await deskPage.$("a[role=menuitem][href='#/Block']");
        expect(menuitemBlock).not.toBeNull();
        const menuitemTransaction = await deskPage.$("a[role=menuitem][href='#/Transaction']");
        expect(menuitemTransaction).not.toBeNull();
        const menuitemFile = await deskPage.$("a[role=menuitem][href='#/files']");
        expect(menuitemFile).not.toBeNull();
    });
});

export default indexPageTest;
